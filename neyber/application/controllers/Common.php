<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Common extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Commonmodel');
    }

    function Get_all_get()
    {
        $city = $this->Commonmodel->get_city();
        $cities = array();
        foreach($city as $k => $v)
        {
            $cities[$v['city_id']]['city_id']=$v['city_id'];
            $cities[$v['city_id']]['city_name']=$v['city_name'];
            $cities[$v['city_id']]['city_latitude'] = $v['city_latitude'];
            $cities[$v['city_id']]['city_longitude'] = $v['city_longitude'];
            $cities[$v['city_id']]['city_admin_status'] = $v['city_admin_status'];
        }
        $city = array();
        foreach($cities as $v){
            $city[] = array('city_id'=>$v['city_id'],'city_name'=>$v['city_name'],'city_latitude'=>$v['city_latitude'],'city_longitude'=>$v['city_longitude'],'city_admin_status'=>$v['city_admin_status']);
        }
        foreach ($city as $key=>$value)
        {
            $city_id = $value['city_id'];
            $car = $this->Commonmodel->get_car($city_id);
            $a= array();
            foreach ($car as $data=>$login)
            {
                $car_type_id = $login['car_type_id'];
                $carmodel = $this->Commonmodel->get_car_model($car_type_id);
                foreach($carmodel as $carmodel)
                {
                    $a[]=$carmodel;
                }
                $car[$data]= $login;
                $car[$data]["Car_Model_list"]=  $a;
                $a = array();
            }
            $city[$key]= $value;
            $city[$key]["Car_type_list"]= $car;
        }
        $this->set_response([
            'status' => 1,
            'message' => 'City And Car Type List',
            'details'=>$city
        ], REST_Controller::HTTP_CREATED);
    }

    function Cancel_Reasons_post()
    {
        $app_id = $this->post('app_id');
        if (!empty($app_id))
        {
            $data = $this->Commonmodel->cancel_reason($app_id);
            if (!empty($data))
            {
                $this->set_response([
                                    'status' => 1,
                                    'message' => 'Cancel Reason',
                                    'details'=>$data
                                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->set_response([
                                    'status' => 0,
                                    'message' => 'No Data Found'
                                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->set_response([
                            'status' => 0,
                            'message' => 'Required Field Missing'
                        ], REST_Controller::HTTP_CREATED);
        }
    }

}