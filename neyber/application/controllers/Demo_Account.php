<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Demo_Account extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Useraccountmodel');
    }

    function Signup_post()
    {
        $first_name = "Demo";
        $lastname =  "User";
        $user_password = "Apporiotaxi";
        $user_phone = $this->post('user_phone');
        $user_email = $this->post('user_email');
        $unique_number = $this->post('unique_number');
        if($first_name != "" && $lastname != "" && $user_password != "" && $unique_number != ""){
            $text = $this->Useraccountmodel->check_unique_number($unique_number);
            if (!empty($text))
            {
                $this->response([
                            'result' => 1,
                            'message' => "Signup Succusfully!!",
                            'details' => $text
                        ], REST_Controller::HTTP_CREATED);
            }else{
                $user_name = $first_name." ".$lastname;
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data=$dt->format('M j');
                $day=date("l");
                $date=$day.", ".$data ;
                if (empty($user_phone))
                {
                    $user_phone = "User Phone Number";
                }
                if (empty($user_email))
                {
                    $user_phone = "User Email Number";
                }
                $data = array(
                    'user_name' => $user_name,
                    'user_phone' => $user_phone,
                    'user_password' => $user_password,
                    'user_email'=>$user_email,
                    'register_date'=>$date,
                    'user_type'=>2
                );
                $text = $this->Useraccountmodel->signup($data);
                $this->response([
                    'result' => 1,
                    'message' => "Signup Succusfully!!",
                    'details' => $text
                ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                'result' => 0,
                'message' => "Required fields missing!!"
            ], REST_Controller::HTTP_CREATED);
        }
    }
    
}