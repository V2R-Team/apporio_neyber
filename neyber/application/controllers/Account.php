<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Account extends REST_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Useraccountmodel');
        $this->load->model('Googlemodel');
    }

    function Signup_post()
    {
        $first_name = $this->post('first_name');
        $lastname =  $this->post('last_name');
        $user_phone = $this->post('phone');
        $user_password = $this->post('password');
        if($first_name != "" && $lastname != "" && $user_phone != ""  && $user_password != ""){
            $user_name = $first_name." ".$lastname;
            $query = $this->Useraccountmodel->check_user($user_phone,$user_password);
            if($query == 0){
                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data == 0) {
                    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                    $data=$dt->format('M j');
                    $day=date("l");
                    $date=$day.", ".$data ;
                    $data = array(
                        'user_name' => $user_name,
                        'user_phone' => $user_phone,
                        'user_password' => $user_password,
                        'register_date'=>$date
                    );
                    $text = $this->Useraccountmodel->signup($data);
                    $this->response([
                             'result' => 1,
                             'message' => "Signup Succusfully!!",
                             'details' => $text
                        ], REST_Controller::HTTP_CREATED);
                }else{
                    $user_id = $data['user_id'];
                    $text = $this->Useraccountmodel->update_password($user_id,$user_password);
                    $this->response([
                                    'result' => 1,
                                    'message' => "Signup Succusfully!!",
                                    'details' => $text
                                ], REST_Controller::HTTP_CREATED);
                }
            }
            else{
                $this->response([
                                'result' => 0,
                                'message' => "Phone already registerd!!"
                            ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                                'result' => 0,
                                'message' => "Required fields missing!!"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Logout_post()
    {
        $user_id = $this->post('user_id');
        $unique_id = $this->post('unique_id');
        if (!empty($user_id) && !empty($unique_id))
        {
            $this->Useraccountmodel->logout($user_id,$unique_id);
                $this->response([
                    'result' => 1,
                    'message' => "logout Successfully"
                ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                            'result' => 0,
                            'message' => "Required fields missing!!"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function phone_post()
    {
        $user_phone = $this->post('phone');
        if($user_phone != "")
        {

            $data = $this->Useraccountmodel->check_phone2($user_phone);
            if($data)
            {
                $this->response([
                                'result' => 1,
                                'message' => "user details",
                                 'details' => $data
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                    'result' => 0,
                                    'message' => "phone number not register!!"
                                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                                'result' => 0,
                                'message' => "Required fields missing!!"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function update_email_post()
    {
        $user_id = $this->post('user_id');
        $email = $this->post('email');
        if($user_id != "" && $email != "")
        {
            $data = $this->Useraccountmodel->update_email($email,$user_id);
            if($data)
            {
                $this->response([
                                'result' => 1,
                                'message' => "email update",
                                 'details'=>$data
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'result' =>0,
                    'message' => "wrong user id"
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'result' =>0,
                'message' => "Required fields missing!!"
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Login_post(){
        $user_phone = $this->post('phone');
        $user_password = $this->post('password');
        if($user_phone != "" && $user_password != "" ){
            $query = $this->Useraccountmodel->login($user_phone,$user_password);
            $id = $query['user_id'];
            $this->Useraccountmodel->online($id);
            if($query){
                $this->response([
                    'result' =>1,
                    'message' => "Login Succusfully!!",
                    'value'=>1,
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }
            else{

                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data)
                {
                    $pass = "";
                    $text = $this->Useraccountmodel->phone($user_phone,$pass);
                    if($text)
                    {
                        $this->response([
                            'result' =>0,
                            'message' => "password does not match!!",
                            'value'=>2
                        ], REST_Controller::HTTP_CREATED);
                    }else {
                        $this->response([
                            'result' =>0,
                            'message' => "password does not match!!",
                            'value'=>0
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else {
                    $this->response([
                                    'result' =>0,
                                    'message' => "user does not exsit!!",
                                    'value'=>4
                                ], REST_Controller::HTTP_CREATED);
                }
            }
        }
        else{
            $this->response([
                                'result' =>0,
                                'message' => "Required fields missing!!"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Change_password_post()
    {
        $user_id = $this->post('user_id');
        $old_password = $this->post('old_password');
        $new_password = $this->post('new_password');
        if($user_id != "" && $old_password != "" && $new_password != ""){
            $data = $this->Useraccountmodel->change_password($user_id,$old_password,$new_password);
            if($data)
            {
                $this->response([
                    'result' =>1,
                    'message' => "password updated"
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'result' =>0,
                    'message' => "old password wrong"
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                            'result' =>0,
                            'message' => "Required fields missing!!"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function facebook_signup_post()
    {
        $facebook_id = $this->post('facebook_id');
        $facebook_mail = $this->post('facebook_mail');
        $facebook_image = $this->post('facebook_image');
        $facebook_firstname = $this->post('facebook_firstname');
        $facebook_lastname = $this->post('facebook_lastname');
        $phone = $this->post('phone');
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');

        if($facebook_id != "" && $facebook_mail != "" && $facebook_image != "" && $facebook_firstname != "" && $facebook_lastname != "" && $phone != "" && $first_name != "" && $last_name != "")
        {

            $data = $this->Useraccountmodel->check_phone($phone);
            if(empty($data))
            {
                $text = array(
                    'facebook_id'=>$facebook_id,
                    'facebook_mail'=>$facebook_mail,
                    'facebook_image'=>$facebook_image,
                    'facebook_firstname'=>$facebook_firstname,
                    'facebook_lastname'=>$facebook_lastname,
                    'user_phone'=>$phone,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->facebook_signup($text);
                $this->response([
                    'result' =>1,
                    'msg' => "Facebook Signup Successfully",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }else{
                $text = array(
                    'facebook_id'=>$facebook_id,
                    'facebook_mail'=>$facebook_mail,
                    'facebook_image'=>$facebook_image,
                    'facebook_firstname'=>$facebook_firstname,
                    'facebook_lastname'=>$facebook_lastname,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->facebook_updated($text,$phone);
                $this->response([
                                'result' =>1,
                                'msg' => "Details Updated",
                                'details'=>$query
                            ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                                'result' =>0,
                                'msg' => "Required field Missing"
                            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function facebook_login_post()
    {
        $facebook_id = $this->input->post('facebook_id');

        if($facebook_id != ""){
            $query = $this->Googlemodel->facebook_login($facebook_id);
            if(empty($query))
            {
                $this->response([
                                'result' =>0,
                                'msg' => "Wrong Facebook Id"
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                'result' =>1,
                                'msg' => "login successfully",
                                'details'=>$query
                            ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                            'result' =>0,
                            'msg' => "Required field Missing"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function google_signup_post()
    {
        $google_id = $this->post('google_id');
        $google_name = $this->post('google_name');
        $google_mail = $this->post('google_mail');
        $google_image = $this->post('google_image');
        $phone = $this->post('phone');
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');

        if($google_id != "" && $google_name != "" && $google_mail != "" && $google_image != "" && $phone != "" && $first_name != "" && $last_name != "")
        {

            $data = $this->Useraccountmodel->check_phone($phone);

            if(empty($data))
            {
                $text = array(
                    'google_id'=>$google_id,
                    'google_name'=>$google_name,
                    'google_mail'=>$google_mail,
                    'google_image'=>$google_image,
                    'user_phone'=>$phone,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->google_signup($text);

                $this->response([
                    'result' =>1,
                    'msg'=>"Google Signup Successfully",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }else{
                $text = array(
                    'google_id'=>$google_id,
                    'google_name'=>$google_name,
                    'google_mail'=>$google_mail,
                    'google_image'=>$google_image,
                    'user_name' => $first_name." ".$last_name
                );

                $query = $this->Googlemodel->google_update($text,$phone);
                $this->response([
                    'result' =>1,
                    'msg'=>"Detail updated",
                    'details'=>$query
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'result' =>0,
                'msg'=>"Required field Missing"
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function google_login_post()
    {
        $google_id = $this->post('google_id');

        if($google_id != ""){
            $query = $this->Googlemodel->google_login($google_id);
            if(empty($query))
            {
                $this->response([
                                    'result' =>0,
                                    'msg'=>"Wrong Google Id"
                                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                                    'result' =>1,
                                    'msg'=>"login successfully",
                                    'details'=>$query
                                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                            'result' =>0,
                            'msg'=>"Required field Missing"
                        ], REST_Controller::HTTP_CREATED);
        }
    }

    public function image_post()
    {
        $user_id = $this->post('user_id');
        if(!empty($_FILES["profile_image"]['name'])) {
            $condition1 = "";
            $ext  = strtolower(substr($_FILES["profile_image"]['name'], -4));
            $ext  = $ext == "jpeg" ? ".jpg" : $ext;
            $config['upload_path']    = 'images';
            $config['allowed_types']  = 'PNG|JPG|png|jpg|PDF|pdf';
            $config['overwrite']      = TRUE;
            $config['encrypt_name']   = FALSE;
            $config['remove_spaces']  = TRUE;
            if( !is_dir($config['upload_path']) ) mkdir($config['upload_path'], 0755, true);
            $condition['user_id'] = $user_id;
            $config['file_name'] = $user_id.time().$ext;
            $this->load->library('upload');
            $this->upload->initialize($config);
            $this->upload->do_upload('profile_image');
            $this->Googlemodel->update("user", array("user_image" => "images/".$user_id.time().$ext),$condition);
            $query = $this->Googlemodel->user($user_id);
            $this->response([
                            'result' =>1,
                            'msg'=>"image updated",
                             'details'=>$query
                        ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                            'result' =>0,
                            'msg'=>"image not updated"
                        ], REST_Controller::HTTP_CREATED);
        }
    }
}
