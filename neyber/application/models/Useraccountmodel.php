<?php
class Useraccountmodel extends CI_Model{
    public function signup($data)
    {
        $this->db->set($data)
                 ->insert('user');
        $id = $this->db->insert_id();
        $query = $this->db->get_where('user', array('user_id' => $id));
        return $query ->row_array();
    }

    public function update_password($user_id,$password)
    {
        $this->db->where('user_id',$user_id)
                 ->update('user',['user_password'=>$password]);
        $query = $this->db->get_where('user', array('user_id' => $user_id));
        return $query ->row_array();
    }

    public function update_email($email,$user_id)
    {
        $this->db->where('user_id',$user_id)
            ->update('user',['user_email'=>$email]);
        $query = $this->db->get_where('user', array('user_id' => $user_id));
        return $query ->row_array();
    }

    public function phone($user_phone,$pass)
    {
        $query =  $this->db->where(['user_phone'=>$user_phone,'user_password'=>$pass])
            ->get('user');
        if($query->num_rows()){
            return $query->row_array();
        }
        else{
            return FALSE;
        }
    }

    public function login($user_phone,$user_password)
    {
        $query =  $this->db->where(['user_phone'=>$user_phone,'user_password'=>$user_password])
            ->get('user');
        if($query->num_rows()){
            return $query->row_array();
        }
        else{
            return FALSE;
        }
    }

    public function check_user($user_phone,$user_password)
    {
        $query =  $this->db->where(['user_phone'=>$user_phone,'user_password'=>$user_password])
                           ->get('user');
        if($query->num_rows()){
            return $query->row_array();
        }
        else{
            return FALSE;
        }
    }

    public function check_phone($user_phone)
    {
        $query =  $this->db->where(['user_phone'=>$user_phone])
                           ->get('user');
        if($query->num_rows()){
            return $query->row_array();
        }
        else{
            return FALSE;
        }
    }

    public function change_password($user_id,$old_password,$new_password)
    {
        $this->db->where(['user_id'=>$user_id,'user_password'=>$old_password])
                 ->update('user',['user_password'=>$new_password]);
        return $this->db->affected_rows();
    }

    public function check_phone2($user_phone)
    {
        $query =  $this->db->where(['user_phone'=>$user_phone])
                           ->where('user_password!=',"")
                           ->get('user');
        if($query->num_rows()){
            return $query->row_array();
        }
        else{
            return FALSE;
        }
    }

    public function logout($user_id,$unique_id)
    {
        $this->db->where(['user_id'=>$user_id,'unique_id'=>$unique_id])
                 ->update('user_device',['login_logout'=>0]);
    }

    public function online($id){
       $data =  array('login_logout'=>1);
       $this->db->where('user_id',$id);
       $this->db->update('user',$data);
    }

    function check_unique_number($unique_number)
    {
        $data = $this->db->get_where('user',['unique_number',$unique_number]);
        return $data->row();
    }

}
