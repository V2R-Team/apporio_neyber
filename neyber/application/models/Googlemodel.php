<?php 
class Googlemodel extends CI_MODEL{
   
   public function facebook_signup($text)
   {
      $this->db->set($text)
               ->insert('user');
               
      $id = $this->db->insert_id();
      $query = $this->db->select('*')
                     ->where('user_id',$id)
                     ->get('user');
       return $query->row_array();                
   }

   public function facebook_updated($text,$phone)
   {
       $this->db->where(['user_phone'=>$phone])
                ->update('user',$text);
       $query = $this->db->select('*')
                         ->where('user_phone',$phone)
                         ->get('user');
       return $query->row_array();
   }

    public function google_update($text,$phone)
    {
        $this->db->where(['user_phone'=>$phone])
                  ->update('user',$text);
        $query = $this->db->select('*')
                      ->where('user_phone',$phone)
                     ->get('user');
        return $query->row_array();
    }

   public function facebook_login($facebook_id)
   {
     $query = $this->db->select('*')
                     ->where(['facebook_id'=>$facebook_id])
                     ->get('user');
       return $query->row_array(); 
   }


   
   public function google_signup($text)
   {
      $this->db->set($text)
               ->insert('user');
               
      $id = $this->db->insert_id();
      $query = $this->db->select('*')
                     ->where('user_id',$id)
                     ->get('user');
       return $query->row_array();                
   } 
   
   public function google_login($google_id)
   {
     $query = $this->db->select('*')
                     ->where(['google_id'=>$google_id])
                     ->get('user');
       return $query->row_array(); 
   }
   
   public function user($user_id)
   {
       $query = $this->db->select('*')
                     ->where('user_id',$user_id)
                     ->get('user');
       return $query->row_array(); 
   }
   
    
    function update($table = '', $data = array(), $condition = '') {
	   if(is_array($condition)) {
	        foreach ($condition as $key => $val) {
	                $this->db->where($key, $val);
	            }
	    }
             elseif($condition != '') {
	            $this->db->where($condition);
	         }
	        $this->db->update($table, $data);
	
	        return true;
     }  

}