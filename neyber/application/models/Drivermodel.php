<?php
class Drivermodel extends CI_Model{

    function add_new_car($data)
    {
        $this->db->insert('table_driver_cars',$data);
    }

    function edit_profile($driver_id,$data)
    {
        $this->db->where(['driver_id'=>$driver_id])
                  ->update('table_driver',$data);
    }

    function change_password($driver_id,$new_password,$old_password)
    {
        $this->db->where(['driver_id'=>$driver_id,'driver_password'=>$old_password])
            ->update('table_driver',['driver_password'=>$new_password]);
        return $this->db->affected_rows();
    }

    function check_bankdetails($driver_id)
    {
        $data = $this->db->get_where('table_driver',['driver_id'=>$driver_id]);
        return $data->row();
    }

    function driver_rides($driver_id)
    {
        $data = $this->db->select('*')
            ->from('table_user_rides')
            ->order_by("user_ride_id", "desc")
            ->where(['driver_id'=>$driver_id])
            ->get();
        return $data->result_array();
    }

    function check_phone($phone)
    {
        $data = $this->db->get_where('table_driver',['driver_phone'=>$phone]);
        return $data->row();
    }

    function driver_profile($driver_id,$driver_token)
    {
        $data = $this->db->get_where('table_driver',['driver_id'=>$driver_id,'driver_token'=>$driver_token]);
        return $data->row();
    }

    public function add_device($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function Signup_Stap_One($data)
    {
        $this->db->insert('table_driver',$data);
        $driver_id = $this->db->insert_id();
        $data = $this->db->get_where('table_driver',['driver_id'=>$driver_id]);
        return $data->row();
    }

    function Signup_Stap_Two($image,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',['driver_profile_image'=>$image,'signup_step'=>2]);
    }

    function Signup_Stap_Three($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function Signup_Stap_Four($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function login($phone,$password)
    {
        $data = $this->db->get_where('table_driver',['driver_phone'=>$phone,'driver_password'=>$password]);
        return $data->row();
    }

    function update_driver_token($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
        $data = $this->db->get_where('table_driver',['driver_id'=>$driver_id]);
        return $data->row();
    }

    function car_type($car_type_id)
    {
        $data = $this->db->get_where('table_car_type',['car_type_id'=>$car_type_id]);
        return $data->row();
    }

    function car_model($car_model_id)
    {
        $data = $this->db->get_where('table_car_model',['car_model_id'=>$car_model_id]);
        return $data->row();
    }

    function driver_online($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function driver_offline($data,$driver_id)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('table_driver',$data);
    }

    function Document_List($car_type_id,$city_id)
    {
        $data = $this->db->select('*')
            ->from('table_documents')
            ->join('table_document_list','table_document_list.document_id=table_documents.document_id','inner')
            ->where(['table_document_list.city_id'=>$city_id,'table_document_list.car_type_id'=>$car_type_id])
            ->get();
        return $data->result_array();

    }

    function check_document($document_id,$driver_id)
    {
        $data = $this->db->get_where('table_driver_document',['driver_id'=>$driver_id,'document_id'=>$document_id]);
        return $data->row();
    }

    function document_upload($image,$driver_id,$document_id)
    {
        $data = array('document_path'=>$image,'driver_id'=>$driver_id,'document_id'=>$document_id,'documnet_varification_status'=>1);
        $this->db->insert('table_driver_document',$data);
    }
}