<?php
class Commonmodel extends CI_Model{

    function get_city()
    {
        $data = $this->db->select('table_city.city_id,table_city.city_name,table_city.city_latitude,table_city.city_longitude,table_city.city_admin_status')
                         ->from('table_city')
                         ->join('table_rate_card','table_rate_card.city_id=table_city.city_id','inner')
                         ->where('table_city.city_admin_status',1)
                         ->get();
        return $data->result_array();
    }

    function get_car($city_id)
    {
        $data = $this->db->select('*')
                         ->from('table_car_type')
                         ->join('table_rate_card','table_rate_card.car_type_id=table_car_type.car_type_id','inner')
                         ->where(['table_car_type.car_type_admin_status'=>1,'table_rate_card.city_id'=>$city_id])
                         ->get();
        return $data->result_array();
    }

    function get_car_model($car_type_id)
    {
        $data = $this->db->select('*')
                         ->from('table_car_model')
                         ->where(['car_model_status'=>1,'car_type_id'=>$car_type_id])
                         ->get();
        return $data->result();
    }

    function cancel_reason($app_id)
    {
        $data = $this->db->select('*')
                        ->from('cancel_reasons')
                        ->where(['reason_type'=>$app_id,'cancel_reasons_status'=>1])
                        ->get();
        return $data->result();
    }

}