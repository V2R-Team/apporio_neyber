-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:21 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_neyber`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Apporio', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced develo', '#467, Spaze iTech Park', 'India', 'haryana', 'GURGAON', 122001, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'brar', 'appppp', '', '', '1234567', '9292922929', 'apporio@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(5, 'Driver ask to cancel', '', 0, 1, 1),
(6, 'Driver too far', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(2, 'Suzuki', 'uploads/car/car_2.png', 1),
(3, 'Ferrari', 'uploads/car/car_3.png', 1),
(4, 'Lamborghini', 'uploads/car/car_4.png', 1),
(5, 'Mercedes', 'uploads/car/car_5.png', 1),
(6, 'Tesla', 'uploads/car/car_6.png', 1),
(10, 'Renault', 'uploads/car/car_10.jpg', 1),
(11, 'taxi', 'uploads/car/car_11.png', 1),
(12, 'taxi', 'uploads/car/car_12.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `ride_mode`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', 1, 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.png', 1, 1),
(4, 'Mini', '', 'Mini', 'uploads/car/editcar_4.png', 1, 1),
(14, 'bike', '', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) NOT NULL,
  `city_longitude` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `city_name_arabic` varchar(255) NOT NULL,
  `city_name_french` varchar(255) NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(3, 'Dummy City', '', '', '20AC', 'Miles', '', '', 1),
(56, 'Gurugram', '', '', '0930', 'Miles', '', '', 1),
(57, 'Delhi', '', '', 'INR', 'Miles', '', '', 1),
(71, 'Florida', '', '', 'USD', 'Miles', '', '', 1),
(77, 'Bursa', '40.1885281', '29.0609636', 'TRY', 'Kilometers', '', '', 1),
(78, 'Lagos', '6.5243793', '3.3792057', 'NGN', 'Kilometers', '', '', 1),
(79, 'Riyadh', '24.7135517', '46.6752957', 'SAR', 'Kilometers', '', '', 1),
(80, 'Dammam', '26.3926665', '49.9777136', 'SAR', 'Kilometers', '', '', 1),
(81, 'PARIS', '31.2000924', '29.9187387', 'EUR', 'Kilometers', '', '', 2),
(82, 'MÃ©xico', '23.634501', '-102.552784', 'MXP', 'Kilometers', '', '', 1),
(83, 'North Miami Beach', '25.9331488', '-80.1625463', 'USD', 'Miles', '', '', 1),
(84, 'Paris', '48.856614', '2.3522219', 'EUR', 'Kilometers', '', '', 1),
(85, 'HÆ°ng YÃªn', '20.8525711', '106.0169971', 'VND', 'Kilometers', '', '', 1),
(86, 'Distrito de Lima', '-12.046374', '-77.0427934', 'PEN', 'Kilometers', '', '', 1),
(87, 'Oujda', '34.681962', '-1.900155', 'EUR', 'Kilometers', '', '', 1),
(88, 'Riyadh', '24.7135517', '46.6752957', 'SAR', 'Kilometers', '', '', 1),
(89, 'Distrito de Lima', '-12.046374', '-77.0427934', 'PEN', 'Kilometers', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(28, 'Erva', 'ervaerva@gmail.com', '251525152515', 'Mudanya/Bursa, TÃ¼rkiye', 'TURKEY', 77, 'erva erva', 'rikapimobil', '123456', '', 1),
(29, 'mazen', 'wkobessy@hotmail.com', '+966542961615', '11422 Riyadh,, Saudi Arabia', 'SAUDI ARABIA', 79, 'Waleed Abdelrahman', '123456', '', '', 1),
(30, 'MIZZO', 'WWWW@YYYY.COM', 'WKOBESSY', 'BAKOS', 'EGYPT', 81, 'Waleed Abdelrahman', '12345678', '133', '', 2),
(31, 'Take me ', 'ronyprevot@gmail.com', '3052006714', '18101 NW 7 ave miami gardens, fl 33169', 'UNITED STATES', 71, 'Peter', 'comcast1', '2323232', 'uploads/company/company_31.jpg', 1),
(32, 'company test', 'test@test.retest', '0406070809', 'info93390@yahoo.fr', 'FRANCE', 0, 'admin', '123456', '1234567', 'uploads/company/company_32.jpg', 1),
(33, 'kljlk', 'asdfgh@gmail.com', '0975687658', 'infolabs', 'VIET NAM', 85, 'jkjh', '12345678', '2', 'uploads/company/company_33.jpg', 1),
(34, 'iuyoiu', 'zxcvbn@gmail.com', '09587587', 'HÃ  Ná»™i, Viá»‡t Nam', 'BAHAMAS', 84, 'bnvnv', '12345678', '1', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 99, 'Apporok', 1, 1, 1, 1, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com', 'apporio@info.com', '  Mr. infolabs 2016 Â© Design & Developed By Apporio Taxi App', '222', 'Apporio', 'gurgaon');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIO', '10', '2017-06-14', '2017-06-15', 'Nominal', 2),
(74, 'APPORIO', '10', '2017-06-14', '2017-06-15', 'Nominal', 1),
(75, 'JUNE', '10', '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', '2017-07-29', '2018-06-30', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `iso` char(3) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`iso`, `name`) VALUES
('KRW', '(South) Korean Won'),
('AFA', 'Afghanistan Afghani'),
('ALL', 'Albanian Lek'),
('DZD', 'Algerian Dinar'),
('ADP', 'Andorran Peseta'),
('AOK', 'Angolan Kwanza'),
('ARS', 'Argentine Peso'),
('AMD', 'Armenian Dram'),
('AWG', 'Aruban Florin'),
('AUD', 'Australian Dollar'),
('BSD', 'Bahamian Dollar'),
('BHD', 'Bahraini Dinar'),
('BDT', 'Bangladeshi Taka'),
('BBD', 'Barbados Dollar'),
('BZD', 'Belize Dollar'),
('BMD', 'Bermudian Dollar'),
('BTN', 'Bhutan Ngultrum'),
('BOB', 'Bolivian Boliviano'),
('BWP', 'Botswanian Pula'),
('BRL', 'Brazilian Real'),
('GBP', 'British Pound'),
('BND', 'Brunei Dollar'),
('BGN', 'Bulgarian Lev'),
('BUK', 'Burma Kyat'),
('BIF', 'Burundi Franc'),
('CAD', 'Canadian Dollar'),
('CVE', 'Cape Verde Escudo'),
('KYD', 'Cayman Islands Dollar'),
('CLP', 'Chilean Peso'),
('CLF', 'Chilean Unidades de Fomento'),
('COP', 'Colombian Peso'),
('XOF', 'Communauté Financière Africaine BCEAO - Francs'),
('XAF', 'Communauté Financière Africaine BEAC, Francs'),
('KMF', 'Comoros Franc'),
('XPF', 'Comptoirs Français du Pacifique Francs'),
('CRC', 'Costa Rican Colon'),
('CUP', 'Cuban Peso'),
('CYP', 'Cyprus Pound'),
('CZK', 'Czech Republic Koruna'),
('DKK', 'Danish Krone'),
('YDD', 'Democratic Yemeni Dinar'),
('DOP', 'Dominican Peso'),
('XCD', 'East Caribbean Dollar'),
('TPE', 'East Timor Escudo'),
('ECS', 'Ecuador Sucre'),
('EGP', 'Egyptian Pound'),
('SVC', 'El Salvador Colon'),
('EEK', 'Estonian Kroon (EEK)'),
('ETB', 'Ethiopian Birr'),
('EUR', 'Euro'),
('FKP', 'Falkland Islands Pound'),
('FJD', 'Fiji Dollar'),
('GMD', 'Gambian Dalasi'),
('GHC', 'Ghanaian Cedi'),
('GIP', 'Gibraltar Pound'),
('XAU', 'Gold, Ounces'),
('GTQ', 'Guatemalan Quetzal'),
('GNF', 'Guinea Franc'),
('GWP', 'Guinea-Bissau Peso'),
('GYD', 'Guyanan Dollar'),
('HTG', 'Haitian Gourde'),
('HNL', 'Honduran Lempira'),
('HKD', 'Hong Kong Dollar'),
('HUF', 'Hungarian Forint'),
('INR', 'Indian Rupee'),
('IDR', 'Indonesian Rupiah'),
('XDR', 'International Monetary Fund (IMF) Special Drawing Rights'),
('IRR', 'Iranian Rial'),
('IQD', 'Iraqi Dinar'),
('IEP', 'Irish Punt'),
('ILS', 'Israeli Shekel'),
('JMD', 'Jamaican Dollar'),
('JPY', 'Japanese Yen'),
('JOD', 'Jordanian Dinar'),
('KHR', 'Kampuchean (Cambodian) Riel'),
('KES', 'Kenyan Schilling'),
('KWD', 'Kuwaiti Dinar'),
('LAK', 'Lao Kip'),
('LBP', 'Lebanese Pound'),
('LSL', 'Lesotho Loti'),
('LRD', 'Liberian Dollar'),
('LYD', 'Libyan Dinar'),
('MOP', 'Macau Pataca'),
('MGF', 'Malagasy Franc'),
('MWK', 'Malawi Kwacha'),
('MYR', 'Malaysian Ringgit'),
('MVR', 'Maldive Rufiyaa'),
('MTL', 'Maltese Lira'),
('MRO', 'Mauritanian Ouguiya'),
('MUR', 'Mauritius Rupee'),
('MXP', 'Mexican Peso'),
('MNT', 'Mongolian Tugrik'),
('MAD', 'Moroccan Dirham'),
('MZM', 'Mozambique Metical'),
('NAD', 'Namibian Dollar'),
('NPR', 'Nepalese Rupee'),
('ANG', 'Netherlands Antillian Guilder'),
('YUD', 'New Yugoslavia Dinar'),
('NZD', 'New Zealand Dollar'),
('NIO', 'Nicaraguan Cordoba'),
('NGN', 'Nigerian Naira'),
('KPW', 'North Korean Won'),
('NOK', 'Norwegian Kroner'),
('OMR', 'Omani Rial'),
('PKR', 'Pakistan Rupee'),
('XPD', 'Palladium Ounces'),
('PAB', 'Panamanian Balboa'),
('PGK', 'Papua New Guinea Kina'),
('PYG', 'Paraguay Guarani'),
('PEN', 'Peruvian Nuevo Sol'),
('PHP', 'Philippine Peso'),
('XPT', 'Platinum, Ounces'),
('PLN', 'Polish Zloty'),
('QAR', 'Qatari Rial'),
('RON', 'Romanian Leu'),
('RUB', 'Russian Ruble'),
('RWF', 'Rwanda Franc'),
('WST', 'Samoan Tala'),
('STD', 'Sao Tome and Principe Dobra'),
('SAR', 'Saudi Arabian Riyal'),
('SCR', 'Seychelles Rupee'),
('SLL', 'Sierra Leone Leone'),
('XAG', 'Silver, Ounces'),
('SGD', 'Singapore Dollar'),
('SKK', 'Slovak Koruna'),
('SBD', 'Solomon Islands Dollar'),
('SOS', 'Somali Schilling'),
('ZAR', 'South African Rand'),
('LKR', 'Sri Lanka Rupee'),
('SHP', 'St. Helena Pound'),
('SDP', 'Sudanese Pound'),
('SRG', 'Suriname Guilder'),
('SZL', 'Swaziland Lilangeni'),
('SEK', 'Swedish Krona'),
('CHF', 'Swiss Franc'),
('SYP', 'Syrian Potmd'),
('TWD', 'Taiwan Dollar'),
('TZS', 'Tanzanian Schilling'),
('THB', 'Thai Baht'),
('TOP', 'Tongan Paanga'),
('TTD', 'Trinidad and Tobago Dollar'),
('TND', 'Tunisian Dinar'),
('TRY', 'Turkish Lira'),
('UGX', 'Uganda Shilling'),
('AED', 'United Arab Emirates Dirham'),
('UYU', 'Uruguayan Peso'),
('USD', 'US Dollar'),
('VUV', 'Vanuatu Vatu'),
('VEF', 'Venezualan Bolivar'),
('VND', 'Vietnamese Dong'),
('YER', 'Yemeni Rial'),
('CNY', 'Yuan (Chinese) Renminbi'),
('ZRZ', 'Zaire Zaire'),
('ZMK', 'Zambian Kwacha'),
('ZWD', 'Zimbabwe Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `document_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_type`
--

CREATE TABLE `document_type` (
  `document_type_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_type`
--

INSERT INTO `document_type` (`document_type_id`, `document_name`, `merchant_id`, `status`) VALUES
(1, 'license', 1, 1),
(2, 'Insurance', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img class=\"alignnone size-full wp-image-434\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\" alt=\"iso_apporio\" width=\"232\" height=\"163\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(4, 3, 'Miles', '', 1, 4, 0, 0, 0, 0, 0, 0, '4', '100', '30', '1', '10', '1', '10'),
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(5, 3, 'Miles', '', 2, 6, 0, 0, 0, 0, 0, 0, '0', '0', '8000', '3', '5', '0', '15'),
(6, 3, 'Miles', '', 3, 7, 0, 0, 0, 0, 0, 0, '1', '30', '1', '4', '1', '1', '1'),
(7, 3, 'Miles', '', 4, 10, 0, 0, 0, 0, 0, 0, '4', '200', '40', '2', '15', '2', '13'),
(8, 3, 'Miles', '', 5, 5, 0, 0, 0, 0, 0, 0, '5', '75', '14', '1', '7', '1', '15'),
(19, 57, 'Miles', '', 1, 2, 0, 0, 0, 0, 0, 0, '1', '200', '2', '1', '2', '1', '1'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '80', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(21, 71, 'Miles', '', 2, 8, 0, 0, 0, 0, 0, 0, '100', '4', '10', '5', '1', '5', '5'),
(22, 57, 'Miles', '', 3, 10, 10, 15, 10, 5, 10, 30, '4', '100', '10', '5', '1', '5', '1'),
(24, 79, '', '', 2, 10, 0, 0, 0, 0, 0, 0, '1', '5', '0.7', '0', '1', '0', '0.7'),
(25, 79, '', '', 4, 15, 0, 0, 0, 0, 0, 0, '30', '0', '3', '0', '2', '0', '3'),
(26, 79, '', '', 5, 25, 0, 0, 0, 0, 0, 0, '0', '18', '2', '0', '2', '0', '4'),
(27, 77, '', '', 2, 15, 0, 0, 0, 0, 0, 0, 'km', '3', '4', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message` text NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating_customer`
--

CREATE TABLE `rating_customer` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating_driver`
--

CREATE TABLE `rating_driver` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_admin_status`) VALUES
(8, 'Apporio', '1', '10', 1),
(9, 'DUMMY', '4', '40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(13, 56, 3, 8, '20', 10, 10, 1),
(14, 56, 3, 9, '100', 20, 40, 1),
(15, 56, 5, 10, '500', 10, 20, 1),
(16, 56, 4, 8, '200', 10, 20, 1),
(18, 88, 4, 8, '6', 100, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(1, 'Portuguese', 1),
(2, 'English', 1),
(3, 'Punjabi', 1),
(4, 'Afrikaans', 1),
(5, 'Arabic', 1),
(6, 'Assamese', 1),
(7, 'Urdu', 1),
(8, 'Telugu', 1),
(9, 'Turkish', 1),
(10, 'French', 1),
(11, 'Bengali/Bangla', 1),
(12, 'Vietnamese', 1),
(13, 'Spanish', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`id`, `message_id`, `language_id`, `message`) VALUES
(1, 1, 2, 'Your account is currently unavailable'),
(2, 2, 2, 'Email address already exists'),
(3, 3, 2, 'Please check the email and password and try again'),
(4, 4, 2, 'Invalid Email address'),
(5, 5, 2, 'Cannot recognize your device'),
(6, 6, 2, 'Some Parameters are missing'),
(7, 7, 2, 'You are logged out'),
(8, 8, 2, 'Invalid inputs'),
(9, 9, 2, 'Driver not found'),
(10, 10, 2, 'Geo Location Updated'),
(11, 11, 2, 'Availability Updated'),
(12, 12, 2, 'Mode Updated'),
(13, 13, 2, 'Ride Accepted'),
(14, 14, 2, 'you are too late, this ride is booked.'),
(15, 15, 2, 'You cannot accept this ride.'),
(16, 16, 2, 'This ride is unavailable'),
(17, 17, 2, 'No reasons available to cancelling ride'),
(18, 18, 2, 'You cannot do this action'),
(19, 19, 2, 'Ride Cancelled'),
(20, 20, 2, 'Status Updated'),
(21, 21, 2, 'Ride not found'),
(22, 22, 2, 'Ride Started'),
(23, 23, 2, 'Ride Completed'),
(24, 24, 2, 'Request Sent'),
(25, 25, 2, 'your payment is pending'),
(26, 26, 2, 'amount received'),
(27, 27, 2, 'your billing amount paid successfully'),
(28, 28, 2, 'waiting for otp'),
(29, 29, 2, 'tips added successfully'),
(30, 30, 2, 'tips removed successfully'),
(31, 31, 2, 'No locations are available'),
(32, 32, 2, 'Records not available'),
(33, 33, 2, 'No countries are availbale'),
(34, 34, 2, 'Transaction Failed'),
(35, 35, 2, 'Transaction Successful'),
(36, 36, 2, 'wallet money settings are not available, Please try again later.'),
(37, 37, 2, 'Review options not found'),
(38, 38, 2, 'Your ratings submitted successfully'),
(39, 39, 2, 'Already you have submitted your ratings for this ride.'),
(40, 40, 2, 'Submitted ratings fields are not valid'),
(41, 41, 2, 'Your account is currently unavailable'),
(42, 42, 2, 'Success'),
(43, 43, 2, 'Invalid referral code'),
(44, 44, 2, 'This mobile number already registered'),
(45, 45, 2, 'You are Logged In successfully'),
(46, 46, 2, 'Your account has been inactivated'),
(47, 47, 2, 'Continue Signup Process'),
(48, 48, 2, 'Authentication Failed'),
(49, 49, 2, 'Successfully registered'),
(50, 50, 2, 'Registration Failure'),
(51, 51, 2, 'Kindly check your email'),
(52, 52, 2, 'Please enter the correct email and try again'),
(53, 53, 2, 'Sorry ! We do not provide services in your city yet.'),
(54, 54, 2, 'Maximum no used in your account'),
(55, 55, 2, 'Coupon Expired'),
(56, 56, 2, 'Unavailable Coupon'),
(57, 57, 2, 'Invalid Coupon'),
(58, 58, 2, 'Booking Request Sent'),
(59, 59, 2, 'No cabs available nearby'),
(60, 60, 2, 'ride confirmed'),
(61, 61, 2, 'Ride request rejected'),
(62, 62, 2, 'Ride request cancelled'),
(63, 63, 2, 'Payment completed'),
(64, 64, 2, 'Wallet amount used successfully'),
(65, 65, 2, 'Wallet Empty'),
(66, 66, 2, 'Pay your bill by cash'),
(67, 67, 2, 'This ride has been paid already'),
(68, 68, 2, 'Payment Initiated'),
(69, 69, 2, 'Payment method currently unavailable'),
(70, 70, 2, 'Mail sent'),
(71, 71, 2, 'Mail not sent'),
(72, 72, 2, 'Your ride has been successfully shared with'),
(73, 73, 2, 'Tracking records not available for this ride'),
(74, 74, 2, 'Location already exist in your favorite list'),
(75, 75, 2, 'Location added to favorite'),
(76, 76, 2, 'No records found for this location'),
(77, 77, 2, 'Updated successfully'),
(78, 78, 2, 'Location removed successfully'),
(79, 79, 2, 'No records found for in your favorite list'),
(80, 80, 2, 'User name changed successfully'),
(81, 81, 2, 'otp sent successfully'),
(82, 82, 2, 'User mobile number changed successfully'),
(83, 83, 2, 'Your current password is not matching'),
(84, 84, 2, 'Password should be at-least 6 characters'),
(85, 85, 2, 'Emergency contact updated successfully'),
(86, 86, 2, 'Emergency contact added successfully'),
(87, 87, 2, 'Sorry, You can not add your details'),
(88, 88, 2, 'Emergency contact is not available'),
(89, 89, 2, 'Sorry, You have not set emergency contact yet'),
(90, 90, 2, 'Contact deleted successfully'),
(91, 91, 2, 'Alert notification sent successfully'),
(92, 92, 2, 'Verification code has been sent to you'),
(93, 93, 2, 'Password changed successfully'),
(94, 94, 2, 'No countries are available'),
(95, 95, 2, 'Error in connection'),
(96, 96, 2, 'Invalid User'),
(97, 97, 2, 'Invalid Driver'),
(98, 98, 2, 'Invalid Ride'),
(99, 99, 2, 'Coupon code applied'),
(100, 100, 2, 'Invalid request'),
(101, 101, 2, 'This user does not exist'),
(102, 102, 2, 'Email id not matched in our records'),
(103, 103, 2, 'Password reset link has been sent to your email address.'),
(104, 104, 2, 'Your ride request confirmed'),
(105, 105, 2, 'your ride cancelled'),
(106, 106, 2, 'Driver arrived on your place'),
(107, 107, 2, 'mins'),
(108, 108, 2, 'km'),
(109, 109, 2, 'Available'),
(110, 110, 2, 'Unavailable'),
(111, 111, 2, 'Pay by Cash'),
(112, 112, 2, 'Use my wallet/money'),
(113, 113, 2, 'Yes'),
(114, 114, 2, 'First'),
(115, 115, 2, 'After'),
(116, 116, 2, 'per km'),
(117, 117, 2, 'per'),
(118, 118, 2, 'per min'),
(119, 119, 2, 'Ride time charges'),
(120, 120, 2, 'Peak time charges'),
(121, 121, 2, 'Mins ride time is FREE! Wait time is chargeable.'),
(122, 122, 2, ' ride time is FREE! Wait time is chargeable.'),
(123, 123, 2, 'Peak time charges may be applicable during high demand hours and will be conveyed during the booking.'),
(124, 124, 2, 'Night time charges may be applicable during the late night hours and will be conveyed during the booking.'),
(125, 125, 2, 'This enables us to make more cabs available to you.'),
(126, 126, 2, 'Service tax is payable in addition to ride fare.'),
(127, 127, 2, 'Night time charges'),
(128, 128, 2, 'Service Tax'),
(129, 129, 2, 'no cabs'),
(130, 130, 2, 'Note'),
(131, 131, 2, 'Peak time charges may apply. Service tax extra.'),
(132, 132, 2, 'Ride time rate post '),
(133, 133, 2, 'This is an approximate estimate. Actual cost and travel time may be different.'),
(134, 134, 2, 'Pay by Card'),
(135, 135, 2, 'Dear'),
(136, 136, 2, 'your'),
(137, 137, 2, 'You cannot make the payment for this trip now.'),
(138, 138, 2, 'This trip has been already ended'),
(139, 139, 2, 'Your trip has been started'),
(140, 140, 2, 'Driver already exist in your favourite list'),
(141, 141, 2, 'Driver added to favorite successfully!'),
(142, 142, 2, 'Favorite driver edited successfully!'),
(143, 143, 2, 'Driver not found in your favorite drivers list'),
(144, 144, 2, 'Driver unfavored successfully!'),
(145, 145, 2, 'You Can\'t apply this tips amount right now...'),
(146, 146, 2, 'Entered inputs are incorrect'),
(147, 147, 2, 'Sorry ! We can not able to fetch information'),
(148, 148, 2, 'Already Cancelled By You'),
(149, 149, 2, 'Already Cancelled By Driver'),
(150, 150, 2, 'This type of cab service is not available in your location'),
(151, 151, 2, 'This Service category not found'),
(152, 152, 2, 'You do not have a verified account, Contact us for more information'),
(153, 153, 2, 'You have a pending trip / transaction. Please tap to view. Without resolving this you will not get ride requests'),
(154, 154, 2, 'rider cancelled this ride'),
(155, 155, 2, 'driver cancelled this ride'),
(156, 156, 2, 'rider wants to pay by cash'),
(157, 157, 2, 'Request for pickup user'),
(158, 158, 2, 'Status Updated Successfully'),
(159, 159, 2, 'Cannot find your identity'),
(160, 160, 2, 'Payment Cancelled'),
(161, 161, 2, 'Network error, Please try again'),
(162, 162, 2, 'You can book ride only after one hour from now'),
(163, 163, 2, 'on'),
(164, 164, 2, 'Booking for'),
(165, 165, 2, 'Referral reward'),
(166, 166, 2, 'Welcome bonus'),
(167, 167, 2, 'Booked'),
(168, 168, 2, 'Accepted'),
(169, 169, 2, 'Cancelled'),
(170, 170, 2, 'Completed'),
(171, 171, 2, 'Awaiting Payment'),
(172, 172, 2, 'On Ride'),
(173, 173, 2, 'Paid'),
(174, 174, 2, 'Pending'),
(175, 175, 2, 'Kilometer'),
(176, 176, 2, 'mi'),
(177, 177, 2, 'Miles'),
(178, 178, 2, 'min'),
(179, 179, 2, 'mins'),
(180, 180, 2, 'Your account have not been activated yet'),
(181, 181, 2, 'Your account has been modified, please login to again.'),
(182, 182, 2, 'Already Location Arrived'),
(183, 183, 2, 'Already Ride Started'),
(184, 184, 2, 'Failed to update'),
(185, 185, 2, 'Invalid language code'),
(186, 186, 2, 'trip'),
(187, 187, 2, 'trips'),
(188, 188, 2, 'Peak time surcharge'),
(189, 189, 2, 'Your emergency contacts are not verified yet'),
(190, 190, 2, 'Profile picture updated successfully'),
(191, 191, 2, 'Error in updating profile picture'),
(192, 192, 2, 'Already this ride has been cancelled'),
(193, 193, 2, 'Recharge'),
(194, 194, 2, 'Base fare'),
(195, 195, 2, 'Peak time fare'),
(196, 196, 2, 'Subtotal'),
(197, 197, 2, 'Discount amount'),
(198, 198, 2, 'Grand Total'),
(199, 199, 2, 'Tips amount'),
(200, 200, 2, 'Wallet used amount'),
(201, 201, 2, 'No categories available in this location'),
(202, 202, 2, 'Fare details are not updated for this category'),
(203, 203, 2, 'Paid Amount'),
(204, 204, 2, 'Your account did not exist'),
(205, 205, 2, 'You have a new trip'),
(206, 206, 2, 'Driver current ride location'),
(207, 207, 2, 'Searching for a driver'),
(208, 208, 2, 'Request Acknowledged'),
(209, 209, 2, 'Request Denied Successfully'),
(210, 210, 2, 'Reporter details not found'),
(211, 211, 2, 'Your report has been submitted successfully'),
(212, 212, 2, 'You have not sent any reports yet'),
(213, 213, 2, 'Peak and Night time charges'),
(214, 214, 2, 'will be applied'),
(215, 215, 2, 'This number has been already added in your emergency contact, so you cannot update this number'),
(216, 216, 2, 'I have an {SITENAME} coupon worth {CURRENCY_CODE} {WELCOME_AMOUNT} for you. Sign up with my referral code {UNIQUE_CODE} for more info visit :'),
(217, 217, 2, 'app invitation'),
(218, 218, 2, 'Email id does not match our records'),
(219, 219, 2, 'Please wait, we will call you back'),
(220, 220, 2, 'Number is unverified'),
(221, 221, 2, 'Call not allowed'),
(222, 222, 2, 'You cannot make a call now'),
(223, 223, 2, 'SMS sent successfully'),
(224, 224, 2, 'You cannot send a sms now'),
(225, 225, 2, 'Your Trip has been completed'),
(226, 226, 2, 'Currently you can\'t able to service in this location'),
(227, 227, 2, 'Location name already exist in your favourite list'),
(229, 1, 1, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Neyber || Website', '2017 Neyber', 'uploads/website/banner_1501853840.jpg', 'MOBILE APP', 'Why Choose Neyber for Taxi Hire', 'uploads/website/app_screen11501854156.png', 'uploads/website/app_screen21501854156.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501854156.png', 'https://play.google.com/', 'uploads/website/itunes_btn1501854156.png', 'https://itunes.apple.com/', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501854156.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501854156.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501854156.png', 'Why Choose', 'Neyber for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/parallax_screen1501854233.png', 'uploads/website/parallax_bg1501854233.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501854233.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501854233.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`iso`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`document_type_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rating_customer`
--
ALTER TABLE `rating_customer`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `rating_driver`
--
ALTER TABLE `rating_driver`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_type`
--
ALTER TABLE `document_type`
  MODIFY `document_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rating_customer`
--
ALTER TABLE `rating_customer`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rating_driver`
--
ALTER TABLE `rating_driver`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
