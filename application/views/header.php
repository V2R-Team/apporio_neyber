<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

<meta name="description" content="izycab">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Apporio Taxi</title>
<link rel="stylesheet" href="<?php echo base_url();?>/css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/font-awesome.min.css" type="text/css">
<!--<link rel="stylesheet" href="css/icomoon.css" type="text/css">-->

</head>
<body>
<div id="wrapper">
  <header class="header">
    <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>  
        <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/signin">Sign in</a></li>
        <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/register">Register</a></li>
       <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/about">About us</a></li>
        <li><a href="#">Terms & Conditions</a></li>
        <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>      
          
          
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
              <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
              <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>
      </ul>
    </div>
   
    <div class="navigation-row">
      <div class="container">
        <div class="row">
		  <div class="col-md-3 col-sm-6 col-xs-4"><strong class="logo"> <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/logo.png" alt=""></a> </strong></div>
          <div class="col-md-9 col-sm-6 col-xs-8">
            <div class="topbar">
               <ul class="top-listed">
                 <?php if($this->session->userdata('user') == ""){?>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/signin">Sign in</a></li>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/register">Register</a></li>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>   
                 <?php  } else if($this->session->userdata('user') == "driver"){ ?>
                 
                 <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li><a href="<?php echo base_url();?>Welcome/driver_profile">Dashboard</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>   
                 
                  <?php } else { ?>
                  
                   <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>  
                  
                   <?php } ?>
                  
              <!--  <li class="hidden-xs">
                    <select>
                      <option value="english">English</option>
                      <option value="hindi">Hindi</option>
                      <option value="french">French</option>
                    </select>
                </li>-->
                <?php if($this->session->userdata('user') == ""){?>
                <li><span class="top_bcmdri_btn"><a href="<?php echo base_url();?>Welcome/driver_signup" >
                <button>BECOME A DRIVER</button> </a></span> 
                  <?php } else { ?>
                <li class=""><a href="<?php echo base_url();?>Welcome/logout">Log Out<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
                <?php } ?>
                </li>
              </ul>
                
              
              </div>
          </div>
        </div>
        
      </div>
    </div>
  </header>