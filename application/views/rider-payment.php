<?php include('header_rider.php'); ?>

  <section class=" mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
             <ul class="nav tabs-vertical left_tab">
              <li class="rider_profile"> <img src="images/profile.png"/>
                <div class="clear"></div>
                <span class="rider_name">Your Name</span> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a> </li>
              <li class="active"><a href="<?php echo base_url();?>Welcome/rider_trips">My Trips<i class="fa fa-car" aria-hidden="true"></i></a> </li>
               <li class=""> <a href="<?php echo base_url();?>Welcome/rider_later">Book Later<i class="fa fa-car"></i></a></li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_change_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="rider-payment.php">Payment<i class="fa fa-money" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-coupons.php">Coupons Applied<i class="fa fa-at" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
            
            
            
            <div class="" id="">
              <h4 class="pb-20">Payment
                <!--<button class="btn btn-icon btn-info  edit_profile"> <i class="fa fa-pencil" data-toggle="modal" data-target="#paymentModal"></i> </button>-->
              </h4>
              
              <strong class="payment_method_heading">Payment Methods</strong>
              <div class="col-md-12 payment_pane"><img src="images/payment-visa.png" width="40"/> Personal Visa •••• 0247<span>Expires 01/2020</span></div>
              <div class="col-md-12 payment_pane mt-20"><img src="images/cash.png" width="40"/> Cash ••••</div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!--<div class="modal fade" id="paymentModal" role="dialog">
  <div class="modal-dialog"> 
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Payment Method</h4>
      </div>
      <div class="modal-body payment_method_modal">
        <form>
          <div class="col-md-12">
            <label class="">
              <input type="radio" checked name="pymtmethod">
              Cash </label>
            <label class="">
              <input type="radio" name="pymtmethod">
              Paypal </label>
            <label class="">
              <input type="radio" name="pymtmethod">
              Credit Card </label>
          </div>
          <div class="form-group col-md-6 mt-20">
            <input type="submit" class="btn btn-info pt-10 pb-10" style="width:100%;" value="Save">
          </div>
          <div class="col-md-6"></div>
        </form>
      </div>
      <div class="clear"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>-->
<?php include('footer.php'); ?>
