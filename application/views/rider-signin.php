<?php if($this->session->flashdata('Wrong')):?>
 <script>alert("Wrong Email or Password!!");</script>
<?php endif; ?>

<?php include('header.php'); ?>

<script>
function validateForm() {
    var x = document.forms["myForm"]["email"].value;
	var y = document.forms["myForm"]["pass"].value;

	 
    if (x == "") {
        alert("Enter email First");
        return false;
    }
	 else if (y == "") {
        alert("Enter Password Please");
        return false;
    }

}
</script>

<section class="mt-80 mb-30">
<div class="container">
<div class="mt-60 pb-20">
    <div class="col-md-5">
        <h2 class="signin_heading mb-30">Rider Sign In</h2>
         <form name="myForm" action="<?php echo base_url();?>Useraccounts/rider_signin" onsubmit="return validateForm()" method="post">
        <div class="form-group">
            <input type="text" class="form-control newsignup" id="email" name="email" placeholder="Enter your email or mobile number" required="">
        </div>
            
        <div class="form-group">
            <input type="password" class="form-control newsignup" id="pass" name="pass" placeholder="Password" required="">
        </div>
        
        <div class="form-group">
            <input type="submit" class="submit_btn" width="100%" value="NEXT">
        </div>
        
         
        <span class="col-md-12 tc_pp"><a href="<?php echo base_url();?>Welcome/forget_update">Forget Password</a> OR Don't have an account? <a href="<?php echo base_url();?>Welcome/rider_signup">Sign up</a></span>
       <!-- <h4 class="col-md-12 or_">OR</h4>
        
        <button class="fb_btn"><i class="fa fa-facebook" aria-hidden="true"></i>Log in with Facebook</button>
            
        <button class="google_btn"><i class="fa fa-google-plus" aria-hidden="true"></i>Sign in with Google+</button>-->
        </form>
    </div>
    <div class="col-md-1"></div>
    
    <div class="col-md-6 pt-50 driver_signin"><img src="<?php echo base_url();?>images/driver-signin.jpg"/></div>
    <div class="clear"></div>
</div> 
</div>
</section>
 

<?php include('footer.php'); ?>