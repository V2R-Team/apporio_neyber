 <section><img src="<?php echo base_url();?>images/footer.png"/></section>
  </div>
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/about">About Us</a></li>
                  <!--<li><a href="#">Blog</a></li>-->
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact Us</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <p class="copyright"> © 2017 Apporio Taxi.</p>
          </div>
        </div>
      </div>
    </section>
  </footer>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/js/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/js/custom-script.js"></script>
</body>

</html>