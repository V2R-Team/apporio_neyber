<?php include('header.php'); ?>

  <section class="mt-80 mb-100 main_sect">
  <div class="container">
    <div class="mt-60 pt-20 pb-20 profile_content">
        <div class="col-md-2"></div>
        <div class="col-md-3 signin_pg">
          <h3>Driver</h3>
          <p>Find everything you need to track your success on the road.</p>
           
          <a href="<?php echo base_url();?>Welcome/driver_signin">
          <button>Driver Sign in<i class="fa fa-angle-right" aria-hidden="true"></i></button>
          </a> </div>
        <div class="col-md-2"></div>
        <div class="col-md-3 signin_pg">
          <h3>Rider</h3>
          <p>Manage your payment options, review trip history, and more.</p>
           <a href="<?php echo base_url();?>Welcome/rider_signin">
          <button>Rider Sign in<i class="fa fa-angle-right" aria-hidden="true"></i></button>
          </a> </div>
        <div class="col-md-2"></div>
      </div>
  </div>  
</section>
</div>
<?php include('footer.php'); ?>