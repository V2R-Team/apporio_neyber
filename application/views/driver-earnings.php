<?php include('header_driver.php'); ?>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4" src="images/profile.png"/>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name">Apporio</div>
                  <div class="driver_car_name">Creta</div>
                  <div class="driver_car_number">DK 12333</div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
                <button class="btn btn-success btn-xs online_offline">Online</button>
                <div class="clear"></div>
              </li>
               <li class="active"> <a href="<?php echo base_url();?>Welcome/driver_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>Welcome/driver_rides">My Rides<i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/driver_documents">Document's<i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>Welcome/driver_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>Welcome/driver_suppourt">Customer Support<i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>Welcome/about">About us <i class="fa fa-info" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
            
            <div class="" id="">
              <h4 class="pb-30">Daily Earnings</h4>
              <div class="col-md-4">
                <div class="earnings_div">
                  <label> Mardi, 25 Avril </label>
                  <div class="total_rides">
                    <div class="col-md-6 padding_none">Total Rides :</div>
                    <div class="col-md-6 padding_none text_right"> 1 </div>
                    <div class="clear"></div>
                  </div>
                  <div class="total_earning">
                    <div class="col-md-6 padding_none"> Total Earnings  : </div>
                    <div class="col-md-6 padding_none text_right"> <i class="fa fa-usd" aria-hidden="true"></i> 5 </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="earnings_div">
                  <label> Vendredi, 28 Avril </label>
                  <div class="total_rides">
                    <div class="col-md-6 padding_none"> Total Rides  : </div>
                    <div class="col-md-6 padding_none text_right">1</div>
                    <div class="clear"></div>
                  </div>
                  <div class="total_earning">
                    <div class="col-md-6 padding_none"> Total Earnings :</div>
                    <div class="col-md-6 padding_none text_right"> <i class="fa fa-usd" aria-hidden="true"></i> 5 </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
              <div class="col-md-6"></div>
            </div>
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>
