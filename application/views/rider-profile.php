 <?php  foreach($profile as $profile){ 
                              $user_id=$profile['user_id'];
                              $user_name= $profile['user_name'];
                              $data=(explode(" ",$user_name));
                              $first_name=$data['0'];
                              $last_name=$data['1'];
                              $user_email= $profile['user_email'];
                              if($user_email != "")
                              {
                              $user_email = $user_email; 
                              }
                              else
                              {
                              $user_email = ""; 
                              }
                              $user_mobile= $profile['user_phone'];
                              $user_image= $profile['user_image'];
				}
				
				
				?>
<?php if($this->session->flashdata('success')):?>
     <script>alert("Profile Succesfully Updated!!");</script>
<?php endif; ?>	
<?php include('header_rider.php'); ?>

  <section class=" mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
             <?php if ($user_image == "") { ?>
              <li class="rider_profile"> <img src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="rider_profile"> <img src="<?php echo base_url($user_image); ?>">
                <?php } ?>
                
             
                <div class="clear"></div>
               <span class="rider_name"><?php echo $user_name?></span> </li>
               <li class=""><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_trips">My Trips<i class="fa fa-car" aria-hidden="true"></i></a> </li>
               <li class=""> <a href="<?php echo base_url();?>Welcome/rider_later">Book Later<i class="fa fa-car"></i></a></li>
              <li class="active"><a href="<?php echo base_url();?>Welcome/rider_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_change_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="rider-payment.php">Payment<i class="fa fa-money" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-coupons.php">Coupons Applied<i class="fa fa-at" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
           
            
            <div class="" id="">
              <h4 class="">General Information
                <button class="btn btn-icon btn-info  edit_profile"> <i class="fa fa-pencil" data-toggle="modal" data-target="#<?php echo $user_id ?>"></i> </button>
              </h4>
              <div class="profile_details">
                <div class="col-md-6"><strong class="headstrg">First Name : </strong>  <?php echo $first_name; ?></div>
                <div class="col-md-6"><strong class="headstrg">Last Name :</strong>  <?php echo $last_name; ?></div>
              </div>
              <div class="profile_details">
                <div class="col-md-6"><strong class="headstrg">Email : </strong> <?php echo $user_email; ?></div>
                <div class="col-md-6"><strong class="headstrg">Mobile :</strong><?php echo $user_mobile; ?></div>
              </div>
            <!--  <div class="profile_details">
                <div class="col-md-6"><strong class="headstrg">Wallet Balance : </strong> $ 20.00</div>
              </div> -->
              <div class="clear"></div>
            </div>
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade" id="<?php echo $user_id ?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Information</h4>
      </div>
      <div class="modal-body">
         
        <?php echo form_open_multipart('Useraccounts/update_rider_profile',['name'=>'update_pro']) ?>
          <div class="form-group modal_edit col-md-6">
            <label>First Name</label>
            <input type="text" class="form-control edit_profile_field" value="<?php echo $first_name; ?>" name="fname" placeholder="First Name"  required="">
          </div>
          <div class="form-group modal_edit col-md-6">
            <label>Last Name</label>
            <input type="text" class="form-control edit_profile_field" value="<?php echo $last_name; ?>" name="lname" placeholder="Last Name" >
          </div>
          <div class="form-group modal_edit col-md-6">
            <label>Email Address</label>
            <input type="email" class="form-control edit_profile_field" value="<?php echo $user_email; ?>"  name="email" placeholder="Email Address" >
          </div>
          <div class="form-group modal_edit col-md-6">
            <label>Mobile Number</label>
            <input type="text" class="form-control edit_profile_field" value="<?php echo $user_mobile; ?>" name="mobile" placeholder="Mobile Number"  required="">
          </div>
          <div class="clear"></div>
          <div class="form-group col-md-6 mt-20">
            <input type="submit" class="btn btn-info pt-10 pb-10" style="width:100%;" value="Save">
          </div>
          <div class="col-md-6"></div>
        </form>
      </div>
      <div class="clear"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php include('footer.php'); ?>
