<?php foreach($profile as $profile){ 
                              $user_id=$profile['user_id'];
                              $user_name= $profile['user_name'];
                              $data=(explode(" ",$user_name));
                              $first_name=$data['0'];
                              $last_name=$data['1'];
                              $user_email= $profile['user_email'];
                              $user_mobile= $profile['user_phone'];
                              $user_image= $profile['user_image'];
			           }
				?>
 <?php if($this->session->flashdata('success')):?>
     <script>alert("Password Succesfully Changed!!");</script>
<?php endif; ?>

 <?php if($this->session->flashdata('fail')):?>
     <script>alert("Incorrect Password!!");</script>
<?php endif; ?>
<?php include('header_rider.php'); ?>

<script>
function validateForm() {
        var x = document.forms["myForm"]["old_password"].value;
	var y = document.forms["myForm"]["new_password"].value;
	var z = document.forms["myForm"]["confirm_password"].value;
 
	 
	 
    if (x == "") {
        alert("Please Enter Old Password");
        return false;
    }
    else if (y == "") {
        alert("Please Enter New Password");
        return false;
    }
    else if (z == "") {
        alert("Please Enter Confirm Password");
        return false;
    }
	
}
</script>

  <section class=" mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($user_image == "") { ?>
              <li class="rider_profile"> <img src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="rider_profile"> <img src="<?php echo base_url($user_image); ?>">
                <?php } ?>
                <div class="clear"></div>
                <span class="rider_name"><?php echo $user_name?></span> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_trips">My Trips<i class="fa fa-car" aria-hidden="true"></i></a> </li>
               <li class=""> <a href="<?php echo base_url();?>Welcome/rider_later">Book Later<i class="fa fa-car"></i></a></li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="active"><a href="<?php echo base_url();?>Welcome/rider_change_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="rider-payment.php">Payment<i class="fa fa-money" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-coupons.php">Coupons Applied<i class="fa fa-at" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
              </ul>
             </div>
          <div class="tab-content col-md-9 tab_content">
            
            
            
            <div class="" id="">
              <h4 class="pb-30">Change Password</h4>
              <div class="col-md-6">
                 
                  <form name="myForm" action="<?php echo base_url();?>Useraccounts/update_rider_password" onsubmit="return validateForm()" method="post">
                  <div class="form-group edit_profile_label">
                    <label>Old Password</label>
                    <input type="password"   class="form-control edit_profile_field" name="old_password" id="old_password"  placeholder="Old password" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <label>New Password</label>
                    <input type="password"   class="form-control edit_profile_field" name="new_password" id="new_password"  placeholder="New password" required>
                    <?php echo form_error('new_password', '<div class="error">', '</div>');?>
                  </div>
                  <div class="form-group edit_profile_label">
                    <label>Confirm Password</label>
                    <input type="password"   class="form-control edit_profile_field" name="confirm_password" id="confirm_password"  placeholder="Confirm password" required>
                    
                  </div>
                  <div class="form-group">
                    <input type="submit" class="submit_btn" style="width:100%;" value="Change Password">
                  </div>
                </form>
              </div>
              <div class="col-md-6"></div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>
