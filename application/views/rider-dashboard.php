<?php if($this->session->flashdata('msg')):?>
     <script>alert("Login Succesful!!");</script>
<?php endif; ?>
 <?php if($this->session->flashdata('Bookig_success')):?>
     <script>alert("Booking Succesfully Added!!");</script>
<?php endif; ?>
<?php if($this->session->flashdata('Bookig_Error')):?>
     <script>alert("Error While Adding Booking!!");</script>
<?php endif; ?>
 <?php foreach($profile as $profile){ 
      
                                    $user_name= $profile['user_name'];
                                    $user_email= $profile['user_email'];
                                    $user_mobile= $profile['user_phone'];
                                    $user_image= $profile['user_image'];
				    }
				    ?>
			 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
function initialize() {

var input = document.getElementById('pick_up');

var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
function initialize() {

var input = document.getElementById('drop_loc');

var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>				

<?php include('header_rider.php'); ?>
<script>
function validateForm() {
    var x = document.forms["myForm"]["pick_up"].value;
	var y = document.forms["myForm"]["drop_loc"].value;
	var z = document.forms["myForm"]["car_type_id"].value;
	var a = document.forms["myForm"]["min-date"].value;
	 
	 
    if (x == "") {
        alert("Please Enter Pick Up Location");
        return false;
    }
	 else if (y == "") {
        alert("Please Enter Drop Location");
        return false;
    }
	else if (z == "") {
        alert("Please Select Car Type");
        return false;
    }
	else if (a == "") {
        alert("Please Select Date and Time");
        return false;
    }

	
	

}
</script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<script type="text/javascript" src="<?php echo base_url();?>/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>/css/material.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>/css/bootstrap-material-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>/js/material.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>/js/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap-material-datetimepicker.js"></script>
  


  <section class=" mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($user_image == "") { ?>
              <li class="rider_profile"> <img src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="rider_profile"> <img src="<?php echo base_url($user_image); ?>">
                <?php } ?>
                <div class="clear"></div>
                <span class="rider_name"><?php echo $user_name?></span> </li>
               <li class="active"><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_trips">My Trips<i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>Welcome/rider_later">Book Later<i class="fa fa-car"></i></a></li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class=""><a href="<?php echo base_url();?>Welcome/rider_change_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="rider-payment.php">Payment<i class="fa fa-money" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-coupons.php">Coupons Applied<i class="fa fa-at" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            <div class="" id="">
              <h4 class="pb-30">My Dashboard</h4>
              <div class="row">
                <div class="col-md-6">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3509.1787981602233!2d77.03998981434013!3d28.413861300696546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d229a97e4d409%3A0xaee2b4134ef269a1!2sApporio+Infolabs+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1493624828531" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-6">
                  <h4 class="pb-20">Ride Later</h4>
                 <form name="myForm" action="<?php echo base_url();?>Useraccounts/rider_book_later" onsubmit="return validateForm()" method="post">
                 
                    <div class="form-group">
                      <input type="text" class="form-control newsignup" id="pick_up" name="pick_up" placeholder="Enter pickup Location" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control newsignup" id="drop_loc" name="drop_loc"  placeholder="Enter Drop Location" required>
                    </div>
                    <div class="form-group">
                    
                    
                      <div class="form-group">
           		  
	                <select name="car_type_id" id="car_type_id"    class="form-control" required="">
                        <option value="">-------- Please Select Car Type --------</option>
	               <?php foreach($car_type as $model){ ?>
	               <option value="<?php echo $model['car_type_id'];?>"><?php echo $model['car_type_name'];?></option>         
	               <?php } ?>
	             </select>          
	          </div>
	          
	          
                    <div class="form-group">
		       <input type="text" id="min-date" name="min-date"  class="form-control newsignup" placeholder="Date" required="">
                    </div>
                    
                    
                      <input type="submit" class="submit_btn" style="width:100%;" value="RIDE NOW">
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
            <script type="text/javascript">
		$(document).ready(function()
		{
			$('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });

			$.material.init()
		});
		</script>
            
           
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


  <section><img src="<?php echo base_url();?>images/footer.png"/></section>
  </div>
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/about">About Us</a></li>
                  <li><a href="#">Blog</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact Us</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <p class="copyright"> © 2017 Apporio Taxi.</p>
          </div>
        </div>
      </div>
    </section>
  </footer>
</div>
 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/js/custom-script.js"></script>
</body>

</html>