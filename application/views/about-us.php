<?php include('header.php'); ?>

<div class="inner-banner">
 
<img src="<?php echo base_url();?>/images/about.jpg" alt="">
 
<div class="inner-banner-holder">
<div class="container">
<h2><?php echo $web_about->title?> </h2>
 
</div>
</div> 
</div>
 
<div id="main-content">
 
<section class="pt-50 pb-100">
<div class="container">
<p><?php echo nl2br($web_about->description);?> </p>    
</div>
</section> 
</div> 
<?php include('footer.php'); ?>