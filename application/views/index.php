<?php
if($this->session->flashdata('pass_success')):?>
 <script>alert("Please Check Your Registered Email Address!!");</script>
<?php endif; ?>
<?php if($this->session->flashdata('pass_fail')):?>
 <script>alert("Email Id is Not registered with Us..");</script>
<?php endif; ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="Scootaride">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $web_home->web_title;?></title>

<link rel="stylesheet" href="<?php echo base_url();?>/css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/font-awesome.min.css" type="text/css">

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/owlcarousel/owl.theme.default.min.css">

<!-- javascript -->
<script src="<?php echo base_url();?>/js/jquery-1.11.3.min.js"></script>
    <script src="<?php echo base_url();?>/assets/owlcarousel/owl.carousel.min.js"></script>
<style>
.owl-nav {
	display: none;
}
</style>
</head>
<body>
<div id="wrapper">
  <header class="header">
    <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
         <li><a href="<?php echo base_url();?>">Hom e</a></li>
        <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/signin">Sign in</a></li>
         <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/register">Register</a></li>
        <li><a href="about-us.php">About us</a></li>
        <li><a href="terms.php">Terms & Conditions</a></li>
        <li><a href="contact-us.php">Contact us</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
            <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="navigation-row">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-4"><strong class="logo"> <a href="index.php"><img src="<?php echo base_url();?>/images/logo.png" alt=""></a> </strong></div>
          <div class="col-md-9 col-sm-6 col-xs-8">
            <div class="topbar">
              <ul class="top-listed">
                 <?php if($this->session->userdata('user') == ""){?>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/signin">Sign in</a></li>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/register">Register</a></li>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>   
                 <?php  } else if($this->session->userdata('user') == "driver"){ ?>
                 
                 <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li><a href="<?php echo base_url();?>Welcome/driver_profile">Dashboard</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>    
                  <?php } else { ?>
                  
                   <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/rider_dashboard">Dashboard</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>Welcome/contact_us">Contact us</a></li>  
                   
                  
                   <?php } ?>
              <!--  <li class="hidden-xs">
                  <select>
                    <option value="english">English</option>
                    <option value="hindi">Hindi</option>
                    <option value="french">French</option>
                  </select>
                </li>-->
                 <?php if($this->session->userdata('user') == ""){?>
                <li><span class="top_bcmdri_btn"><a href="<?php echo base_url();?>Welcome/driver_signup" >
                <button>BECOME A DRIVER</button> </a></span> 
                  <?php } else { ?>
                <li class=""><a href="<?php echo base_url();?>Welcome/logout">Log Out<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
                <?php } ?>
                
                  
                 </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="banner"  style="background-image:url(<?php echo base_url().$web_home->banner_img;?>)">
    <div class="banner-caption">
      <div class="container">
        <div class="banner-inner-holder" style="float:left;">
         <?php if($this->session->userdata('user') == ""){?>
          <h2>Find your way..</h2>
            
           <a href="<?php echo base_url();?>Welcome/driver_signup" class="become_driver">SIGN UP TO DRIVER &nbsp; <i class="fa fa-car" aria-hidden="true"></i></a> <br>
           <a href="<?php echo base_url();?>Welcome/rider_signup" class="become_rider">SIGN UP TO RIDER &nbsp; <i class="fa fa-car" aria-hidden="true"></i></a> </div>
        <?php  } else{ ?>
        <h2>Find your way..</h2>
         <a href=""  > <i   aria-hidden="true"></i></a> <br>
           <a href=""  > <i   aria-hidden="true"></i></a> </div>
        
        <?php } ?>
      </div>
    </div>
  </div>
  <div id="main-content">
    <section class="mobile-app-section pt-20 pb-40">
      <div class="container">
        <div class="heading-style1">
          <h2><?= $web_home->app_heading;?></h2>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="app-thumb"> <img class="app-img1" height="460" width="190" src="<?php echo base_url().$web_home->app_screen1;?>" alt="App Screen"> <img class="app-img2" src="<?php echo base_url().$web_home->app_screen2;?>" height="460" width="190" alt="App Screen"> </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="app-text">
              <h4><?= $web_home->app_heading1;?></h4>
              <p><?php echo nl2br($web_home->app_details);?> </p>
              <strong><?= $web_home->market_places_desc;?></strong>
              <ul class="app-btn">
                <li><a target="_blank" href="<?= $web_home->google_play_url;?>"><img src="<?php echo base_url().$web_home->google_play_btn;?>" height="39" width="132" alt=""></a></li>
                <li><a target="_blank" href="<?= $web_home->itunes_url;?>"><img src="<?php echo base_url().$web_home->itunes_btn;?>" height="39" width="132" alt=""></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="taxi-section pt-40 pb-40">
      <div class="container">
        <div class="tabs-holder">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tab-01">
              <div class="row">
                <div class="col-md-4 col-sm-6">
                  <article class="taxi-holder">
                    <figure class="about_apporio"> <img src="<?php echo base_url().$web_home->heading1_img;?>" height="122" width="120" alt=""> </figure>
                    <div class="text">
                      <h3><?php echo $web_home->heading1;?></h3>
                      <ul class="meta-listed">
                        <p><?php echo nl2br($web_home->heading1_details);?></p>
                      </ul>
                    </div>
                  </article>
                </div>
                <div class="col-md-4 col-sm-6">
                  <article class="taxi-holder">
                    <figure class="about_apporio"> <img src="<?php echo base_url().$web_home->heading2_img;?>" height="122" width="120" alt=""> </figure>
                    <div class="text">
                      <h3><?php echo $web_home->heading2;?></h3>
                      <ul class="meta-listed">
                        <p><?php echo nl2br($web_home->heading2_details);?></p>
                      </ul>
                    </div>
                  </article>
                </div>
                <div class="col-md-4 col-sm-6">
                  <article class="taxi-holder">
                    <figure class="about_apporio"> <img src="<?php echo base_url().$web_home->heading3_img;?>" height="122" width="120" alt=""> </figure>
                    <div class="text">
                      <h3><?php echo $web_home->heading3;?></h3>
                      <ul class="meta-listed">
                        <p><?php echo nl2br($web_home->heading3_details);?></p>
                      </ul>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="why-choose-section" style="background: url(<?php echo base_url().$web_home->parallax_bg;?>) top left no-repeat;">
      <div class="container">
        <div class="why-choose-text col-md-7 pt-80 pb-80">
          <h3 style="color: white;"><?= $web_home->parallax_heading1;?></h3>
          <h2> <?= $web_home->parallax_heading2;?> </h2>
          <p><?php echo nl2br($web_home->parallax_details); ?></p>
          <a href="#" class="whats_new">
          </a> </div>
        <div class="col-md-5 right_mb pt-20 pb-20"><img class="app-img1" height="460" width="190" src="<?php echo base_url().$web_home->parallax_screen;?>" alt=""> </div>
      </div>
    </section>
    <div class="clear"></div>
    <section class="pt-100 pb-50">
      <div class="container">
        <ul class="why-choose-listed">
          <li class="col-md-6">
            <div class="box"> <img src="<?php echo base_url().$web_home->features1_bg;?>" alt="">
              <h3><?= $web_home->features1_heading;?></h3>
              <i class="fa fa-building-o fa-3x" aria-hidden="true"></i>
              <p> <?php echo nl2br($web_home->features1_desc);?> </p>
              <!--<a href="#" class="readmore">SEE THE IMPACT <i class="fa fa-angle-right" aria-hidden="true"></i></a> </div>-->
          </li>
          <li class="col-md-6">
            <div class="box"> <img src="<?php echo base_url().$web_home->features2_bg;?>" alt="">
              <h3><?= $web_home->features2_heading;?></h3>
              <i class="fa fa-car fa-3x" aria-hidden="true"></i>
              <p> <?php echo nl2br($web_home->features1_desc);?></p>
              <!--<a href="#" class="readmore">SEE THE IMPACT<i class="fa fa-angle-right" aria-hidden="true"></i></a> </div>-->
          </li>
        </ul>
      </div>
    </section>
    <section><img src="<?php echo base_url();?>images/footer.png"/></section>
  </div>
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url();?>Welcome/about">About Us</a></li>
                <!--<li><a href="#">Blog</a></li>-->
                <li><a href="<?php echo base_url();?>Welcome/contact_us">Contact Us</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <p class="copyright"> © <?= $web_home->web_footer; ?>.</p>
          </div>
        </div>
      </div>
    </section>
  </footer>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
</body>
</html>