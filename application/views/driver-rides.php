<?php include('header_driver.php'); ?>
  <style>
.panel-group .panel .panel-heading1 a[data-toggle=collapse]:before, .panel-group .panel .panel-heading1 .accordion-toggle:before {
	padding-top: 15px;
}
</style>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($driver_image == "") { ?>
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4"  src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="driver_profile"> <img    class="col-md-4 col-sm-8 col-xs-8" src="<?php echo base_url($driver_image); ?>">
                <?php } ?>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name"><?php echo $driver_name?></div>
                  <div class="driver_car_name"><?php echo $car_model_name?></div>
                  <div class="driver_car_number"><?php echo $car_number?></div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
                <button class="btn btn-success btn-xs online_offline">Online</button>
                <div class="clear"></div>
              </li>
               <li class=""> <a href="<?php echo base_url();?>Welcome/driver_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="active">  <a href="<?php echo base_url();?>Welcome/driver_rides">My Rides<i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>Welcome/driver_documents">Document's<i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>Welcome/driver_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>Welcome/driver_suppourt">Customer Support<i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>Welcome/about">About us <i class="fa fa-info" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
             <div class="" id="">
              <h4 class="pb-30">My Rides</h4>
              <div class="pb-20 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-1 col-sm-1 col-xs-1"><strong>Profile</strong></div>
                <div class="col-md-3 col-sm-3 col-xs-3"><strong>Rider Name</strong></div>
                <div class="col-md-2 col-sm-2 col-xs-2"><strong>Phone</strong></div>
                <div class="col-md-3 col-sm-3 col-xs-3"><strong>Date</strong></div>
                <div class="col-md-2 col-sm-2 col-xs-2 text-right"><strong>Status</strong></div>
              </div>
              <div class="clear"></div>
              <div class="">
               <?php if(count($rides )){?>
              <?php foreach($rides as $rides) {
            		       $user_name= $rides['user_name'];
                              $user_email= $rides['user_email'];
                              $user_phone= $rides['user_phone'];
                              $user_image= $rides['user_image'];
                              $ride_date= $rides['ride_date']; 
                              $ride_time= $rides['ride_time']; 
                              $ride_status= $rides['ride_status']; 
			      $pickup_location= $rides['pickup_location'];
			      $drop_location= $rides['drop_location'];?>
                <div class="panel-group panel-group-joined" id="accordion-test">
                  <div class="panel panel-default">
                    <div class="panel-heading1 panel_driver_heading">
                      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseOne">
                       <?php if ($user_image == "") { ?>
                        <div class="col-md-1 col-sm-1 col-xs-1 rider_dp"><img src="<?php echo base_url();?>/images/profile.png"/></div>
                         <?php } else{?>
                          <div class="col-md-1 col-sm-1 col-xs-1 rider_dp"><img src="<?= $driver_image; ?>" ></div>
                          <?php }?>
                        <div class="col-md-3 col-sm-3 col-xs-3 pt-15"><?php echo $user_name; ?></div>
                        <div class="col-md-2 col-sm-2 col-xs-2 pt-15"><?php echo $user_phone; ?></div>
                        <div class="col-md-3 col-sm-3 col-xs-3 pt-15"><?php echo $ride_date; ?></div>
                        <div class="col-md-2 col-sm-2 col-xs-2 ride_status_div">
                         
                      
                      <?php 
						if($ride_status=='')
						 {
						 echo "----";
					         }
						else if($ride_status==1)
						{
						echo " Scheduled ";  
						}
						else if($ride_status==2)
						{
						echo " Cancelled "; 
						}
						else if($ride_status==3)
						{
						echo "Accepted";  
						}
						else if($ride_status==4)
						{
						echo "Rejected";  
						}
						else if($ride_status==5)
						{
						echo "Arrived";  
						}
						else if($ride_status==6)
						{
						echo "Started";  
						}
						else if($ride_status==7)
						{
						echo "<strong style=\"color:#47bf7b;\">Completed</strong>";  
						}
						 
					   ?>      
                        
                          </div>
                        </a>
                        <div class="clear"></div>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <div class="pt-20 pb-20 trip_content">
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                              <tr>
                                <td width="136" colspan="" class="" style="text-align-last:left; width:120px;">
                                
                                 <?php if ($user_image == "") { ?>
                                <img src="<?php echo base_url();?>/images/profile.png" width="128" height="128"></td>
                                 <?php } else{?>
                                 <img src="<?= $driver_image; ?>" width="128" height="128" >
                                   </td>
                                  <?php }?>
                                <td width="283"><table width="81%" style="margin-left:10px;" aling="center">
                                    <tbody>
                                      <tr>
                                        <th width="27%" height="38" class="">Name</th>
                                        <td width="73%" class=""><?php echo $user_name; ?></td>
                                      </tr>
                                      <tr>
                                        <th height="38" class="">Email</th>
                                        <td class=""><?php echo $user_email; ?></td>
                                      </tr>
                                      <tr>
                                        <th height="38" class="">Phone</th>
                                        <td class=""><?php echo $user_phone; ?></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                                <td width="283"><table width="87%" style="margin-left:10px;" aling="center">
                                    <tbody>
                                      <tr>
                                        <th width="40%" height="38" class="">Ride Date</th>
                                        <td width="60%" class=""><?php echo $ride_date; ?></td>
                                      </tr>
                                      <tr>
                                        <th height="38" class="">Ride Time</th>
                                        <td class=""><?php echo $ride_time; ?></td>
                                      </tr>
                                      <tr>
                                        <th height="38" class="">Ride Status </th>
                                        <td class="">
                                             <?php 
						if($ride_status=='')
						 {
						 echo "----";
					         }
						else if($ride_status==1)
						{
						echo "<strong style=\"color:#000000;\">Scheduled</strong>";  
						}
						else if($ride_status==2)
						{
						echo "<strong style=\"color:#d03f3f;\">Cancelled</strong>"; 
						}
						else if($ride_status==3)
						{
						echo "Accepted";  
						}
						else if($ride_status==4)
						{
						echo "Rejected";  
						}
						else if($ride_status==5)
						{
						echo "Arrived";  
						}
						else if($ride_status==6)
						{
						echo "Started";  
						}
						else if($ride_status==7)
						{
						echo "<strong style=\"color:#47bf7b;\">Completed</strong>";  
						}
						 
					   ?>      </td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
                          <div class="col-md-12 padding_none user_ride_div_bottom">
                            <div class="col-md-4 padding_none">
                              <div class="user_ride_div">
                                <label>Pick Up Location  </label>
                                <p> <?php echo $pickup_location;?> </p>
                              </div>
                            </div>
                            <div class="col-md-5 padding_none_right">
                              <div class="user_ride_div">
                                <label>Drop Location </label>
                                <p> <?php echo $drop_location;?> </p>
                              </div>
                            </div>
                            <div class="col-md-3 padding_none spt_btn">
                              <div class="col-md-12 padding_none">
                               <!-- <button class="btn btn-success btn-sm m-b-5">Support</button>-->
                              </div>
                            </div>
                          </div>
                          <div class="clear"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php }} else{ ?>
                <table>
               <tr>
                    <th width="200%"><center><h1> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                             No Rides</h1></center>  </th>
                  </tr>
                  </table>
                <?php } ?>
              </div>
            </div>
            
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>