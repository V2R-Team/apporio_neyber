<?php include('header_rider.php'); ?>
  <section class=" mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <li class="rider_profile"> <img src="images/profile.png"/>
                <div class="clear"></div>
                <span class="rider_name">Your Name</span> </li>
              <li class=""> <a href="rider-dashboard.php">Dashboard<i class="fa fa-tachometer" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-trips.php">My Trips<i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-profile.php" >Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-password.php">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="rider-payment.php">Payment<i class="fa fa-money" aria-hidden="true"></i></a> </li>
              <li class="active"> <a href="rider-coupons.php">Coupons Applied<i class="fa fa-at" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="#">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
            <div class="" id="">
              <h4 class="pb-30">Coupons Applied</h4>
              <table id="" class="table table-striped table-bordered table-responsive no-footer" role="grid" aria-describedby="datatable_info">
                <thead>
                  <tr>
                    <th>Coupon Code</th>
                    <th>Coupon Price</th>
                    <th>Expiry Date</th>
                   
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Hello</td>
                    <td>123</td>
                    <td>2017-01-11</td>                   
                  </tr>
                  <tr>
                    <td>HAPPYOYO</td>
                    <td>100</td>
                    <td>2017-02-10</td>
                   
                  </tr>
                  <tr>
                  	<td>ABINASH</td>
                    <td>50</td>
                    <td>2017-02-18</td>                   
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>
