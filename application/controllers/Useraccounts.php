<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Useraccounts extends CI_Controller {

		function __construct() {
					parent::__construct();   
				        $this->load->model('User_account');
				       }
				      
	function generateRandomString($length = 6) {
    							$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					       	        $charactersLength = strlen($characters);
   					                $randomString = '';
 				 		        for ($i = 0; $i < $length; $i++) {
  										         $randomString .= $characters[rand(0, $charactersLength - 1)];
											  }
						       	return $randomString;
							}

	 
	public function rider_signup()
	{         
                $this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[user.user_email]');
                $this->form_validation->set_rules('mob', 'Mobile', 'required|trim|is_unique[user.user_phone]');
				
                 if ($this->form_validation->run() == FALSE){   
              							 $this->session->set_flashdata('Wrong_email','Email ALready Exists');
                                                                 return redirect('Welcome/rider_signup');
            						    }
                 else
                     { 
                     
                     $first=$this->input->post('fname');
                     $last= $this->input->post('lname');
                     $user_name=$first." ".$last;
                     $user_phone= $this->input->post('mob');
                     $user_password= $this->input->post('pass');
                     $user_email= $this->input->post('email');
                     $referral_code= $this->input->post('pcode');
                     if($referral_code == "")
                     {
                     $referral_code=0;
                     }
                     else
                     {
                     $referral_code= $referral_code;
                     }
            
                      $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
					  $data=$dt->format('M j'); 
		              $day=date("l");
		              $date=$day.", ".$data ;
                      $data = array(
                    		 'user_name'=>$user_name,
                    		 'user_email'=> $user_email,
                   		 'user_phone'=> $user_phone,
                    		 'user_password'=> $user_password,
                    		 'referral_code'=> $referral_code,  
                    		 'password_created'=>1,
                    		 'register_date'=>$date ,
                    		 'login_logout'=>1,
                    		 'status'=>1, 
                          	  );
                     
		 $user_id=$this->User_account->rider_signup($data);
                 $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
                 $this->session->set_flashdata('Rider_success','Registred Succesfully');	 	 
		     //   $to = $user_email;
     	        $subject = 'Taxi App Registration!';
	        $from = 'hello@apporio.com';
 
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
		// Create email headers
		$headers .= 'From: '.$from."\r\n".
	       'Reply-To: '.$from."\r\n" .
	       'X-Mailer: PHP/' . phpversion();
 
		// Compose a simple HTML email message
 		// Sending email
	
		$message = '<html>
				<head>
				<title>'.$subject.' </title>
			</head>
			<body>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:24px;background-color:#34495e">
					<tbody>
						<tr>
							<td>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:30px 30px 30px 30px;background-color:#fafafa">
									<tbody>
									
									<tr>
										<td  style="font-size:28px;font-family:Arial,Helvetica,sans-serif;color:#34495e; text-align:center; padding-bottom:30px;" ><b> Izycab App</b></td>
									</tr>
									<tr>
										<td style="font-size:12px;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2"> cher <strong>'.$user_name.'</strong>, </td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
									
										<td style="font-size:12px;text-align:justify;line-height:1.4;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2">	 	 
										<strong> 
Nous sommes heureux de vous informer que votre inscription a lapplication Izycab App est Succesfull.Nous vous assurons nos meilleurs services en tout temps.
 </strong>
										<br>
										
										
										</td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
										<td height="24" colspan="2">
								Nous esperons que vous avez apprecie votre experience en utilisant notre service.
										</td>
									</tr>
									
									<tr>
										<td height="30" style="border-bottom:1px solid #eaedef" colspan="2"> </td>
									</tr>
									<tr>
										<td height="12" colspan="2"> </td>
									</tr>
									<tr>
										<td colspan="3" align="center">
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<table width="100%">
												<tbody>
													<tr>
														<td align="center">
															Merci et salutations,
															<br> <br>
															
													Lequipe de lapplication Izycab App.
														</td>
													</tr>
												</tbody>
											</table>
											<span class="HOEnZb"><font color="#888888"> </font> </span>
										</td>
									</tr>
								</tbody>
							</table>
							<span class="HOEnZb"><font color="#888888"> </font></span>
						</td>
					</tr>
				</tbody>
			</table>
		     </body>
		 </html>';
		 mail($to, $subject, $message, $headers);
                     
		     redirect('Welcome/rider_dashboard');			   
	             }
		}
		 
  public function rider_signin()
  {
  $email=$this->input->post('email');
  $pass= $this->input->post('pass');
    $data = $this->User_account->login($email,$pass);
     
         if(!empty($data))
    	  {
    	  foreach($data as $id)
     				{
				     $user_id=$id['user_id'];
				     }
    	  $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	  $this->session->set_flashdata('msg', 'Login Succesful');
  	    
	  redirect('Welcome/rider_dashboard');	
	  }
          else
          {
          $this->session->set_flashdata('Wrong',$this->input->post('email')) ;
            
			 $this->load->view('rider-signin');
	        //redirect('Welcome/driver_profile');
      	  }
   	  }
   	  
   	  
   	  
   	  
    public function driver_signup()
	{         
	 
                $this->form_validation->set_rules('email', 'Email', 'trim|is_unique[driver.driver_email]');
                $this->form_validation->set_rules('mob', 'Mobile', 'trim|is_unique[driver.driver_phone]');
				// $this->form_validation->set_rules('mob', 'Mobile', '|min_length[9]|max_length[15]');
				 
                 if ($this->form_validation->run() == FALSE){   
                     // echo validation_errors();  die();
              							                          $this->session->set_flashdata('Wrong_email','Email ALready Exists');
                                                                 return redirect('Welcome/driver_signup');
            						    }
                 else
                     {  
                
                     $first=$this->input->post('fname');
                     $last= $this->input->post('lname');
                     $user_name=$first." ".$last;
                     $user_phone= $this->input->post('mob');
                     $user_password= $this->input->post('pass');
                     $user_email= $this->input->post('email');
                     $car_type_id= $this->input->post('car_type_id');
                     $car_model_id= $this->input->post('car_model_id');
                     $carnumber= $this->input->post('carnumber');
                     $city_id= $this->input->post('city_id');
                      
            
                      $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
		      $data=$dt->format('M j'); 
		      $day=date("l");
		      $date=$day.", ".$data ;
                      $data = array(
                    		 'driver_name'=>$user_name,
                    		 'driver_email'=> $user_email,
                   		 'driver_phone'=> $user_phone,
                    		 'driver_password'=> $user_password,
                    		 'car_type_id'=>$car_type_id,
                    		 'car_model_id'=>$car_model_id,
                    		 'car_number'=>$carnumber,
                    		 'city_id'=>$city_id,
                    		 'register_date'=>$date ,
                    		 'login_logout'=>1,
                    		 'driver_admin_status'=>1,
                    		 'car_type_id'=>$car_type_id,
                    		 'car_model_id'=>$car_model_id,
                    		 'car_number'=>$carnumber, 
                          	  );
		    $user_id=$this->User_account->driver_signup($data);
                  $this->session->set_userdata(['user'=>"driver",'user_id'=>$user_id]); 
                 $this->session->set_flashdata('Rider_success','Registred Succesfully');	
                 
                  redirect('Welcome/driver_profile');	
                 }
                 
                 }  
                 
          public function driver_signin()
  	{
  	$email=$this->input->post('email');
  	$pass= $this->input->post('pass');
  	 
    	$data = $this->User_account->login_driver($email,$pass);
        
         if(!empty($data))
    	  {
    	  foreach($data as $id)
    				     {
				     $user_id=$id['driver_id'];
				     }
				     
    	   $this->session->set_userdata(['user'=>"driver",'user_id'=>$user_id]); 
  	  $this->session->set_flashdata('msg', 'Login Succesful');
	  redirect('Welcome/driver_profile');	
	  }
          else
          {
          $this->session->set_flashdata('Wrong',$this->input->post('email')) ;
		  
	  $this->load->view('driver-signin');
          //return redirect('Welcome/driver_profile');
      	  }
   	  }    
               
           					
    	public function update_rider_password()
	{    
	    $this->form_validation->set_rules('new_password', 'New password','trim|matches[confirm_password]');
	    $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim');
	   
            if($this->form_validation->run() == FALSE)
           {
                // echo "Error";
                  //echo validation_errors();die();
                $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('rider-password',['profile'=>$profile]);
            
             }else{   
             //echo "Success";die();         
	       $new_password= $this->input->post('new_password');
	       $data=array(
	        	  'user_password' => $new_password
	      		  );
	       	  
	      $user_id =  $this->session->userdata('user_id');
	      
	      $old_password =  $this->input->post('old_password');
	      
	      $products=$this->User_account->update_rider_password($user_id,$old_password,$data); 
		 
		if($products)
		{
		$this->session->set_flashdata('success', 'Login Succesful');
		 $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		$this->load->view('rider-password',['profile'=>$profile]);
		}
		else
		{
		 
		$this->session->set_flashdata('fail', 'Login Succesfull');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		$this->load->view('rider-password',['profile'=>$profile]);
		}
		}
		}		
	
	public function update_driver_password()
	{    
	    $this->form_validation->set_rules('new_password', 'New password','required|trim|matches[confirm_password]');
	    $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|trim');
	    
            if($this->form_validation->run() == FALSE)
           {
           
                $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-password',['profile'=>$profile]);
            
        }else{ 
	       $new_password= $this->input->post('new_password');
	       $data=array(
	        	  'driver_password' => $new_password
	      		  );
	       	  
	      $user_id =  $this->session->userdata('user_id');
	      $old_password =  $this->input->post('old_password');
	      
	      $products=$this->User_account->update_driver_password($user_id,$old_password,$data); 
		 
		if($products)
		{
		 
		$this->session->set_flashdata('success', 'Login Succesful');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-password',['profile'=>$profile]);
		}
		else
		{
		 
		$this->session->set_flashdata('fail', 'Login Succesfull');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-password',['profile'=>$profile]);
		}
		 
	 
	}	}				
    		  
    public function update_rider_profile()
	{    
	       $fname=$this->input->post('fname');
	       $lname=$this->input->post('lname');
	       $username=$fname." ". $lname;
	       $email=$this->input->post('email');
	       $mobile=$this->input->post('mobile');
	       
	       
	       /* $config['upload_path']    = './uploads/user';
	                      $config['allowed_types']  = 'PNG|JPG|png|jpg|PDF|pdf|jpeg';
	             
	       			    $this->load->library('upload');
                  		    $this->upload->initialize($config);
                  	 if( $this->upload->do_upload('image') == FALSE){
                  	   
		             $error =  $this->upload->display_errors();
		             $user_id =  $this->session->userdata('user_id');
					 
	              
	      $profile=$this->User_account->view_rider_profile($user_id ); 
		 $this->load->view('rider-profile',['profile'=>$profile]);
                                  }
                              else{
                             
                            $data = $this->upload->data(); 
                            $cat_image = base_url("uploads/user/".$data['raw_name'].$data['file_ext']);   
                       	    $fname=$this->input->post('fname');
	                    $lname=$this->input->post('lname');
	  	            $username=$fname." ". $lname;
		            $email=$this->input->post('email');
		            $mobile=$this->input->post('mobile');
	             */
                            $data=array(
	        	   'user_name' => $username,
	        	   'user_email' => $email,
	        	   'user_phone' => $mobile,
	        	   'user_image'=> $cat_image 
	      		   );        
                     
                      	  
	      $user_id =  $this->session->userdata('user_id');
	      $products=$this->User_account->update_rider_profile($user_id,$data); 
		  
		 
		if($products)
		{
		$this->session->set_flashdata('success', 'Login Succesful');
		$user_id =  $this->session->userdata('user_id');
	    $profile=$this->User_account->view_rider_profile($user_id ); 
		$this->load->view('rider-profile',['profile'=>$profile]);
		}
		else
		{
		 
		$this->session->set_flashdata('fail', 'Login Succesfull');
		$user_id =  $this->session->userdata('user_id');
	    $profile=$this->User_account->view_rider_profile($user_id ); 
		$this->load->view('rider-profile',['profile'=>$profile]);
		}
		}
		 
                   
                   
                   
	       
	       
	       
	    
		
		public function update_driver_profile()
	        {      
	      
	       	  
	       	  
	       	 	    /*  $config['upload_path']    = './uploads/driver';
	                  $config['allowed_types']  = 'PNG|JPG|png|jpg|PDF|pdf|jpeg';
	             
	       			    $this->load->library('upload');
                  		    $this->upload->initialize($config);
                  	 if( $this->upload->do_upload('image') == FALSE){
                  	   
		             $error =  $this->upload->display_errors();
		            
		           $user_id =  $this->session->userdata('user_id');
	              $profile=$this->User_account->view_driver_profile($user_id );
		        $this->load->view('driver-profile',['profile'=>$profile]);
                                  }
                              else{
                             
                            $data = $this->upload->data(); 
                            $cat_image = base_url("uploads/user/".$data['raw_name'].$data['file_ext']);  */ 
                            $drivername=$this->input->post('drivername');
	    		    $driveremail=$this->input->post('driveremail');
	        	    $driverphone=$this->input->post('driverphone');
	        	    
	       $data=array(
	        	  'driver_name' => $drivername,
	        	  'driver_email' => $driveremail,
	        	  'driver_phone' => $driverphone,
	        	  'driver_image' =>$cat_image 
	      		  );
	               
	       	   ;
	      $user_id =  $this->session->userdata('user_id');
	      $products=$this->User_account->update_driver_profile($user_id,$data); 
		 
		if($products)
		{
		$this->session->set_flashdata('success', 'Login Succesful');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-profile',['profile'=>$profile]);
		}
		else
		{
		 
		$this->session->set_flashdata('fail', 'Login Succesfull');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-profile',['profile'=>$profile]);
		}
		}
		 
		public function driver_suppourt()
	       {      
	       $drivername=$this->input->post('name');
	       $driveremail=$this->input->post('email');
	       $driverphone=$this->input->post('phone');
	       $driverphone=$this->input->post('query');
	        $user_id =  $this->session->userdata('user_id');
	       $data=array(
	                  'driver_id' => $user_id ,
	        	  'name' => $drivername,
	        	  'email' => $driveremail,
	        	  'phone' => $driverphone,
	        	  'query' => $driverphone
	      		  );
	      		  
	      $products=$this->User_account->driver_suppourt($data); 
		if($products)
		{
		$this->session->set_flashdata('success', 'Login Succesful');
		$user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-support',['profile'=>$profile]);
		}
		 
		}
		
public function Upload_driver_docs()
	       { 	 
                 $driver_id=  $this->session->userdata('user_id');
		$img_name=$_FILES['insurance']['name'];
		$filedir="uploads/driver/";
			$fileext = strtolower(substr($_FILES['insurance']['name'],-4));
			if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
			{
				if($fileext=="jpeg") 
				{
					$fileext=".jpg";
				}
				$pfilename = time()."insurance_".$driver_id.$fileext;
				$filepath1 = "uploads/driver/".$pfilename;
				$filepath = $filedir.$pfilename;
				copy($_FILES['insurance']['tmp_name'], $filepath);
				 
				
				 
					$img_name=$_FILES['license']['name'];
					$filedir="uploads/driver/";
		
					 
						$fileext = strtolower(substr($_FILES['license']['name'],-4));
						if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
						{
							if($fileext=="jpeg") 
							{
								$fileext=".jpg";
							}
							$pfilename = time()."license_".$driver_id.$fileext;
							$filepath2 = "uploads/driver/".$pfilename;
							$filepath = $filedir.$pfilename;
							copy($_FILES['license']['tmp_name'], $filepath);
							 
							
							 
								$img_name=$_FILES['rc']['name'];
								$filedir="uploads/driver/";
		
								 
									$fileext = strtolower(substr($_FILES['rc']['name'],-4));
									if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
									{
										if($fileext=="jpeg") 
										{
											$fileext=".jpg";
										}
										$pfilename = time()."rc_".$driver_id.$fileext;
										$filepath3 = "uploads/driver/".$pfilename;
										$filepath = $filedir.$pfilename;
										copy($_FILES['rc']['tmp_name'], $filepath);
										 

							}}}
							
							
							$data=array(
							       'insurance'=>$filepath3 ,
							        'license'=>$filepath3 ,
							        'rc'=>$filepath3 );
							 $user_id =  $this->session->userdata('user_id');
							 $driver_docs=$this->User_account->update_driver_docs($user_id,$data);
							 
							 if($driver_docs)
									{
									$this->session->set_flashdata('success', 'Login Succesful');
									$user_id =  $this->session->userdata('user_id');
								    $profile=$this->User_account->view_driver_profile($user_id );
									$this->load->view('driver-documents',['profile'=>$profile]);
									}
							}
							
public function rider_book_later()
	{         
                     $user_id =  $this->session->userdata('user_id');
                     $pick_up=$this->input->post('pick_up');
                     $drop_loc= $this->input->post('drop_loc');
                     $min_date = $this->input->post('min-date');
                     $car_type_id=$this->input->post('car_type_id');
                     $data=(explode(" ",$min_date ));
                     $later_date=$data['0'];
                     $later_time=$data['1'];
                     
					 
					 
					 
 
        $address = $pick_up; // Google HQ
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        $pickup_lat = $output->results[0]->geometry->location->lat;
        $pickup_long = $output->results[0]->geometry->location->lng;
		 	 
		
        $address = $drop_loc; // Google HQ
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        $drop_lat = $output->results[0]->geometry->location->lat;
        $drop_long = $output->results[0]->geometry->location->lng;		
					 
					 
					 
					 
					 
                     date_default_timezone_set('Asia/Kolkata');
                     $dateFormat = 'j/n/Y';
                     $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
      		     $data=$dt->format('M j');
	             $day=date("l");
	              
       		   $date=$day.", ".$data ; 
        	    $time=date("h:i A");
        	    
                      $data=array(
                      		 'user_id'=>$user_id,
                      		  'coupon_code'=>"",
                      		   'pickup_lat'=>$pickup_lat,
                      		    'pickup_long'=>$pickup_long,
                      		     'pickup_location'=>$pick_up,
                      		      'drop_lat'=>$drop_lat,
                      		       'drop_long'=>$drop_long,
                      		        'drop_location'=>$drop_loc,
                      		         'ride_date'=>$date,
                      		          'ride_time'=>$time,
                      		           'ride_type'=>2,
                        		     'ride_status'=>1,
	                      		      'later_date'=>$later_date,
	                      		        'later_time'=>$later_time,
	                      		         'car_type_id'=>$car_type_id,
	                      		          'ride_admin_status'=>1,
	                      		           'payment_option_id'=>"",
	                      		             'card_id'=>"",
                      		 );
                      ;    
		 $booking_id=$this->User_account->rider_book_later($data);
		 if($booking_id)
		 {      
	$list=$this->User_account->rider_book_later_user_data($user_id);
	foreach($list as $login);
   
    $list2=$this->User_account->rider_book_later_cartype_data($car_type_id);
	foreach($list2 as $login2);
	
     $list3=$this->User_account->rider_book_later_carmodel_data($car_type_id);
	foreach($list3 as $login2);
	
   
	 $admin_email = $login['user_email'];
		 
	 $to =  $admin_email;
     $subject = 'Izycab Ride';
     $from = 'support@izycab.com';
 
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
 
// Compose a simple HTML email message
 
 
// Sending email
	$message = '
		<div class="gmail_quote">
  <div dir="ltr">
    <div class="gmail_quote">
      <br>                                   
      <div style="margin:0;padding:0">   
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" cla	ss="tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td style="text-align:left;padding:10px;"> '.$date.' </td>
			          <td style="text-align:right;padding:10px;"> Izycab Ride </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;">  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;">  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> 
																			
							Vos Détails de réservation, '.$login['user_name'].'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="14px;padding-right:14px" class="ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Détails du trajet</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                                                                 
                                                                <img height="50" style="height:50px" src="http://apporio.co.uk/apporiotaxi/'.$login['user_image'].'" alt="" class="CToWUd">
                                                              </td>             
                                                                                                
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$login['user_name'].'</td>                 
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                        
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                                   
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:12px;padding-left:0px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                                                                   <tbody>
                                                                                                                                    
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                                                                              
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                               
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                    
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:7px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                            
                                 
                                                          <tbody>
                                                            <tr>                                                                                  
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">    
                                                                <img width="38" style="width:38px" src="http://apporio.co.uk/apporiotaxi/api/mail/car.png" alt="" class="CToWUd">
                                                              </td>               
                                                              <td align="left" style="padding-top:4px;color:#000000;font-size:14px;padding-left:16px;line-height:16px">  '.$login2['car_type_name'].'  -  '.$login3['car_model_name'].'        </td>                                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                     
                                                      </td>                                                                 
                                                    </tr>                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                             
                                            <tr style="vertical-align:top;text-align:left;display:block;background-color:#ffffff;padding-bottom:10px;padding-top:5px" align="left" bgcolor="#ffffff">
                                              <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:inline-block;padding:10px 0 0 14px" align="left" valign="top" class="route-address">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:auto;padding:0">                                                                 
                                                  <tbody>                                                                                        
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding-top:5px" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">       
                                                          <span class="aBn">                                     
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101540">
                                                                <span class="aQJ">'.$later_time.'
                                                                </span>
                                                              </span>
                                                            </a> 
                                                          </span>                                                                         
                                                        </span>                                                                     
                                                      </td>                                                                     
                                                      <td rowspan="2" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:17px!important;padding:3px 2px 10px 2px" align="left" valign="top">  
                                                        <img width="6" height="84px" src="http://apporio.co.uk/apporiotaxi/api/mail/border.png" style="outline:none;text-decoration:none;float:left;clear:both;display:block;width:6px!important;height:84px;padding-top:5px" align="left" class="CToWUd CToWUd">            
                                                      </td>                                                                     
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:57px;padding:0 10px 10px 0" align="left" valign="top">                                                                         
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$pick_up.' </span>                
                                                      </td>                                                                 
                                                    </tr>              
                                                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding:0" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">                             
                                                          <span class="aBn">                                  
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101541">
                                                                
                                                                </span>
                                                              </span>
                                                            </a>                                                                             
                                                          </span>                                                                         
                                                        </span>                                     
                                                      </td>                                        
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:auto;padding:0 0px 0 0" align="left" valign="top">                                   
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$drop_loc.'  </span>                                                                     
                                                      </td>                                                                 
                                                    </tr>                              
                                                  </tbody>                                                             
                                                </table>                                         
                                              </td>                                                     
                                            </tr>                                                 
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table">
                                  <tbody>
                                    <tr>                                             
                                      <td class="fare-break-up" style="padding-right:14px;padding-left:14px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold;border-bottom:1px solid #d7d7d7">
                                              </td>                                                     
                                            </tr>                                                                                                  
                                            <tr>                                                     
                                              <td bgcolor="#ffffff" class="base-padding">             
                                                <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
					                <tr>
					                  <td style="text-align:left; padding:8px;">  </td>
					                  <td style="text-align:right; padding:8px;"> </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px;"> 
																		 </td>
					                  <td style="text-align:right; padding:8px;"> </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <td style="text-align:left; padding:8px;"></td>
					                  <td style="text-align:right; padding:8px;">  </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px; width:100px;"> 
																							</td>
					                  <td style="text-align:right; padding:8px;"> </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  
					                </tr>
					                <tr>
					                </tr>
					              </table>                                                     
                                              </td>                                                 
                                            </tr>                                                                                                                                   
                                          </tbody>
                                        </table>                                     
                                      </td>                                 
                                    </tr>                             
                                  </tbody>
                                </table>                         
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                   <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:24px; font-weight: bold;">   </td>
			        </tr>
			        <tr>
			          <td style="text-align:left;padding:20px;">   <span class="il">     </td>
			          <td style="text-align:right;padding:20px;">    </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p> 
      </div>  
    </div>
    <br>
  </div>
</div>
	'	;
		
		 mail($to, $subject, $message, $headers);	
	            
	            
                 $this->session->set_flashdata('Bookig_success','Bookig Succesfully');
                 redirect('Welcome/rider_dashboard');	
                 }else
                 {
                 $this->session->set_flashdata('Bookig_Error','Bookig Succesfully');
                  redirect('Welcome/rider_dashboard');	
                 }
                 }
				 
	public function forget_pass_update()
  	{
  	$type=$this->input->post('type');
  	$email= $this->input->post('email');
  	if($type == 'user')
	{		
    	$data = $this->User_account->login_rider_forget($email);
		  $admin_pass = $data['0']['user_password'];
		  $user_name = $data['0']['user_name'];
				   
	}
    else if($type == 'driver')
     {
	 $data = $this->User_account->login_driver_forget($email);	
	 $admin_pass = $data['0']['driver_password'];
	 $user_name = $data['0']['driver_name'];
	 }		
         if(!empty($data))
    	  {
    	       
		 
				
		$to =  $email;
      $subject = 'Izycab App Password';
     $from = 'support@izycab.com';
 
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
 
// Compose a simple HTML email message
 
 
// Sending email

			
		$message = '<html>
			<head>
				<title>'.$subject.' </title>
			</head>
			<body>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:24px;background-color:#34495e">
					<tbody>
						<tr>
							<td>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:30px 30px 30px 30px;background-color:#fafafa">
									<tbody>
									
									<tr>
										<td  style="font-size:28px;font-family:Arial,Helvetica,sans-serif;color:#34495e; text-align:center; padding-bottom:30px;" ><b> Izycab App</b></td>
									</tr>
									<tr>
										<td style="font-size:12px;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2"> cher <strong>'.$user_name.'</strong>, </td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
									
										<td style="font-size:12px;text-align:justify;line-height:1.4;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2">	 	 
										<strong> 
<center>Your Password is : <h3><u> '.$admin_pass.'</u></h3>.</center>
 </strong>
										<br>
										
										
										</td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
										<td height="24" colspan="2">
							<center>	Nous esperons que vous avez apprecie votre experience en utilisant notre service.</center>
										</td>
									</tr>
									
									<tr>
										<td height="30" style="border-bottom:1px solid #eaedef" colspan="2"> </td>
									</tr>
									<tr>
										<td height="12" colspan="2"> </td>
									</tr>
									<tr>
										<td colspan="3" align="center">
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<table width="100%">
												<tbody>
													<tr>
														<td align="center">
															Merci et salutations,
															<br> <br>
															
													Lequipe de lapplication Izycab App.
														</td>
													</tr>
												</tbody>
											</table>
											<span class="HOEnZb"><font color="#888888"> </font> </span>
										</td>
									</tr>
								</tbody>
							</table>
							<span class="HOEnZb"><font color="#888888"> </font></span>
						</td>
					</tr>
				</tbody>
			</table>
			</body>
		</html>';
		 
		
		 mail($to, $subject, $message, $headers);
		  
		  
		  
  	     $this->session->set_flashdata('pass_success', 'Login Succesful');
	     redirect('Welcome/');	
	     }
	 
          else
          {
          $this->session->set_flashdata('pass_fail',$this->input->post('email')) ;
		  
		  $this->load->view('forgot-password');
          //return redirect('Welcome/driver_profile');
      	  }
   	  }    			 
                 
                 }  
                 
										 
 
		
	
					
               