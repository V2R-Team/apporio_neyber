<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Welcome extends CI_Controller {

		function __construct(){
			parent::__construct();   
	        $this->load->model('User_account');
        }

	 
	public function index()
	{       
		$web_home = $this->User_account->web_home(); 		 
		$this->load->view('index',['web_home'=>$web_home]);
	}
	
	public function signin()
	{   
	if($this->session->userdata('user') == "") { 
		$this->load->view('signin');
		}
		else{
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function register()
	{      
	           if($this->session->userdata('user') == "") { 
		$this->load->view('register');
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function contact_us()
	{   
	    $web_contact = $this->User_account->web_contact(); 
		$this->load->view('contact-us',['web_contact'=>$web_contact]);
	} 	
	public function about()
	{
		
		 $web_about = $this->User_account->web_about(); 
		 $this->load->view('about-us',['web_about'=>$web_about]);
	}
	
	public function forget_update()
	{
		 $this->load->view('forgot-password');
	}
	
	
	public function forget_password()
	{      
		$this->load->view('forgot-password');
	}
	public function driver_signin()
	{     if($this->session->userdata('user') == "") { 
		$this->load->view('driver-signin');}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_signup()
	{      if($this->session->userdata('user') == "") { 
	        $city=$this->User_account->city(); 
	        $car_type=$this->User_account->car_type(); 
			$driver_signup = $this->User_account->driver_signup_page();
		$this->load->view('driver-signup',['city'=>$city,'car_type'=>$car_type,'driver_signup'=>$driver_signup]);}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_signin()
	{   if($this->session->userdata('user') == "") {     
		$this->load->view('rider-signin');
	}else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_signup()
	{       
	        if($this->session->userdata('user') == "") { 
			$web_rider_signup = $this->User_account->web_rider_signup();
		    $this->load->view('rider-signup',['web_rider_signup'=>$web_rider_signup]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function rider_later()
	{       
	      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $data=$this->User_account->rider_trips_booking_later($user_id);
	        
		$this->load->view('rider_later',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    } 
	}
	
	
	public function terms()
	{       
		$this->load->view('terms');
	}
	public function rider_dashboard()
	{  
	         if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
	         $car_type=$this->User_account->car_type();  
		$this->load->view('rider-dashboard',['profile'=>$profile,'car_type'=>$car_type]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_trips()
	{       if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id ); 
	        $data=$this->User_account->rider_rides($user_id); 
	        
	        foreach ($data as $key =>$value)
		 {
		  $category_id=$value['ride_id'];
		  $text=$this->User_account->get_doneride_id($category_id);		 
		  $data[$key] = $value;
		  $data[$key]["sub_cat"] = $text;
		 }
	        
	        
	        
	        // echo "<pre>";print_r($data);                               
		  $this->load->view('rider-trips',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	
	public function rider_trips_booking_later()
	{       if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $data=$this->User_account->rider_trips_booking_later($user_id); 
	         
		     $this->load->view('rider-trips',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	
	
	public function rider_profile()
	{      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id ); 
	        $this->load->view('rider-profile',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_change_password()
	{        
	        if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		$this->load->view('rider-password',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_payment()
	{
	        if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		 
		$this->load->view('rider-payment',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function rider_coupons()
	{
	      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $coupons=$this->User_account->view_rider_coupons($user_id); 
	         //print_r($coupons);die();
	       if(count($coupons))
	        {
	        $coupons_data= array();
	        foreach($coupons as $data)
	        {
	        $coupon_code=$data['coupon_code'];
	        if($coupon_code != "")
	        {
	        $coupons_data[]=$this->User_account->view_rider_coupons_data($coupon_code); 
	        }
	        }
	          
	         $this->load->view('rider-coupons',['profile'=>$profile,'coupons_data'=>$coupons_data]);
	        
	        }
	        else
	        {
	        $this->load->view('rider-coupons',['profile'=>$profile]);
	        } 
		
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function driver_profile()
	{       
	       if($this->session->userdata('user') == "driver") {   
	       $user_id =  $this->session->userdata('user_id');
		   //echo $user_id;die();
	        $profile=$this->User_account->view_driver_profile($user_id );
			//print_r($profile);die();
		$this->load->view('driver-profile',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_rides()
	{       
	        if($this->session->userdata('user') == "driver") {   
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
	        $rides=$this->User_account->view_rides($user_id );
	        
		$this->load->view('driver-rides',['profile'=>$profile,'rides'=>$rides]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_documents()
	{ 
	    if($this->session->userdata('user') == "driver") {  
	         $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-documents',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_earnings()
	{        if($this->session->userdata('user') == "driver") {  
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-earnings',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_password()
	{       if($this->session->userdata('user') == "driver") {  
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-password',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_suppourt()
	{        
	       if($this->session->userdata('user') == "driver") {  
	       $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-support',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	 
	
	public function car_model(){  
    	                                         $car_type_id= $this->input->post('car_type_id');
    	                                         $data = $this->User_account->get_carmodel($car_type_id);
    	                                         
    	                                          if($data)
             						{
					             echo "<option value>-------- Please Select Model --------</option>";
					            foreach($data as $data_value)
								      {
						      echo "<option value='".$data_value['car_model_id']."'>".$data_value['car_model_name']."</option>";
								      }
        					
    						}
    						}
    						
    	public function Contact_us_form()
	   {        
	       $uname=$this->input->post('uname');
           $email= $this->input->post('email');
           $skypephone= $this->input->post('skypephone');
           $subject= $this->input->post('subject');
                     $data=array(
                                
                                'name'=>$uname,
                                'email'=>$email,               		
                                'skypho'=>$skypephone,
                                'subject'=>$subject    
                                );
                    	         
                     $profile=$this->User_account->Contact_us_form($data);
                     $this->session->set_flashdata('Rider_success','Registred Succesfully');
		      return redirect('Welcome/contact_us');
		}
		 
	 
            
	
	 public function logout()
     	 {
      	 $this->session->set_flashdata('Logout',"logout");
       	 $this->session->unset_userdata(['user','user_id']);
      	 return redirect('Welcome');
         }	
    
	
	
	
}
