<?php
error_reporting(0);
function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

include_once '../apporioconfig/start_up.php';
require("Mail.php");
header("Content-Type: application/json");

$user_name=$_REQUEST['user_name'];
$user_email=$_REQUEST['user_email'];
$user_phone=$_REQUEST['user_phone'];
$user_password=$_REQUEST['user_password'];
$language_id=$_REQUEST['language_id'];

$random=generateRandomString();

if($user_name!="" && $user_email!="" && $user_phone!="" && $user_password!="")
{
	$sql="select * from user where user_email='$user_email'";
	$result = $db->query($sql);
	$ex_rows=$result->num_rows;
	
	if($ex_rows==0)
	{
		$sql1="select * from user where user_phone='$user_phone'";
		$result1 = $db->query($sql1);
		$ex_rows1=$result1->num_rows;
		
		if($ex_rows1==0)
		{
			$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
			$data=$dt->format('M j'); 
			$day=date("l");
			$date=$day.", ".$data ;

			$query2="INSERT INTO user (user_name, user_email, user_phone,user_password,referral_code,phone_verified,password_created,register_date,login_logout,status) VALUES('$user_name','$user_email','$user_phone','$user_password','$random',1,1,'$date',1,1)";
			
			if ($db->query($query2) === TRUE) 
			{
				$admin_email = $user_email ;
		$recipients = $admin_email;
		$headers['From']    = '"Taxi App" <taxiapp@apporio.co.uk>';
		$headers['To']      = '"Taxi App" <taxiapp@apporio.co.uk>';
		$headers['Subject'] = 'Taxi App Registration';
		$headers["Content-Type"] = "text/html; charset=UTF-8";  
			
		$body = '<html>
			<head>
				<title>'.$subject.' </title>
			</head>
			<body>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:24px;background-color:#34495e">
					<tbody>
						<tr>
							<td>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:30px 30px 30px 30px;background-color:#fafafa">
									<tbody>
									
									<tr>
										<td  style="font-size:28px;font-family:Arial,Helvetica,sans-serif;color:#34495e; text-align:center; padding-bottom:30px;" ><b> Taxi App</b></td>
									</tr>
									<tr>
										<td style="font-size:12px;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2"> Dear <strong>'.$user_name.'</strong>, </td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
									
										<td style="font-size:12px;text-align:justify;line-height:1.4;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2">	 	 
										<strong> We are happy to inform you that your registration for Taxi App is Succesfull. We assure you of our best services at all times .
 </strong>
										<br>
										
										
										</td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
										<td height="24" colspan="2">
											We Hope you enjoyed your experience using our service.
										</td>
									</tr>
									
									<tr>
										<td height="30" style="border-bottom:1px solid #eaedef" colspan="2"> </td>
									</tr>
									<tr>
										<td height="12" colspan="2"> </td>
									</tr>
									<tr>
										<td colspan="3" align="center">
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<table width="100%">
												<tbody>
													<tr>
														<td align="center">
															Thanks & Regards,
															<br> <br>
															The Taxi App Team.
														</td>
													</tr>
												</tbody>
											</table>
											<span class="HOEnZb"><font color="#888888"> </font> </span>
										</td>
									</tr>
								</tbody>
							</table>
							<span class="HOEnZb"><font color="#888888"> </font></span>
						</td>
					</tr>
				</tbody>
			</table>
			</body>
		</html>';
		
		$params['sendmail_path'] = '/usr/lib/sendmail';
		$mail_object =& Mail::factory('sendmail', $params);
		$mail_object->send($recipients, $headers, $body);
		
			
		$last_id = $db->getLastId();
				$query3="select * from user where user_id='$last_id'";
				$result3 = $db->query($query3);
				$list=$result3->row;
		
				$re = array('result'=> 1,'msg'=> "Signup Succesfully",'details'	=> $list);		
			} 
			else 
			{
    			echo "Error: " . $sql . "<br>" . $db->error;
			}
		}
		else
		{
			$re = array('result'=> 0,'msg'=> "Phone Number already exist",);
		}
	}
	else
	{
		$re = array('result'=> 0,'msg'=> "Email already exist",);
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>