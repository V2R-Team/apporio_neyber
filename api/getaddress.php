<?php
error_reporting(0);
header("Content-Type: application/json");

$latitude=$_REQUEST['latitude'];
$longitude=$_REQUEST['longitude'];
if($latitude !="" && $longitude !="" )
{
   $geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='
                                         .$latitude.','.$longitude.'&sensor=false');

   
   $output= json_decode($geocode);
   $address =  $output->results[0]->formatted_address; 
   $re = array('result' => 1,'msg'	=>"Address",'details'=>$address);
}
else
{
   
   $re = array('result' => 0,'msg'	=>"Required Field Missing");
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>