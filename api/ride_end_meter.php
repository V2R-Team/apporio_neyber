<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

$ride_id = $_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$end_lat=$_REQUEST['end_lat'];
$end_long=$_REQUEST['end_long'];
$end_location=$_REQUEST['end_location'];
$distance = $_REQUEST['distance'];
$driver_token=$_REQUEST['driver_token'];
$language_id=$_REQUEST['language_id'];

if($ride_id!="" && $driver_id!="" && $end_lat!="" && $end_long!="" && $end_location!="" && $driver_token!= "" )
{
    $query="select * from driver where driver_token='$driver_token' AND driver_id='$driver_id'";
    $result = $db->query($query);
    $ex_rows=$result->num_rows;
    $list = $result->row;
    $total_payment_eraned = $list['total_payment_eraned'];
    $company_payment = $list['company_payment'];
    $driver_payment = $list['driver_payment'];
    $commision = $list['commission'];
    if($ex_rows==1)
    {
        $last_time_stamp = date("h:i:s A");
        $end_time = date("h:i:s A");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data ;
        $new_time=date("h:i");
        $completed_rides = $list['completed_rides']+"1";
        $city_id = $list['city_id'];
        $car_type_id = $list['car_type_id'];
       
        $query5="UPDATE driver SET last_update='$new_time',completed_rides='$completed_rides' WHERE driver_id='$driver_id'" ;
        $db->query($query5);

        $query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='7' WHERE ride_id='$ride_id'" ;
        $db->query($query1);


        $query2 = "select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
        $result2 = $db->query($query2);
        $list3 = $result2->row;
        $distance_unit = $list3['distance_unit'];
        $base_distance = $list3['base_distance'];
        $base_distance_price = $list3['base_distance_price'];
        $base_price_per_unit = $list3['base_price_per_unit'];


        $free_ride_minutes = $list3['free_ride_minutes'];
        $price_per_ride_minute = $list3['price_per_ride_minute'];

        $free_waiting_time = $list3['base_wating_time'];
        $wating_price_minute = $list3['wating_price_minute'];
		if($distance_unit == "Kilometers"){
		    $dist1 = ($distance/1000);
		}else{
		  $dist1 = $distance*0.00062137;
		}
        
        $dist1 = sprintf("%.2f",$dist1);
        if($dist1 <= $base_distance)
        {
            $final_amount = $base_distance_price;
            $final_amount = sprintf("%.2f",$final_amount);
        }
        else
        {
            $diff_distance = $dist1-$base_distance;
            $amount1= ($diff_distance * $base_price_per_unit);
            $final_amount = $base_distance_price+$amount1;
            $final_amount = sprintf("%.2f",$final_amount);
        }
        $query3="select * from done_ride where ride_id='$ride_id'";
        $result3 = $db->query($query3);
        $list=$result3->row;
        $done_ride=$list['done_ride_id'];
        $begin_time = $list['begin_time'];
        $waiting_time = $list['waiting_time'];
        $time1 = new DateTime($begin_time);
        $time2 = new DateTime($end_time);
        $interval = $time1->diff($time2);
        $ride_time = $interval->format('%i');
        $dist = $dist1." ".$distance_unit;
        $query2="UPDATE done_ride SET end_lat='$end_lat',end_long='$end_long', end_location='$end_location', end_time='$end_time', amount='$final_amount', distance='$dist',tot_time='$ride_time' WHERE ride_id='$ride_id'";
        $db->query($query2);


        if($ride_time > $free_ride_minutes)
        {
            $diff_distance = $ride_time-$free_ride_minutes;
            $amount1= ($diff_distance * $price_per_ride_minute);
            $amount1 = sprintf("%.2f", $amount1);
            $query2="UPDATE done_ride SET  ride_time_price='$amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET  ride_time_price='00.00' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }


        if($waiting_time > $free_waiting_time)
        {
            $diff_distance = $waiting_time-$free_waiting_time;
            $final_amount1= ($diff_distance * $wating_price_minute);
            $final_amount1 = sprintf("%.2f", $final_amount1);
            $query2="UPDATE done_ride SET waiting_price='$final_amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET waiting_price='00.00' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }
        
        $query4="select * from ride_table where ride_id='$ride_id'";
        $result4 = $db->query($query4);
        $list4=$result4->row;
        $user_id=$list4['user_id'];
        $coupan = $list4['coupon_code'];
         
         $query="select * from done_ride where ride_id='$ride_id'";
	      $result = $db->query($query);
	      $list12345 = $result->row;
         $waiting_price = $list12345['waiting_price'];
         $amount =  $list12345['amount'];
         $ride_time_price =  $list12345['ride_time_price'];
         $total_amount =  $waiting_price+$amount+$ride_time_price;
         if($coupan != "")
         {
	    	$query1 ="select * from coupons where coupons_code='$coupan'";
	        $result1 = $db->query($query1);
	        $list1 = $result1->row; 
                $coupon_type = $list1['coupon_type'];
                $coupon_code = $list1['coupons_code'];
               $coupan_price = $list1['coupons_price'];
               if($coupon_type == "Nominal"){
                  $total_amount = $total_amount-$coupan_price;
             }else{
                   $coupan_price = ($total_amount*$coupan_price)/100;
                   $total_amount =  $total_amount-$coupan_price;
             }
         }else{
            $coupan_price  = "0.00";
         }
    
     
        if($commision != 0){
	     $commision_price = ($total_amount*$commision)/100;
             $company_commision = number_format((float)$commision_price, 2, '.', '');
             $driver_amount = $total_amount-$company_commision;
             $driver_amount=number_format((float)$driver_amount, 2, '.', '');
	}else{
	     $commision_price = "0.00"; 
	     $driver_amount = $total_amount;
	 }
	

        $ride_status = $list4['ride_status'];
        $payment_option_id=$list4['payment_option_id'];
        
        if($payment_option_id == 1){
	      $company_payment1 = $company_payment+$company_commision;
	      $driver_payment1 = $driver_payment;
	}else{
	     $company_payment1 = $company_payment;
	     $driver_payment1 = $driver_payment+$driver_amount;
	}
             $total_payment_eraned = $total_payment_eraned+$driver_amount;			 
	     $query5="UPDATE driver SET total_payment_eraned='$total_payment_eraned',company_payment='$company_payment1',driver_payment='$driver_payment1' WHERE driver_id='$driver_id'" ;
         $db->query($query5);
        $query2="UPDATE done_ride SET  total_amount='$total_amount',company_commision='$company_commision',driver_amount='$driver_amount',coupan_price='$coupan_price' WHERE ride_id='$ride_id'";
         $db->query($query2);	 
        $query1="select * from driver_earnings where driver_id='$driver_id' AND date=CURDATE()";
	$result1 = $db->query($query1);
	$driver_earn=$result1->row; 
	if(empty($driver_earn))
	{
	$date = date("Y-m-d");
	$query2="INSERT INTO driver_earnings(driver_id,rides,total_amount,amount,date) 
    VALUES('$driver_id','1','$total_amount','$driver_amount','$date')";
	$db->query($query2);
	}else{
	         $driver_earning_id = $driver_earn['driver_earning_id'];
	        $rides = $driver_earn['rides']+1;
	        $amount = $driver_earn['amount']+$driver_amount;
            $total_amount = $driver_earn['total_amount']+$total_amount;
		$query3="UPDATE driver_earnings SET rides='$rides',amount='$amount',total_amount='$total_amount' WHERE driver_earning_id='$driver_earning_id'";
		$db->query($query3);
	}

        $query5="select * from user_device where user_id='$user_id' AND login_logout=1";
        $result5 = $db->query($query5);
        $list5=$result5->rows;
        $language="select * from messages where language_id='$language_id' and message_id=28";
        $lang_result = $db->query($language);
        $lang_list=$lang_result->row;
        $message=$lang_list['message_name'];
        $done_ride= (String) $done_ride;
        $ride_status= (String) $ride_status;

        if (!empty($list5))
        {
            foreach ($list5 as $user)
            {
                $device_id = $user['device_id'];
                $flag = $user['flag'];
                if($flag == 1)
                {
                    IphonePushNotificationCustomer($device_id,$message,$done_ride,$ride_status);
                }
                else
                {
                    AndroidPushNotificationCustomer($device_id,$message,$done_ride,$ride_status);
                }
            }
        }else{
                $query5="select * from user where user_id='$user_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;
                $device_id=$list5['device_id'];
                if($device_id!="")
                {
                    if($list5['flag'] == 1)
                    {
                        IphonePushNotificationCustomer($device_id, $message,$done_ride,$ride_status);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id, $message,$done_ride,$ride_status);
                    }
                }
        }
                $query3="select * from done_ride where ride_id='$ride_id'";
                $result3 = $db->query($query3);
                $list=$result3->row;
                $list['payment_option_id']=$payment_option_id;
        $re = array('result'=> 1,'msg'=> $message,'details'	=> $list);

    }
    else {
        $re = array('result'=> 419,'msg'=> "No Record Found",);
    }
}
else
{
    $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>