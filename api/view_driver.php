<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

function sortByOrder($a, $b) 
{
      return $a['distance'] - $b['distance'];
}

$user_lat=$_REQUEST['user_lat'];
$user_long=$_REQUEST['user_long'];
$city_id=$_REQUEST['city_id'];
$car_type_id=$_REQUEST['car_type_id'];
$language_id=$_REQUEST['language_id'];

if($user_lat!= "" && $user_long!= "" && $city_id!= "" && $car_type_id!="")
{
		$query1="select * from driver where city_id='$city_id' and car_type_id='$car_type_id' and login_logout=1 and busy=0 and online_offline=1 and status=1";
		$result1 = $db->query($query1);
		$list1=$result1->rows;
				
		$info 	= $_REQUEST;
        unset($info['PHPSESSID']);
			
		if(!empty($list1))
		{
			foreach($list1 as $login3)
        	{
               	$driver_lat = $login3['current_lat'];
               	$driver_long =$login3['current_long'];
					
				$theta = $user_long - $driver_long;
  				$dist = sin(deg2rad($user_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($user_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
  				$dist = acos($dist);
  				$dist = rad2deg($dist);
  				$miles = $dist * 60 * 1.1515;
  				$unit = strtoupper("M");
  				if ($unit == "K") 
  				{
					$km=$miles* 1.609344;
  				} 
  				else if ($unit == "N") 
  				{
      	        	$miles * 0.8684;
  				}	 
  				else if($unit == "M")
  				{
	  				$miles;
 				}
				if($miles<= 20)
				{
					$c[] = array("driver_id"	=> $login3['driver_id'],"driver_lat"	=> $login3['current_lat'],"driver_long"	=> $login3['current_long']);
				}
			}
			if(!empty($c))
			{
				$d = array();
				foreach ($c as $key => $row)
				{
		    	      $d[$key] = $row['distance'];
				}
				array_multisort($d, SORT_DESC, $c);
				$re = array ("result" => 1, "msg" => $c);
			}
			else
			{
				$re = array('result'=> 0,'msg'	=> "No Record Found",);
			}
		}
		else 
		{
			$re = array('result'=> 0,'msg'	=> "No Record Found",);
		}	
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
