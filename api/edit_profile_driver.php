<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$driver_id=$_REQUEST['driver_id'];
$driver_name=$_REQUEST['driver_name'];
$driver_phone=$_REQUEST['driver_phone'];
$language_id=$_REQUEST['language_id'];

if($driver_id!="" && $driver_name!="" && $driver_phone!="")
{
	$sql1="select * from driver where driver_phone='$driver_phone' AND driver_id !='$driver_id'";
	$result1 = $db->query($sql1);
	$ex_rows1=$result1->num_rows;
	if($ex_rows1==0)
	{
		$sql2="UPDATE driver SET driver_name='$driver_name' , driver_phone='$driver_phone'  WHERE driver_id='$driver_id'";
		$db->query($sql2);

        $sql4="select * from driver where driver_id='$driver_id'";
		$result4 = $db->query($sql4);
		$list=$result4->row;
		$driver_image=$list['driver_image'];
		if(!empty($_FILES['driver_image'])) 
		{
			$img_name = $_FILES['driver_image']['name'];
			$filedir  = "../uploads/driver/";
			if(!is_dir($filedir)) mkdir($filedir, 0755, true);
			$fileext = strtolower(substr($_FILES['driver_image']['name'],-4));
			if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
			{
				if($fileext=="jpeg") 
				{
					$fileext=".jpg";
				}
				$pfilename = time()."driver_".$driver_id.$fileext;
				$filepath1 = "uploads/driver/".$pfilename;
				$filepath = $filedir.$pfilename;
				if(file_exists("../".$driver_image)) unlink("../".$driver_image);
				copy($_FILES['driver_image']['tmp_name'], $filepath);
						
				$upd_qry = "UPDATE driver SET driver_image ='$filepath1' where driver_id ='$driver_id'";
				$db->query($upd_qry);
				
				$sql3="select * from driver where driver_id='$driver_id'";
				$result3 = $db->query($sql3);
				$list=$result3->row;
				
				$car_type_id=$list['car_type_id'];
			    $car_model_id=$list['car_model_id'];
			
			$query4="select * from car_type where car_type_id='$car_type_id'";
			$result4 = $db->query($query4);
			$list4=$result4->row;
			$car_type_name=$list4['car_type_name'];
			
			$query5="select * from car_model where car_model_id='$car_model_id'";
			$result5 = $db->query($query5);
			$list5=$result5->row;
			$car_model_name=$list5['car_model_name'];
			$list['car_type_name']=$car_type_name;
			$list['car_model_name']=$car_model_name;

			$language="select * from messages where language_id='$language_id' and message_id=18";
			$lang_result = $db->query($language);
			$lang_list=$lang_result->row;
			$message_name=$lang_list['message_name'];

			$re = array('result'=> 1,'msg'=> $message_name,'details'	=> $list);
			}
		}
		else 
		{
			$sql3="select * from driver where driver_id='$driver_id'";
			$result3 = $db->query($sql3);
			$list=$result3->row;
			$car_type_id=$list['car_type_id'];
			$car_model_id=$list['car_model_id'];
			
			$query4="select * from car_type where car_type_id='$car_type_id'";
			$result4 = $db->query($query4);
			$list4=$result4->row;
			$car_type_name=$list4['car_type_name'];
			
			$query5="select * from car_model where car_model_id='$car_model_id'";
			$result5 = $db->query($query5);
			$list5=$result5->row;
			$car_model_name=$list5['car_model_name'];
            $list['car_type_name']=$car_type_name;
			$list['car_model_name']=$car_model_name;
            $language="select * from messages where language_id='$language_id' and message_id=18";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;
            $message_name=$lang_list['message_name'];

            $re = array('result'=> 1,'msg'=> $message_name,'details'	=> $list);
		}
		
	}else{
        $language="select * from messages where language_id='$language_id' and message_id=19";
        $lang_result = $db->query($language);
        $lang_list=$lang_result->row;
        $message_name=$lang_list['message_name'];

        $re = array('result'=> 0,'msg'=> $message_name);
    }
}
else
{
   $language="select * from messages where language_id='$language_id' and message_id=3";
   $lang_result = $db->query($language);
   $lang_list=$lang_result->row;
   $message_name=$lang_list['message_name'];
   $re = array('result' => 0,'msg'	=>$message_name);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>