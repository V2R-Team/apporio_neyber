<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");
include 'pn_android.php';
include 'pn_iphone.php';

$ride_id = $_REQUEST['ride_id'];
$driver_id = $_REQUEST['driver_id'];
$driver_token = $_REQUEST['driver_token'];
$language_id = $_REQUEST['language_id'];

if($ride_id != "" && $driver_id != "" && $driver_token!= "" && $language_id != "")
{
    $last_time_stamp = date("h:i:s A");
    $query="select * from ride_table where ride_id='$ride_id'";
    $result = $db->query($query);
    $list = $result->row;
    $ride_status_cancel = $list['ride_status'];
    $ride_type = $list['ride_type'];
    if($ride_type == 1){
     $ride_time = strtotime($list['ride_time']);
     $ride_expire_time =   strtotime(date('H:i:s',time() - 1 * 60));
    }else{
       $ride_time = 1;
       $ride_expire_time = 0;
    }
     

    if($ride_status_cancel != 0 && $ride_time >= $ride_expire_time)
    {
	$query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
				$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
		$date=$dt->format('M j, Y');
	$day=date("l");
	$date=$day.", ".$date;
	$new_time=date("H:i");

	$query5="UPDATE driver SET last_update='$new_time',last_update_date='$date' WHERE driver_id='$driver_id'" ;
					$db->query($query5);
					
	$query5="UPDATE table_user_rides SET driver_id='$driver_id' WHERE booking_id='$ride_id' AND ride_mode=1";
        $db->query($query5);

	
	$query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='3' WHERE ride_id='$ride_id'" ;
        $db->query($query1);
		$query3="select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id where ride_table.ride_id='$ride_id'";
			$result3 = $db->query($query3);
			$list3=$result3->row;
			$user_id=$list3['user_id'];
			$ride_status = $list3['ride_status'];

            $query4="select * from user_device where user_id='$user_id' AND login_logout=1";
            $result4 = $db->query($query4);
            $list4=$result4->rows;
            $language="select * from messages where language_id='$language_id' and message_id=25";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;
            $message=$lang_list['message_name'];
            $ride_id= (String) $ride_id;
            $ride_status= (String) $ride_status;
            if (!empty($list4))
			{
				foreach ($list4 as $login)
				{
					$device_id = $login['device_id'];
					$flag = $login['flag'];
                    if($flag == 1)
                    {
                        IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);

                    }
                }
			}else{
                   $query4="select * from user where user_id='$user_id'";
                   $result4 = $db->query($query4);
                   $list4=$result4->row;
                   $device_id=$list4['device_id'];
                   if($device_id!="")
                    {
                      if($list4['flag'] == 1)
                      {
                            IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
                      }
                      else
                      {
                            AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
                      }
                    }
            }
			$re = array('result'=> 1,'msg'=> "Booking Accepted!!",'details'	=> $list3,);
		}
	else {
			$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}else{
        $language="select * from messages where language_id='$language_id' and message_id=39";
	    $lang_result = $db->query($language);
        $lang_list=$lang_result->row;
        $message=$lang_list['message_name'];

  $re = array('result'=> 0,'msg'=> $message);
}
}
else 
{
	$re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
