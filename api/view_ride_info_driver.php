<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$ride_id=$_REQUEST['ride_id'];
$driver_token=$_REQUEST['driver_token'];
$language_id=$_REQUEST['language_id'];

if($ride_id!=""  && $driver_token!= "") 
{
	$query="select * from driver where driver_token='$driver_token'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	if($ex_rows==1)
	{
		$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
		$date=$dt->format('M j, Y');
		$day=date("l");
		$date=$day.", ".$date;
		$new_time=date("H:i");

		$query1="UPDATE driver SET last_update='$new_time',last_update_date='$date' WHERE driver_token='$driver_token'" ;
		$db->query($query1);
		
		$query="select * from ride_table where ride_id='$ride_id'";
		$result = $db->query($query);
		$list=$result->row;
	
		$user_id=$list['user_id'];
		$ride_status=$list['ride_status'];
        $coupan = $list['coupon_code'];
        $payment_option_id = $list['payment_option_id'];
        $query1234 ="select * from payment_option where payment_option_id='$payment_option_id'";
        $result1234 = $db->query($query1234);
        $list1234 = $result1234->row;
        $list['payment_option_id'] = $payment_option_id;
        $list['payment_option_name'] = $list1234['payment_option_name'];
		$query="select * from user where user_id='$user_id'";
		$result = $db->query($query);
		$list2=$result->row;

		$user_image=$list2['user_image'];
		$user_name=$list2['user_name'];
		$user_phone=$list2['user_phone'];
		$rating=$list2['rating'];
	
		$list['user_image']=$user_image;
		$list['user_name']=$user_name;
		$list['user_phone']=$user_phone;
		$list['rating']=$rating;
		
		if($ride_status==5)
		{
			$query="select * from done_ride where ride_id='$ride_id'";
			$result = $db->query($query);
			$list1=$result->row;
			
			$list['arrived_time']=$list1['arrived_time'];
			
			$list['begin_lat']="";
			$list['begin_long']="";
			$list['begin_location']=$list['pickup_location'];
			$list['begin_time']="";
			
			$list['end_lat']="";
			$list['end_long']="";
			$list['end_location']=$list['drop_location'];
			
			$list['amount']="";
			$list['distance']="";
			$list['time']="";
			$list['payment_status']="";
            $list['waiting_time'] = "";
            $list['waiting_price'] = "";
            $list['done_ride_time'] = "";
            $list['ride_time_price'] = "";
            $list['end_time'] = "";
            $list['coupons_code'] = "";
            $list['coupons_price'] = "";
            $list['total_amount']= "";
		}
		else if($ride_status==6)
		{
			$query="select * from done_ride where ride_id='$ride_id'";
			$result = $db->query($query);
			$list1=$result->row;
			
			$list['arrived_time']=$list1['arrived_time'];
			
			$list['begin_lat']=$list1['begin_lat'];
			$list['begin_long']=$list1['begin_long'];
			$list['begin_location']=$list1['begin_location'];
			$list['begin_time']=$list1['begin_time'];
			
			$list['end_lat']="";
			$list['end_long']="";
			$list['end_location']=$list['drop_location'];
			
			$list['amount']="";
			$list['distance']="";
			$list['time']="";
			$list['payment_status']="";
            $list['waiting_time'] = "";
            $list['waiting_price'] = "";
            $list['done_ride_time'] = "";
            $list['ride_time_price'] = "";
            $list['end_time'] = "";
            $list['coupons_code'] = "";
            $list['coupons_price'] = "";
            $list['total_amount']= "";
		}
	
		else if($ride_status==7)
		{
			$query="select * from done_ride where ride_id='$ride_id'";
			$result = $db->query($query);
			$list1=$result->row;
		
			$list['arrived_time']=$list1['arrived_time'];
			
			$list['begin_lat']=$list1['begin_lat'];
			$list['begin_long']=$list1['begin_long'];
			$list['begin_location']=$list1['begin_location'];
			$list['begin_time']=$list1['begin_time'];
				
			$list['end_lat']=$list1['end_lat'];
			$list['end_long']=$list1['end_long'];
			$list['end_location']=$list1['end_location'];
			
			$list['amount']=$list1['amount'];
			$list['distance']=$list1['distance'];
			$list['time']=$list1['tot_time'];
			$list['payment_status']=$list1['payment_status'];
            $list['waiting_time'] = $list1['waiting_time']." "."Min";
            $list['waiting_price'] = $list1['waiting_price'];
            $list['done_ride_time'] = $list1['tot_time']." "."Min";
            $list['ride_time_price'] = $list1['ride_time_price'];
            $list['end_time'] = $list1['end_time'];
            $total_amount = $list1['waiting_price']+$list['amount']+$list['ride_time_price'];
            if($coupan == "")
            {
                $coupon_code = "";
                $coupan_price = "";
            }else{
                $query2 ="select * from coupons where coupons_code='$coupan'";
                $result2 = $db->query($query2);
                $list2 = $result2->row;
                $coupon_type = $list2['coupon_type'];
                $coupon_code = $list2['coupons_code'];
                $coupan_price = $list2['coupons_price'];
                if($coupon_type == "Nominal"){
                    $total_amount = $total_amount-$coupan_price;
                }else{
                    $coupan_price = ($total_amount*$coupan_price)/100;
                    $total_amount = $total_amount-$coupan_price;
                }
            }
            $total_amount = (string) $total_amount;
            $list['coupons_code'] = $coupon_code;
            $list['coupons_price'] = $coupan_price;
            $list['total_amount']= $total_amount;
		}
		else
		{
			$list['arrived_time']="";
			
			$list['begin_lat']="";
			$list['begin_long']="";
			$list['begin_location']=$list['pickup_location'];
			$list['begin_time']="";
			
			$list['end_lat']="";
			$list['end_long']="";
			$list['end_location']=$list['drop_location'];
			
			$list['amount']="";
			$list['distance']="";
			$list['time']="";
			$list['payment_status']="";
            $list['waiting_time'] = "";
            $list['waiting_price'] = "";
            $list['done_ride_time'] = "";
            $list['ride_time_price'] = "";
            $list['end_time'] = "";
            $list['coupons_code'] = "";
            $list['coupons_price'] = "";
            $list['total_amount']= "";
		}
		$re = array('result'=> 1,'msg'=> "",'details'	=> $list);
	}
	else 
	{
		$re = array('result'=> 419,'msg'=> "No Record Found",);
	}
}
else
{
	$re = array('result'=> 0,'msg'=> "Require fields Missing!!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>