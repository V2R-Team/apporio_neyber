<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

function sortByOrder($a, $b)
{
    return $a['distance'] - $b['distance'];
}

$user_id = $_REQUEST['user_id'];
$coupon_code = $_REQUEST['coupon_code'];
$pickup_lat = $_REQUEST['pickup_lat'];
$pickup_long = $_REQUEST['pickup_long'];
$pickup_location = $_REQUEST['pickup_location'];
$drop_lat = $_REQUEST['drop_lat'];
$drop_long = $_REQUEST['drop_long'];
$drop_location = $_REQUEST['drop_location'];
$car_type_id=$_REQUEST['car_type_id'];
$language_id=$_REQUEST['language_id'];
$payment_option_id=$_REQUEST['payment_option_id'];
$card_id=$_REQUEST['card_id'];
$me = "user id".$user_id."pickup lat".$pickup_lat."pickup_long".$pickup_long."pickup_location".$pickup_location."drop_lat".$drop_lat."drop lat".$drop_long."drop location".$drop_location."car tpe".$car_type_id."payment".$payment_option_id;
if($user_id !="" && $pickup_lat !="" && $pickup_long !="" && $pickup_location !="" && $drop_lat !="" &&
    $drop_long !="" && $drop_location !="" &&  $car_type_id !="" && $payment_option_id !="")
{

        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;

    $query3="select * from driver where car_type_id='$car_type_id' and online_offline = 1 and driver_admin_status=1 and busy=0 and login_logout=1";
    $result3 = $db->query($query3);
    $ex_rows=$result3->num_rows;

    if($ex_rows==0)
    {
        $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
        $last_time_stamp = date("h:i:s A");
        $query1="INSERT INTO no_driver_ride_table(user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date1','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp')";
	$db->query($query1);
        $re = array('result'=> 0,'msg'=> "No Driver Online",);
    }
    else {

        $list3=$result3->rows;
        $c = array();
        foreach($list3 as $login3)
        {
            $driver_lat = $login3['current_lat'];
            $driver_long =$login3['current_long'];

            $theta = $pickup_long - $driver_long;
            $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km=$miles* 1.609344;
            if($km<= 10)
            {
                $c[] = array("driver_id"	=> $login3['driver_id'],"distance" => $km,);
            }
        }

        if(!empty($c)){

            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
             $date = date("Y-m-d");
            $last_time_stamp = date("h:i:s A");
            $query1="INSERT INTO ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp,date) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date1','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp','$date')";

            $db->query($query1);
            $last_id = $db->getLastId();

            $query3="select * from ride_table where ride_id='$last_id'";
            $result3 = $db->query($query3);
            $list=$result3->row;
            $ride_status =$list['ride_status'];

            usort($c, 'sortByOrder');
            $c = $c[0]['driver_id'];
            $driver_id=$c;

            $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id, allocated_ride_status) VALUES ('$last_id','$driver_id','1')";
            $db->query($query5);

            $query5="INSERT INTO table_user_rides(booking_id,ride_mode,user_id) VALUES ('$last_id','1','$user_id')";
            $db->query($query5);

            $query4="select * from driver where driver_id='$driver_id'";
            $result4 = $db->query($query4);
            $list4=$result4->row;
            $device_id=$list4['device_id'];
            $language="select * from messages where language_id=1 and message_id=32";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;

            $message=$lang_list['message_name'];
            $ride_id= (String) $last_id;
            $ride_status= (String) $ride_status;


            if($device_id!="")
            {
                if($list4['flag'] == 1)
                {
                    IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
                else
                {
                    AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
            $re = array('result'=> 1,'msg'=> "".$km,'details'=> $list);

        }else{
            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
            $last_time_stamp = date("h:i:s A");
            $query1="INSERT INTO no_driver_ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,ride_image,car_type_id,payment_option_id,card_id,last_time_stamp) 
	VALUES ('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date','$time','1','1','$image','$car_type_id','$payment_option_id','$card_id','$last_time_stamp')";
            $db->query($query1);
            $re = array('result'=> 0,'msg'=> "No Nearest Driver",);
        }

    }

}
else
{
    $re = array('result'=> 0,'msg'=> $me);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>