<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
?>

<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Driver Maps</h3>
            <nav id="sidebar-wrapper" class="">
      <div id="driver_details">
        <ul class="sidebar-nav ">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i style="color:#ffffff;" class="fa fa-times"></i></a>
            <li style="clear:both;"></li>

        </ul>
      </div>
    </nav>

  </div>
  <div class="row">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOfKh2baXWXfu3rIQDnTnIWxviWpjSyNI"></script>
    <div id="map" style="width:100%;height:550px;  "></div>



    <script type="text/javascript">

var locations = [
    <?php
    $query="select * from driver where online_offline=1 and busy != 1 ";
    $result = $db->query($query);
    $list=$result->rows;
    foreach($list as $driver) {
        $driver_id = $driver['driver_id'];
        $driver_name = $driver['driver_name'];
        $current_lat  = $driver['current_lat'];
        $current_long = $driver['current_long'];
    ?>
['<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $driver_id ?>)" ><?php echo $driver_name?></div>', <?php echo $current_lat ?>,<?php echo $current_long ?>,'<?php echo ICON_URL ?>'],
<?php };?>
];

var map = new google.maps.Map(document.getElementById('map'),{
zoom: 2,
center: new google.maps.LatLng(28.4120558, 77.0433644),
mapTypeId: google.maps.MapTypeId.ROADMAP
});
var infowindow = new google.maps.InfoWindow(), marker, i;

for (i = 0; i < locations.length; i++) {

marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][3]
            });
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			infowindow.setContent(locations[i][0]);
			infowindow.open(map, marker);
		}
	})(marker, i));
}

</script>


<script>

  function showmapdetails(driver_id) {
    $('#sidebar-wrapper').addClass("active");
    $.ajax({
     url: "ajax.php?action=driver_details1&driver_id="+driver_id,
     success: function(res) {
      $('#driver_details').html(res);
     }
    });
  }

  function showmapdetails1(driver_id) {
    $('#sidebar-wrapper').addClass("active");
    $.ajax({
     url: "ajax.php?action=driver_details1_ride&driver_id="+driver_id,
     success: function(res) {
      $('#driver_details').html(res);
     }
    });
  }


</script>
  </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>