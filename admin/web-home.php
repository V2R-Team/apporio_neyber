<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from web_home";
$result = $db->query($query);
$list=$result->row;
if(isset($_POST['save']))
{
    $query1="UPDATE web_home SET features2_desc='".$_POST['features2_desc']."',features2_heading='".$_POST['features2_heading']."',features1_desc='".$_POST['features1_desc']."',features1_heading='".$_POST['features1_heading']."',parallax_details='".$_POST['parallax_details']."',parallax_heading2='".$_POST['parallax_heading2']."',parallax_heading1='".$_POST['parallax_heading1']."',heading3_details='".$_POST['heading3_details']."',heading3='".$_POST['heading3']."',heading2_details='".$_POST['heading2_details']."',heading2='".$_POST['heading2']."',heading1_details='".$_POST['heading1_details']."',heading1='".$_POST['heading1']."',itunes_url='".$_POST['itunes_url']."',google_play_url='".$_POST['google_play_url']."',market_places_desc='".$_POST['market_places_desc']."',app_details='".$_POST['app_details']."',app_heading1='".$_POST['app_heading1']."',web_title='".$_POST['web_title']."',web_footer='".$_POST['web_footer']."',app_heading='".$_POST['app_heading']."' WHERE id=1";
    $db->query($query1);

    if(!empty($_FILES['features2_bg']))
    {

        $imgInp= $_FILES['features2_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['features2_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "features2_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['features2_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET features2_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }


    if(!empty($_FILES['features1_bg']))
    {

        $imgInp= $_FILES['features1_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['features1_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "features1_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['features1_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET features1_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['parallax_bg']))
    {

        $imgInp= $_FILES['parallax_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['parallax_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "parallax_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['parallax_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET parallax_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }


    if(!empty($_FILES['parallax_screen']))
    {

        $imgInp= $_FILES['parallax_screen']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['parallax_screen']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "parallax_screen".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['parallax_screen']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET parallax_screen='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['heading3_img']))
    {

        $imgInp= $_FILES['heading3_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading3_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading3_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading3_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading3_img='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }


    if(!empty($_FILES['heading2_img']))
    {

        $imgInp= $_FILES['heading2_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading2_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading2_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading2_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading2_img='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['heading1_img']))
    {

        $imgInp= $_FILES['heading1_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading1_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading1_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading1_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading1_img='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }


    if(!empty($_FILES['itunes_btn']))
    {

        $imgInp= $_FILES['itunes_btn']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['itunes_btn']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "itunes_btn".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['itunes_btn']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET itunes_btn='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['banner_img']))
    {

        $imgInp= $_FILES['banner_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['banner_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "banner_".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['banner_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET banner_img='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['app_screen1'])){

        $imgInp= $_FILES['app_screen1']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['app_screen1']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "app_screen1".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['app_screen1']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET app_screen1='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['app_screen2'])){

        $imgInp= $_FILES['app_screen2']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['app_screen2']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "app_screen2".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['app_screen2']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET app_screen2='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['google_play_btn'])){

        $imgInp= $_FILES['google_play_btn']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['google_play_btn']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "google_play_btn".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['google_play_btn']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET google_play_btn='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }



    $db->redirect("home.php?pages=web-home");
}



?>
<style>
    .topbnr {max-height: 400px !important;}
    .features_img{ width: 130px; margin: 0px 0px 20px 0px;}
    .page-title button {float: none;}
    .fileUpload{ float:right;}
    .tp_rht{float:right;}
</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Home Page</h3>
        <span class="tp_rht">

        <form method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">


      <input type="submit" class=" btn btn-info" id="save" name="save" value="Save" >
      <a href="home.php?pages=web-pages" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>

  </span>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Website Title</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="web_title" id="web_title" value="<?php echo $list['web_title'];?>" placeholder="Website Title">
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Footer</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="web_footer" id="web_footer" value="<?php echo $list['web_footer'];?>" placeholder="Copyright">
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading row">
                    <h3 class="panel-title col-md-6">Banner</h3>
                    <span class=" col-md-6">
          <div class="fileUpload btn btn-purple"> <span><i class="ion-upload m-r-5"></i>Upload</span>

               <input type="file" name="banner_img" id="imgInp" class="upload"/>
          </div>

        </span>
                </div>
                <div class="panel-body row"> <img class="col-md-12 topbnr" id="topbanner" name="banner_img" src="../<?php echo $list['banner_img'];?>" alt="your image" /> </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Mobile App</h3></div>
                <div class="panel-body">

                    <div class="form-group col-md-6">
                        <label for="">Main Heading</label>
                        <input type="text" class="form-control" name="app_heading" id="app_heading" value="<?php echo $list['app_heading'];?>" placeholder="App Heading">
                    </div>


                    <div class="form-group col-md-6">
                        <label for="">App Heading</label>
                        <input type="text" class="form-control" name="app_heading1" id="app_heading1" value="<?php echo $list['app_heading1'];?>" placeholder="Heading">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">App Screen 1</label>
                        <input type="file" class="form-control" name="app_screen1" id="app_screen1"  placeholder="App Screenshots">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">App Screen 2</label>
                        <input type="file" class="form-control" name="app_screen2" id="app_screen2"  placeholder="App Screenshots">
                    </div>



                    <div class="form-group col-md-12">
                        <label for="">App Description</label>
                        <textarea style="height:150px;" class="form-control" id="app_details" name="app_details"   placeholder="Your App Details"><?php echo $list['app_details'];?></textarea>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="">Market Places Description</label>
                        <input type="text" class="form-control" name="market_places_desc" id="market_places_desc"  value="<?php echo $list['market_places_desc'];?>" placeholder="Market Places Description">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">Google Play Button</label>
                        <input type="file" class="form-control" name="google_play_btn" id="google_play_btn" placeholder="Android App Store">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">Google Play App Url</label>
                        <input type="text" class="form-control" name="google_play_url" id="google_play_url" value="<?php echo $list['google_play_url'];?>" placeholder="Google Play App Url">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">iTunes Button</label>
                        <input type="file" class="form-control" name="itunes_btn" id="itunes_btn" placeholder="iOS App Store">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">iTunes App Url</label>
                        <input type="text" class="form-control" name="itunes_url" id="itunes_url"  value="<?php echo $list['itunes_url'];?>" placeholder="iTunes App Url">
                    </div>



                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $list['heading1'];?></h3>
                </div>
                <div class="panel-body">

                    <center><img class="features_img" id="img01" src="../<?php echo $list['heading1_img'];?>" alt="your image" /></center>


                    <div class="form-group">
                        <label class="sr-only" for="">Image</label>
                        <input type="file" id="heading1_img" name="heading1_img" class="form-control" placeholder="Choose Image...">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Heading</label>
                        <input type="text" class="form-control" id="heading1" value="<?php echo $list['heading1'];?>" name="heading1" placeholder="Your Heading">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Your Details</label>
                        <textarea class="form-control" id="heading1_details" name="heading1_details" placeholder="Your Details"><?php echo $list['heading1_details'];?></textarea>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $list['heading2'];?></h3>
                </div>
                <div class="panel-body">

                    <center><img class="features_img" id="img02" src="../<?php echo $list['heading2_img'];?>" alt="your image" /></center>


                    <div class="form-group">
                        <label class="sr-only" for="">Image</label>
                        <input type="file" name="heading2_img" id="heading2_img" class="form-control" placeholder="Choose Image...">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Heading</label>
                        <input type="text" id="heading2" name="heading2"  class="form-control"value="<?php echo $list['heading2'];?>" id="heading2" placeholder="Your Heading">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Your Details</label>
                        <textarea class="form-control" id="heading2_details" name="heading2_details" placeholder="Your Details"><?php echo $list['heading2_details'];?></textarea>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $list['heading3'];?></h3>
                </div>
                <div class="panel-body">
                    <center><img class="features_img" id="img03" src="../<?php echo $list['heading3_img'];?>" alt="your image" /></center>
                    <div class="form-group">
                        <label class="sr-only" for="">Image</label>
                        <input type="file" name="heading3_img" id="heading3_img"  class="form-control" placeholder="Choose Image...">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Heading</label>
                        <input type="text" class="form-control" id="heading3" name="heading3" value="<?php echo $list['heading3'];?>" id="heading3" placeholder="Your Heading">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="">Your Details</label>
                        <textarea class="form-control" id="heading3_details" name="heading3_details" placeholder="Your Details"><?php echo $list['heading3_details'];?></textarea>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Parallax Background</h3></div>
                <div class="panel-body">

                    <div class="form-group col-md-6">
                        <label for="">Heading 1</label>
                        <input type="text" class="form-control" name="parallax_heading1" id="parallax_heading1" value="<?php echo $list['parallax_heading1'];?>" placeholder="Heading 1">
                    </div>


                    <div class="form-group col-md-6">
                        <label for="">Heading 2</label>
                        <input type="text" class="form-control" name="parallax_heading2" id="parallax_heading2" value="<?php echo $list['parallax_heading2'];?>" placeholder="Heading 2">
                    </div>

                    <div class="form-group col-md-12">
                        <label for="">App Description</label>
                        <textarea style="height:150px;" class="form-control" id="parallax_details" name="parallax_details" placeholder="Your Description"><?php echo $list['parallax_details'];?></textarea>
                    </div>



                    <div class="form-group col-md-6">
                        <label for="">App Screenshots</label>
                        <input type="file" class="form-control" name="parallax_screen" id="parallax_screen" placeholder="App Screen">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">Background Image</label>
                        <input type="file" class="form-control" name="parallax_bg" id="parallax_bg" placeholder="App Screen">
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Feature 1</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="">Heading</label>
                        <input type="text" class="form-control" name="features1_heading" id="features1_heading" value="<?php echo $list['features1_heading'];?>" placeholder="Heading">
                    </div>

                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" id="features1_desc" name="features1_desc" placeholder="Your Description"><?php echo $list['features1_desc'];?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Background Image</label>
                        <input type="file" class="form-control" name="features1_bg" id="features1_bg" placeholder="Background Image">
                    </div>


                </div>
            </div>
        </div>

        <!-- Horizontal form -->
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Feature 2</h3></div>
                <div class="panel-body">

                    <div class="form-group">
                        <label for="">Heading</label>
                        <input type="text" class="form-control" name="features2_heading" id="features2_heading" value="<?php echo $list['features2_heading'];?>" placeholder="Heading">
                    </div>

                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" id="features2_desc" name="features2_desc" placeholder="Your Description"><?php echo $list['features2_desc'];?></textarea>
                    </div>


                    <div class="form-group">
                        <label for="">Background Image</label>
                        <input type="file" class="form-control" name="features2_bg" id="features2_bg" placeholder="Background Image">
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>

</form>

</section>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#topbanner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });







</script>
</body>
</html>
