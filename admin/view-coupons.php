<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from coupons ORDER BY coupons_id";
$result = $db->query($query);
$list=$result->rows;
if(isset($_POST['deletee']))
{
    $delqry1="DELETE from coupons where coupons_id='".$_POST['delete']."'";
    $db->query($delqry1);
    $db->redirect("home.php?pages=view-coupons");
}
if (isset($_POST['delete'])) {

    $checkbox = $_POST['chk'];
    $count = count($checkbox);
    for($i = 0; $i < $count; $i++) {
        $id = (int) $checkbox[$i];
        if ($id > 0)
        {
            $query1="DELETE FROM coupons WHERE coupons_id='$id'";
            $db->query($query1);
            $db->redirect("home.php?pages=view-coupons");
        }
    }
}

if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE coupons SET status='".$_GET['status']."' WHERE coupons_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view-coupons");
}


if(isset($_POST['savechanges']))
{
    $query2="UPDATE coupons SET coupons_code='".$_POST['coupons_code']."', coupons_price='".$_POST['coupons_price']."',start_date='".$_POST['start_date']."', expiry_date='".$_POST['expiry_date']."' where coupons_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-coupons");
}


?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Promo Codes</h3>
            <span class="tp_rht">
            <a href="home.php?pages=add-coupons" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Promo"><i class="fa fa-plus"></i></a>
            <a href="javascript:godelete()" alt="Delete"><button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button></a>
      </span>
        </div>
        <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="10%">
                                            <label class="option block mn">
                                                <input type="checkbox" name="checkbox" id="main_ch" onClick="checkall(this)" >
                                                <span class="checkbox mn"></span>
                                            </label>
                                            &nbsp;</th>
                                        <th width="5%">S.No</th>
                                        <th>Promo  Code</th>
                                        <th width="5%">Promo  Price</th>
                                        <th>Start Date</th>
                                        <th>Expiry Date</th>
                                        <th>Promo  Type</th>
                                        <th width="8%">Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $viewcoupon){?>
                                        <tr>
                                            <td>
                                                <label class="option block mn" style="width: 55px;">
                                                    <input name="chk[]" type="checkbox" id="chk" value="<?php echo $viewcoupon['coupons_id']?>">
                                                    <span class="checkbox mn"></span>
                                                </label>
                                            </td>
                                            <td><?php echo $viewcoupon['coupons_id'];?></td>
                                            <td><?php
                                                $coupons_code = $viewcoupon['coupons_code'];
                                                    echo $coupons_code;
                                                ?></td>
                                            <td><?php
                                                $coupons_price = $viewcoupon['coupons_price'];
                                                    echo $coupons_price;
                                                ?></td>
                                            <td><?php
                                                $start_date = $viewcoupon['start_date'];
                                                echo $start_date;
                                                ?></td>
                                            <td><?php
                                                $expiry_date = $viewcoupon['expiry_date'];
                                                    echo $expiry_date;
                                                ?></td>

                                            <td><?php
                                                $coupon_type = $viewcoupon['coupon_type'];
                                                    echo $coupon_type;
                                                ?></td>

                                            <?php
                                            if($viewcoupon['status']==1) {
                                                ?>
                                                <td class="">
                                                    <label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="">
                                                    <label class="label label-default" > Deactive</label> 
                                                </td>
                                            <?php } ?>
                                            <td>
                                                <div class="row action_row" style="width:95px;">
                                                <span data-target="#<?php echo $viewcoupon['coupons_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                   <?php   if($viewcoupon['status'] == 2) { ?>                
     <a href="home.php?pages=view-coupons&status=1&id=<?php echo $viewcoupon['coupons_id'];?>" data-original-title="Active"  class="btn menu-icon btn_eye"> <i class="fa fa-eye"></i> </a>
								   <?php }else{ ?>
    <a href="home.php?pages=view-coupons&status=2&id=<?php echo $viewcoupon['coupons_id'];?>" data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>

                                                    <span data-target="#delete<?php echo $viewcoupon['coupons_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>


                                                </div>                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- End row -->

    </div>
</form>
<?php foreach($list as $viewcoupon){?>
    <div class="modal fade" id="<?php echo $viewcoupon['coupons_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Coupon Details</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Coupon Type</label>
                                    <input type="text" class="form-control"  placeholder="Coupon Type" name="coupon_type" value="<?php echo $viewcoupon['coupon_type'];?>" id="coupon_type" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Coupon Code</label>
                                    <input type="text" class="form-control"  placeholder="Coupon Code" name="coupons_code" value="<?php echo $viewcoupon['coupons_code'];?>" id="coupons_code" required>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Coupon Price</label>
                                    <input type="text" class="form-control"  placeholder="Coupon Price" name="coupons_price" value="<?php echo $viewcoupon['coupons_price'];?>" id="coupons_price" required>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Start Date</label>
                                    <input type="date" class="form-control"  placeholder="mm/dd/yyyy" name="start_date" value="<?php echo $viewcoupon['start_date'];?>" id="start_date" required>
                                </div>
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Expiry Date</label>
                                    <input type="date" class="form-control"  placeholder="mm/dd/yyyy" name="expiry_date" value="<?php echo $viewcoupon['expiry_date'];?>" id="expiry_date" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $viewcoupon['coupons_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php }?>
</section>
<!-- Main Content Ends -->


<?php
foreach($list as $Delcoupon){ ?>
    <div class="modal fade" id="delete<?php echo $Delcoupon['coupons_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete</h4>
                </div>
                <form  method="post">
                    <div class="modal-body">
                        <div class="row">
                            <h4>Do You Really Want To Delete This Coupon?</h4></div>
                        <div class="modal-footer">
                            <button type="submit" name="deletee" value="<?php echo $Delcoupon['coupons_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
<?php } ?>


</body></html>