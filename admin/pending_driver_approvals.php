<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
    if(isset($_POST['seabt12']))
	{
		$sel=$_POST["sear12"];
		$in=$_POST["seain12"];
		$query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id where driver.$sel='$in' AND driver.verification_status=0";
	}
	else{
    $query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id WHERE driver.verification_status=0 ORDER BY driver.driver_id DESC";
	}
	$result = $db->query($query);
	$list=$result->rows;



if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE driver SET commission='".$_POST['commission']."' where driver_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=drivers");
     }			
  if (isset($_GET['driver_id'])){
    $date = date("Y-m-d");
      $query2="UPDATE driver SET verification_status=1,verification_date='$date' where driver_id='".$_GET['driver_id']."'";
      $db->query($query2);
      echo '<script>alert("Driver Verify Successfully")</script>';
      $db->redirect("home.php?pages=pending-driver-approvals");
  }
        
?>
<script>
function myfunction()
{
  var option = document.getElementById('option').value;
  var search = document.getElementById('search').value;
        if(option == "")
        {
            alert("Select Search By");
            return false;
        }
        if(search  == "")
        {
            alert("Enter Keyword To Search");
            return false;
        }
}
</script>

  <div class="wraper container-fluid">
    <div class="page-title">
 
      <h3 class="title">Pending Approval Drivers</h3>
      <span class="tp_rht">
         <a href="home.php?pages=drivers" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>

    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
            <div class="col-md-12">
				<form method="post">
						<div class="col-md-2 col-sm-2 col-xs-12">
							  <select class="form-control" name="sear12">
								<option value="driver_name">---Search by---</option>
								<option value="driver_name">Name</option>
								<option value="driver_email">Email</option>
								<option value="driver_phone">Mobile</option>
							  </select>
							</div>
					
					<div class="col-md-2 col-xs-12 form-group ">
					  <div class="input-group">
						<input type="text" id="first-name" name="seain12" placeholder="Search" class="form-control col-md-12 col-xs-12" required>
					  </div>
					</div>

					
					<div class="col-sm-2  col-xs-12 form-group ">
						  <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
				   </div> 
				</form>
               <div class="col-sm-4"></div>
               <div class="dt-buttons btn-group col-sm-2">
               		<a class="btn btn-default buttons-copy buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons" href="#" onclick="msg()"><span>Import</span></a>
               		<a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons" href="#" onclick="msg()"><span>Export</span></a>
               </div>
               
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>

                      <th>Driver Code</th>
                      <th>Driver Name</th>
                      <th>Email Id</th>
                      <th>Mobile No.</th>              
                      <th width="5%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $driver){?>
                    <tr>
                      <td><a href="#driverdetails<?php echo $driver['driver_id'];?>"  data-toggle="modal"><span class="badge bg-warning"><?php echo $driver['driver_id'];?></span></a></td>
                      <td>
					  	<?php
							$driver_name = $driver['driver_name'];
								echo $driver_name;
						?>
                      </td>
                      <td>
					  	<?php
							$driver_email = $driver['driver_email'];
                            $e1=substr($driver_email,0,2);
                            $e2= explode("@",$driver_email);
                            $domain=$e2[1];
                            echo $e1."*****@".$domain;
						?>
                      </td>
                      <td >
					  	<?php
							$driver_phone = $driver['driver_phone'];
                            $e1=substr($driver_phone,7);
                            echo "********".$e1;
						?>
                      </td>
                    		
		      <td >
		      
       <div class="row action_row" style="width:95px;">
                 <span data-target="#edit<?php echo $driver['driver_id'];?>" data-toggle="modal"><a data-original-title="Edit Commission"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
			<a href="home.php?pages=verify-driver&driver_id=<?php echo $driver['driver_id']; ?>" data-original-title="Click To Verfiy"  data-toggle="tooltip" class="btn menu-icon btn_eye"> <i class="fa fa-eye"></i> </a>
			</div>
               </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
<?php foreach($list as $driver){?>
<div class="modal fade" id="edit<?php echo $driver['driver_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Driver Commission Details</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
      <div class="modal-body">
        <div class="row">
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Commission</label>
              <input type="number" class="form-control"  placeholder="Commission" name="commission" value="<?php echo $driver['commission'];?>" id="commission" required>
            </div>
          </div>
 
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $driver['driver_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php }?>



<!--Driver Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
 foreach($list as $driver){?>
<div class="modal fade" id="driverdetails<?php echo $driver['driver_id'];?>" role="dialog"> <?php echo $driver['driver_id'];?>
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content" style="padding:20px !important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Driver Full Details</h4>
      </div>
        <div class="modal-body" style="max-height: 500px; overflow-x: auto;">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
              <tr>
                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($driver['driver_image'] != '' && isset($driver['driver_image'])){echo '../'.$driver['driver_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                <td><table style="margin-left:20px;" aling="center" border="0">
                    <tbody>
                      <tr>
                        <th class=""> Name</th>
                        <td class=""><?php
                             $drivername = $driver['driver_name'];
                             echo $drivername;
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Email</th>
                        <td class=""><?php
                            $driver_email = $driver['driver_email'];
                            $e1=substr($driver_email,0,2);
                            $e2= explode("@",$driver_email);
                            $domain=$e2[1];
                            echo "*****@".$domain;
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Phone</th>
                        <td class=""><?php
                            $driver_phone = $driver['driver_phone'];
                            $e1=substr($driver_phone,7);
                            echo "*****@".$e1;
                            ?></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <th class="">Device</th>
                <td class=""><?php
                   	  $phonedevice = $driver['flag'];
                    switch ($phonedevice){
                        case "1":
                            echo "Iphone";
                            break;
                        case "2":
                            echo "Android";
                            break;
                        default:
                            echo "------";
                    }
                   ?></td>
              </tr>
              <tr>
                <th class="">Car Type</th>
                <td class=""><?php
                         $car_type_name = $driver['car_type_name'];
                         echo $car_type_name;
                  ?>
                </td>
              </tr>
               <tr>
                <th class="">Commission</th>
                <td class=""><?php
                       $commission = $driver['commission'];
                       echo $commission."%";
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">Car Model</th>
                <td class=""><?php
                       $car_model_name = $driver['car_model_name'];
                       echo $car_model_name;
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">Car Number</th>
                <td class=""><?php
                      $carnumber = $driver['car_number'];
                      echo $carnumber;
                   ?></td>
              </tr>
              <tr>
                <th class="">City Name</th>
                <td class=""><?php 
                    $city_name = $driver['city_name'];
                    echo $city_name;
                    ?></td>
              </tr>
              <tr>
                <th class="">Register Date</th>
                <td class=""><?php
                      $registerdate = $driver['register_date'];
                      echo $registerdate;
                   ?></td>
              </tr>
              <tr>
                <th class="">Current Location</th>
                <td class=""><?php
                      $currentlocation = $driver['current_location'];
                                                        if($currentlocation=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $currentlocation;
                                                           }
                                                       ?></td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
           <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <tfoot>
            	<tr>
                	<th>Licence</th>
                    <th>Registration Certificate (RC)</th>
                    <th>Insurance</th>
                </tr>
            	<tr>
					<?php $filenotexit="http://apporio.co.uk/apporiotaxi/uploads/driver/filenotexit.png"; ?>
                    <?php $fileexit="http://apporio.co.uk/apporiotaxi/uploads/driver/fileexit.png"; ?>
                	
                     <td>
                     	<a target="_blank" href="<?php if($driver['license'] != '' && isset($driver['license'])){echo '../'.$driver['license'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['license'] != '' && isset($driver['license'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                      </td>
                      
                     <td>
                     	<a target="_blank" href="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo '../'.$driver['rc'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                         </a>
                     </td>
                     <td>
                     	<a target="_blank" href="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo '../'.$driver['insurance'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                     </td>
                    
                </tr>
            </tfoot>
           </table>
        </div>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>
 
</section>
</body></html>