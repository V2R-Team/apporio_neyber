<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query = "select * from ride_table where ride_id='".$_GET['id']."'";
$result = $db->query($query);
$ride=$result->row;
$lat = $ride['pickup_lat'];
$pickup_long = $ride['pickup_long'];
$pickup_location = $ride['pickup_location'];
$drop_lat = $ride['drop_lat'];
$drop_long = $ride['drop_long'];
$drop_location = $ride['drop_location'];
?>
<style>
    .divmap{ height: 500px;
    width: 100%;
    }
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg&sensor=false"></script>
<script type="text/javascript">
    var markers = [
        {
            "title": 'Pick Up Location',
            "lat": '<?php echo $lat;?>',
            "lng": '<?php echo $pickup_long;?>',
            "description": '<?php echo $pickup_location;?>'
        }
        ,
        {
            "title": 'Drop Up Location',
            "lat": '<?php echo $drop_lat;?>',
            "lng": '<?php echo $drop_long;?>',
            "description": '<?php echo $drop_location;?>'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        var path = new google.maps.MVCArray();
        var service = new google.maps.DirectionsService();


        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="row">
        <div id="dvMap" class="divmap col-md-4"></div>
    </div>
</div>

