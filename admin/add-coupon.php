<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$errorMsg = "";
$query="select * from coupon_type";
$result = $db->query($query);
$listcoupontype = $result->rows;
if(isset($_POST['save']))
{
    if($_POST['coupons_price'] != "")
    {
        $query2="INSERT INTO coupons (coupons_code,coupons_price,start_date,expiry_date,coupon_type,status) VALUES ('".$_POST['coupons_code']."','".$_POST['coupons_price']."','".$_POST['start_date']."','".$_POST['expiry_date']."','".$_POST['coupon_type']."',1)";
        $db->query($query2);
        $errorMsg = "Coupon Suceessfully Added!";
    }else{
        $query2="INSERT INTO coupons (coupons_code,coupons_price,start_date,expiry_date,coupon_type,status) VALUES ('".$_POST['coupons_code']."','".$_POST['coupons_percantage']."','".$_POST['start_date']."','".$_POST['expiry_date']."','".$_POST['coupon_type']."',1)";
        $db->query($query2);
        $errorMsg = "Coupon Suceessfully Added!";
    }
$db->redirect("home.php?pages=add-coupons");
}

?>
<link href="css/calander.css" rel="stylesheet" />
<script src="js/calander_jquery.js"></script>
<script src="js/calander_jquery-ui.js"></script>
<script>
    var j = jQuery.noConflict();
    j(document).ready(function() {
        j("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }).attr('readOnly', 'true');
        j("#datepicker1").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0}).attr('readOnly', 'true');
    });
</script>
<script type="text/javascript">
    function Validate() {
        var coupon = document.getElementById("coupon_type").value;
        var code = document.getElementById("coupons_code").value;
        var price = document.getElementById("coupons_price").value
        if (coupon == "")
        {
            alert("Please Select Coupon Type.");
            return false;
        }
        if (code == "")
        {
            alert("Please Enter Coupons Code");
            return false;
        }
        if(document.getElementById("datepicker").value == "")
        {
            alert("Please Select Coupons Start Date");
            return false;
        }
        if(document.getElementById("datepicker1").value == "")
        {
            alert("Please Select Coupons End Date");
            return false;
        }
        if(document.getElementsByName("couponLimit").value == "")
        {
            alert("Please Enter Coupons Usage Limit.");
            return false;
        }
        if(coupon == "Percentage" && document.getElementById("coupons_percantage").value == "")
        {
            alert("Please Select Coupons Percantage");
            return false;
        }
        if(coupon == "Percentage" && document.getElementById("coupons_percantage").value > 100)
        {
            alert("Please Select Coupons Percantage Value Less Then 100");
            return false;
        }
        if(coupon == "Nominal" && isNaN(price))
        {
            alert("Coupon Price Should Numeric value only");
            return false;
        }
        if (document.getElementById('total_usage_limit').value == "")
        {
            alert("Enter Total Usage Limit of Promocode")
        }
        if (document.getElementById('par_user_limit').value == "")
        {
            alert("Enter Par User Limit of Promocode")
        }
    }
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $("select").change(function(){
            $(this).find("option:selected").each(function(){
                if($(this).attr("value")=="Nominal"){

                    $(".red").show();

                    $(".green").hide();
                }
                else if($(this).attr("value")=="Percentage"){

                    $(".green").show();

                    $(".red").hide();
                }

                else{
                    $(".box").hide();
                }
            });
        }).change();
    });
</script>
<style>
    .total{ display:none;}
</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Promo</h3>
        <span>
      <h3 style="float:right; color:red;"><?php echo $errorMsg;?></h3>
    </span>
      <span class="tp_rht">
           <a href="home.php?pages=view-coupons" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
       </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" onsubmit="return Validate()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Promo Type</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="coupon_type" id="coupon_type">
                                        <option value="">--Please Select Promo Type--</option>
                                        <?php foreach($listcoupontype as $coupontype): ?>
                                            <option value="<?=  $coupontype['coupon_type_name'];?>" ><?php echo $coupontype['coupon_type_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Promo Code*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Promo Code" name="coupons_code" id="coupons_code">
                                </div>
                            </div>

                            <div class="form-group total red">
                                <label class="control-label col-lg-2">Promo Price*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Promo Price" name="coupons_price" id="coupons_price">
                                </div>
                            </div>

                            <div class="form-group total green">
                                <label class="control-label col-lg-2">Promo Percantage*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Promo Percantage" name="coupons_percantage" id="coupons_percantage">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Promo Start Date*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Promo Start Date" name="start_date" id="datepicker" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Promo Expiry Date*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Promo Expiry Date" id="datepicker1" name="expiry_date">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Total Usage Limit*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Total Usage Limit" id="total_usage_limit" name="total_usage_limit">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Par User Limit*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Par User Limit" id="par_user_limit" name="par_user_limit">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white"  id="save" name="save" value="Add Promo " >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
</body>
</html>
