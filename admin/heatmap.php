<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
?>
    <style>
      #map {
        height: 550px;
        width:100%;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      #floating-panel {
        background-color: #fff;
        border: 1px solid #999;
        left: 25%;
        padding: 5px;
        position: absolute;
        top: 10px;
        z-index: 5;
      }
    </style>
 <div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Heat Maps</h3>
  </div>
<div>
  <div class="row">
    <div id="floating-panel">
      <button onclick="toggleHeatmap()" class="btn btn-success">Toggle Heatmap</button>
      <button onclick="changeGradient()" class="btn btn-success">Change gradient</button>
      <button onclick="changeRadius()" class="btn btn-success">Change radius</button>
      <button onclick="changeOpacity()" class="btn btn-success">Change opacity</button>
    </div>
    <div id="map"></div>
    </div>
    <script>

      var map, heatmap;

      function initMap() {
         bounds = new google.maps.LatLngBounds();
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: {lat: 0, lng: 0},
          mapTypeId: 'roadmap',
          
        });

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: getPoints(),
          map: map
        });
        changeRadius();
        changeGradient();         
      }

      function toggleHeatmap() {
        heatmap.setMap(heatmap.getMap() ? null : map);
      }

      function changeGradient() {
        var gradient = [
          'rgba(0, 255, 255, 0)',
          'rgba(0, 255, 255, 1)',
          'rgba(0, 191, 255, 1)',
          'rgba(0, 127, 255, 1)',
          'rgba(0, 63, 255, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(0, 0, 223, 1)',
          'rgba(0, 0, 191, 1)',
          'rgba(0, 0, 159, 1)',
          'rgba(0, 0, 127, 1)',
          'rgba(63, 0, 91, 1)',
          'rgba(127, 0, 63, 1)',
          'rgba(191, 0, 31, 1)',
          'rgba(255, 0, 0, 1)'
        ]
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
      }

      function changeRadius() {
        heatmap.set('radius', heatmap.get('radius') ? null : 20);
      }

      function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
      }

      
      function getPoints() {
        return [
        <?php
            $query="select * from ride_table Where pickup_lat != '' AND pickup_long!=''";
	    $result = $db->query($query);
	    $list=$result->rows;
           foreach($list as $ride) {
	        $pickup_lat = $ride['pickup_lat'];
	        $pickup_long = $ride['pickup_long'];
            ?>
          new google.maps.LatLng(<?php echo $pickup_lat ?>, <?php echo $pickup_long ?>),
           <?php } ?>
        ];
       }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3Y-lk2bCkNlUWHWH35YwRzMgV_snOpuM&libraries=visualization&callback=initMap">
    </script>
  </section>
<!-- Main Content Ends -->

</body></html>