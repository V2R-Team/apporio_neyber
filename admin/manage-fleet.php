<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
$db->redirect("index.php");
}
include('common.php');

$query="select * from car_type";
$result = $db->query($query);
$list=$result->rows;

$query5="select * from car_make";
$result5 = $db->query($query5);
$makelist=$result5->rows;

$query51="select * from car_make";
$result51 = $db->query($query51);
$makelist1=$result51->rows;

if(isset($_GET['status']) && isset($_GET['id']))
{
$query1="UPDATE car_type  SET car_admin_status='".$_GET['status']."' WHERE car_type_id='".$_GET['id']."'";
$db->query($query1);
$db->redirect("home.php?pages=manage-fleet");
}

if(isset($_GET['makestatus']) && isset($_GET['makeid']))
{
$query9="UPDATE car_make  SET make_status='".$_GET['makestatus']."' WHERE make_id='".$_GET['makeid']."'";
$db->query($query9);
$db->redirect("home.php?pages=manage-fleet");
}


if(isset($_POST['savechanges']))
{
if($_FILES['car_type_image']['name'] == ""){
$query2="UPDATE car_type  SET car_type_name='".$_POST['car_type_name']."',car_type_name_french='".$_POST['car_type_french']."' where car_type_id='".$_POST['savechanges']."'";
$db->query($query2);
$db->redirect("home.php?pages=manage-fleet");
}else{
$img_name = $_FILES['car_type_image']['name'];
$filedir  = "../uploads/car/";
if(!is_dir($filedir)) mkdir($filedir, 0755, true);
$fileext = strtolower(substr($_FILES['car_type_image']['name'],-4));
if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
{
if($fileext=="jpeg")
{
$fileext=".jpg";
}
$pfilename = "editcar_".$_POST['savechanges'].$fileext;
$filepath1 = "uploads/car/".$pfilename;
$filepath = $filedir.$pfilename;
copy($_FILES['car_type_image']['tmp_name'], $filepath);

$query2="UPDATE car_type  SET car_type_name='".$_POST['car_type_name']."',car_type_name_french='".$_POST['car_type_french']."',car_type_image='$filepath1' where car_type_id='".$_POST['savechanges']."'";
$db->query($query2);
$db->redirect("home.php?pages=view-car-type");
}
}
}

if(isset($_POST['savechanges2']))
{
if($_FILES['car_image']['name'] == ""){
$query21="UPDATE car_make  SET make_name='".$_POST['make_name']."' where make_id='".$_POST['savechanges2']."'";
$db->query($query21);
$db->redirect("home.php?pages=manage-fleet");
}else{
$img_name = $_FILES['car_image']['name'];
$filedir  = "../uploads/car/";
if(!is_dir($filedir)) mkdir($filedir, 0755, true);
$fileext = strtolower(substr($_FILES['car_image']['name'],-4));
if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
{
if($fileext=="jpeg")
{
$fileext=".jpg";
}
$pfilename = "editcar_".$_POST['savechanges2'].$fileext;
$filepath1 = "uploads/car/".$pfilename;
$filepath = $filedir.$pfilename;
copy($_FILES['car_image']['tmp_name'], $filepath);

$query2="UPDATE car_make  SET make_name='".$_POST['make_name']."',make_img='$filepath1' where make_id='".$_POST['savechanges2']."'";
$db->query($query2);
$db->redirect("home.php?pages=manage-fleet#make");
}
}
}


if(isset($_POST['save']))
{

$query2="INSERT INTO car_type (car_type_name) VALUES ('".$_POST['car_type_name']."')";
$db->query($query2);
$car_type_id = $db->getLastId();
if(!empty($_FILES['car_type_image']))
{
$img_name = $_FILES['car_type_image']['name'];
$filedir  = "../uploads/car/";
if(!is_dir($filedir)) mkdir($filedir, 0755, true);
$fileext = strtolower(substr($_FILES['car_type_image']['name'],-4));
if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
{
if($fileext=="jpeg")
{
$fileext=".jpg";
}
$pfilename = "car_".$car_type_id.$fileext;
$filepath1 = "uploads/car/".$pfilename;
$filepath = $filedir.$pfilename;
copy($_FILES['car_type_image']['tmp_name'], $filepath);

$upd_qry = "UPDATE car_type SET car_type_image ='$filepath1' where car_type_id ='$car_type_id'";
$db->query($upd_qry);
}
}
$msg = "Vehicle Details Save Successfully";
echo '<script type="text/javascript">alert("'.$msg.'")</script>';
$db->redirect("home.php?pages=manage-fleet");
}


//ADD MAKE
if(isset($_POST['save2']))
{

$query6="INSERT INTO car_make (make_name) VALUES ('".$_POST['make_name']."')";
$db->query($query6);
$make_id = $db->getLastId();
    if(!empty($_FILES['make_image']))
    {
       echo $img_name = $_FILES['make_image']['name'];
        $filedir  = "../uploads/car/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['make_image']['name'],-4));
        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "car_".$make_id.$fileext;
            $filepath1 = "uploads/car/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['make_image']['tmp_name'], $filepath);

           $upd_qry2 = "UPDATE car_make SET make_img ='$filepath1' where make_id ='$make_id'";
            $db->query($upd_qry2);
        }
    }
    else{
        echo "<script>alert('No Image Selected.')</script>";
    }
    $msg = "Vehicle Make Details Saved Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=manage-fleet");
}


/*ADD CAR MODEL*/
$brandqry='select * from car_make';
$brandrslt=$db->query($brandqry);
$brandlist=$brandrslt->rows;

if(isset($_POST['save3'])) {
$query4="INSERT INTO car_model (car_type_id,car_model_name,car_make) VALUES('".$_POST['car_type_id1']."','".$_POST['car_model_name1']."','".$_POST['car_make1']."')";
$db->query($query4);
$car_model_id = $db->getLastId();
if(!empty($_FILES['car_model_image1']))
{
$img_name = $_FILES['car_model_image1']['name'];
$filedir  = "../uploads/car/";
if(!is_dir($filedir)) mkdir($filedir, 0755, true);
$fileext = strtolower(substr($_FILES['car_image']['name'],-4));
if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
{
if($fileext=="jpeg")
{
$fileext=".jpg";
}
$pfilename = "car_".$car_model_id.$fileext;
$filepath1 = "uploads/car/".$pfilename;
$filepath = $filedir.$pfilename;
copy($_FILES['car_model_image1']['tmp_name'], $filepath);

$upd_qry = "UPDATE car_model SET car_model_image ='$filepath1' where car_model_image ='$car_model_id'";
$db->query($upd_qry);
}
}
$msg = "Vehicle Model Details Save Successfully";
echo '<script type="text/javascript">alert("'.$msg.'")</script>';
  $db->redirect("home.php?pages=manage-fleet");
}

$query123 = "select * from car_model INNER JOIN car_type ON car_model.car_type_id=car_type.car_type_id ORDER BY car_model_id DESC";
$result123 = $db->query($query123);
$list3 = $result123->rows;

if(isset($_GET['status']) && isset($_GET['id']))
{
$query1="UPDATE car_model SET car_model_status='".$_GET['status']."' WHERE car_model_id='".$_GET['id']."'";
$db->query($query1);
$db->redirect("home.php?pages=manage-fleet");
}


if(isset($_POST['savechanges3']))
{
if($_FILES['car_model_image']['name'] == ""){
$query2="UPDATE car_model SET car_model_name='".$_POST['car_model_name']."',car_model_name_french='".$_POST['car_model_french']."',car_type_id='".$_POST['car_type_id']."',car_make='".$_POST['car_make']."',car_model='".$_POST['car_model']."',car_year='".$_POST['car_year']."',car_color='".$_POST['car_color']."' where car_model_id='".$_POST['savechanges3']."'";
$db->query($query2);
$db->redirect("home.php?pages=manage-fleet");
}else{
$img_name = $_FILES['car_model_image']['name'];
$filedir  = "../uploads/car/";
if(!is_dir($filedir)) mkdir($filedir, 0755, true);
$fileext = strtolower(substr($_FILES['car_model_image']['name'],-4));
if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
{
if($fileext=="jpeg")
{
$fileext=".jpg";
}
$pfilename = "editcar_".$_POST['savechanges3'].$fileext;
$filepath1 = "uploads/car/".$pfilename;
$filepath = $filedir.$pfilename;
copy($_FILES['car_model_image']['tmp_name'], $filepath);
$query2="UPDATE car_model SET car_model_name='".$_POST['car_model_name']."',car_model_name_french='".$_POST['car_model_french']."', car_type_id='".$_POST['car_type_id']."',car_model_image='$filepath1',car_make='".$_POST['car_make']."',car_model='".$_POST['car_model']."',car_year='".$_POST['car_year']."',car_color='".$_POST['car_color']."' where car_model_id='".$_POST['savechanges3']."'";
$db->query($query2);
$db->redirect("home.php?pages=manage-fleet");
}
}
}

//delete type

if(isset($_POST['delete']))
     {
       $delqry1="DELETE from car_type where car_type_id='".$_POST['delete']."'";
       $db->query($delqry1);
       $db->redirect("home.php?pages=manage-fleet");
     }

//delete make

if(isset($_POST['delete1']))
     {
       $delqry2="DELETE from car_make where make_id='".$_POST['delete1']."'";
       $db->query($delqry2);
       $db->redirect("home.php?pages=manage-fleet",'refresh');
     }

//delete model

if(isset($_POST['delete2']))
     {
       $delqry3="DELETE from car_model where car_model_id='".$_POST['delete2']."'";
       $db->query($delqry3);
       $db->redirect("home.php?pages=manage-fleet",'refresh');
     }

?>

<script type="text/javascript">
$(document).on('click', '#close-preview', function(){
$('.image-preview').popover('hide');
// Hover befor close the preview
$('.image-preview').hover(
function () {
$('.image-preview').popover('show');
},
function () {
$('.image-preview').popover('hide');
}
);
});

$(function() {
// Create the close button
var closebtn = $('<button/>', {
type:"button",
text: 'x',
id: 'close-preview',
style: 'font-size: initial;',
});
closebtn.attr("class","close pull-right");
// Set the popover default content
$('.image-preview').popover({
trigger:'manual',
html:true,
title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
content: "There's no image",
placement:'bottom'
});
// Clear event
$('.image-preview-clear').click(function(){
$('.image-preview').attr("data-content","").popover('hide');
$('.image-preview-filename').val("");
$('.image-preview-clear').hide();
$('.image-preview-input input:file').val("");
$(".image-preview-input-title").text("Browse");
});
// Create the preview image
$(".image-preview-input input:file").change(function (){
var img = $('<img/>', {
id: 'dynamic',
width:250,
height:200
});
var file = this.files[0];
var reader = new FileReader();
// Set preview image into the popover data-content
reader.onload = function (e) {
$(".image-preview-input-title").text("Change");
$(".image-preview-clear").show();
$(".image-preview-filename").val(file.name);
img.attr('src', e.target.result);
$(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
}
reader.readAsDataURL(file);
});
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });

    $(document).ready(function () {
        $("#datatable_filter label:first").after("<button type='button' style='margin-top: 20px; margin-left: 10px;'  class='btn addBtn btn-success' data-toggle='modal' data-target='#popup' > <i class='fa fa-plus-circle'></i></button>");
        $("#datatable_filter").css({"margin-top":"-24px"});
        $("#datatable1_filter label:first").after("<button type='button' style='margin-top: 20px; margin-left: 10px;'  class='btn addBtn btn-success' data-toggle='modal' data-target='#makepopup' > <i class='fa fa-plus-circle'></i></button>");
        $("#datatable1_filter").css({"margin-top":"-24px"});
        $("#datatable2_filter label:first").after("<button type='button' style='margin-top: 20px; margin-left: 10px;'  class='btn addBtn btn-success' data-toggle='modal' data-target='#carmodelpopup' > <i class='fa fa-plus-circle'></i></button>");
        $("#datatable2_filter").css({"margin-top":"-24px"});
    });
</script>

<style>.clear{clear:both !important;}</style>
<div class="wraper container-fluid">

<div class="row m-t-30">
<div class="col-sm-8" style="margin-top:-20px;margin-left:-10px;">
<ul id="myTab" class="nav-tabs navi navi_tabs">
<li class="active"><a data-toggle="tab" href="#type"><i class="fa fa-taxi" aria-hidden="true"></i> <b>Vehicle Type</b></a></li>
<li class=""><a data-toggle="tab" href="#make"><i class="fa fa-wrench" aria-hidden="true"></i> <b>Vehicle Make</b></a></li>
<li class=""><a data-toggle="tab" href="#model"><i class="fa fa-car" aria-hidden="true"></i> <b>Vehicle Model</b></a></li>
</ul>
<div class="panel panel-default p-0">
<div class="panel-body p-0">

<div class="tab-content m-0">
<div id="type" class="tab-pane active">
<div class="user-profile-content">
<form role="form" method="post" >
<form method="post" name="frm">
    <div class="">

                            <div class="">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Vehicle Name</th>
                                        <th>Vehicle Image</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $cartype){?>
                                        <tr>
                                            <td>
                                                <?php
                                                $car_type_name  = $cartype['car_type_name'];
                                                echo $car_type_name;
                                                ?>
                                            </td>

                                            <td align="center"><img src="../<?php echo $cartype['car_type_image'];?>" style="margin-top:-25px;margin-bottom:-25px;" width="80px" height="80px"></td>


                                            <?php
                                            if($cartype['car_admin_status']==1) {
                                                ?>
                                                <td class="">
                                                	<label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="">
                                                	<label class="label label-default" > Deactive</label>
                                                </td>
                                            <?php } ?>
                                            <td>
                                              <div class="row action_row" style="width:95px;">
                                                           <span data-target="#<?php echo $cartype['car_type_id']?>" data-toggle="modal"><a data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            <span>
                                          
                                            <?php if($cartype['car_admin_status']==1){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=manage-fleet&status=2&id=<?php echo $cartype['car_type_id']?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=manage-fleet&status=1&id=<?php echo $cartype['car_type_id']?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>
                                                <span data-target="#delete<?php echo $cartype['car_type_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>
                                           
                                           </span>

                                              </div>
</td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>


        </div>
        <!-- End row -->

    </form>
<!-- Page Content Ends -->
<!-- ================== -->
<?php foreach($list as $cartype){?>
    <div class="modal fade" id="<?php echo $cartype['car_type_id']?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Vehicle Brand Details</h4>
                </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Vehicle Name</label>
                                    <input type="text" class="form-control"  placeholder="Car Name" name="car_type_name" value="<?php echo $cartype['car_type_name'];?>" id="car_type_name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Vehicle Name In French</label>
                                    <input type="text" class="form-control"  placeholder="Car Name In french" name="car_type_french" value="<?php echo $cartype['car_type_name_french'];?>" id="car_type_french" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Vehicle Image</label>
                                    <input type="file" name="car_type_image" id="car_type_image" />
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        
                    </div>

            </div>
        </div>
    </div>
<?php }?>


</div>
</div>
</div>
<div class="clear"></div>


<div id="make" class="tab-pane">
<div class="user-profile-content">
<form role="form" method="post" onSubmit="return validatelogin()">
<div class="row">
<div class="col-md-12">
<div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
    <table id="datatable1" class="table table-striped table-bordered table-responsive">
        <thead>
        <tr>
            <th >Make Name</th>
            <th>Image</th>
            <th>Status</th>
            <th >Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($makelist as $carmake){?>
            <tr>

                <td width="30%" >
                    <?php
                    $car_make_name  = $carmake['make_name'];
                    echo $car_make_name;
                    ?>
                </td>

                <td align="center"><img src="../<?php echo $carmake['make_img'];?>"  width="50px" height="50px" style="margin-top:-10px;margin-bottom:-10px;"></td>


                <?php
                if($carmake['make_status']==1) {
                    ?>
                    <td class="">
                            <label class="label label-success" >Active</label>
                    </td>
                    <?php
                } else {
                    ?>
                    <td class="">
                            <label class="label label-default" >Deactive</label>
                    </td>
                <?php } ?>
                <td width="25%">
                                              <div class="row action_row"style="width:160px;">

<span data-target="#<?php echo $carmake['make_id']?>" data-toggle="modal"><a data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            <span>
                                          
                                            <?php if($carmake['make_status']==1){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=manage-fleet&makestatus=2&makeid=<?php echo $carmake['make_id']?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=manage-fleet&makestatus=1&makeid=<?php echo $carmake['make_id']?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>
                                                <span data-target="#deleteMake<?php echo $carmake['make_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>
                                            </span>
                                              </div>
</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
</div>

</div>
</form>
</div>
</div>

    <?php foreach($makelist1 as $editcar){?>
        <div class="modal fade" id="<?php echo $editcar['make_id']?>" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content starts-->

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Edit Make Details</h4>
                    </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="field-3" class="control-label">Make Name</label>
                                        <input type="text" class="form-control"  placeholder="Make Name" name="make_name" value="<?php echo $editcar['make_name'];?>" id="make_name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="field-3" class="control-label">Vehicle Image</label>
                                        <input type="file" name="car_image" id="car_image" />
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                            <button type="submit" name="savechanges2" value="<?php echo $editcar['make_id']?>" class="btn btn-info">Save Changes</button>
                        </div>
                </div>
            </div>
        </div>
    <?php }?>


    <div id="model" class="tab-pane">
<div class="user-profile-content">
<form role="form" method="post" onSubmit="return validatelogin()">
<div class="col-md-12" >
<div class="row">
<div class="">
<div class="">
<div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                    <table id="datatable2" class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Vehicle Type</th>
                            <th>Vehicle Make</th>
                            <th>Vehicle Model</th>
                            <th >Status</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $dummyImg="http://apporio.co.uk/apporiotaxi/uploads/car/cardummy.png";
                        foreach($list3 as $viewmodel){?>
                            <tr>
                                <td><?php
                                    $car_type_name = $viewmodel['car_type_name'];
                                    echo $car_type_name;
                                    ?>
                                </td>
                                <td><?php
                                    $car_make = $viewmodel['car_make'];
                                    echo $car_make;
                                    ?>
                                </td>
                                <td><?php
                                    $carmodel = $viewmodel['car_model_name'];
                                    echo $carmodel;
                                    ?>
                                </td>
                                <?php
                                if($viewmodel['car_model_status']==1) {
                                    ?>
                                    <td class="">
                                        <a href="home.php?pages=manage-fleet&status=2&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Active">
                                            <label class="label label-success"> Active</label>
                                        </a>
                                    </td>
                                    <?php
                                } else {
                                    ?>
                                    <td class="">
                                        <a href="home.php?pages=manage-fleet&status=1&id=<?php echo $viewmodel['car_model_id']?>" class="" title="Deactive">
                                            <label class="label label-default" >Deactive</label>
                                        </a>
                                    </td>
                                <?php } ?>

                                <td width="25%">
                                              <div class="row action_row"style="width:160px;">
                                              <span data-target="#<?php echo $viewmodel['car_model_id']?>" data-toggle="modal"><a data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            <span>
                                          
                                            <?php if($viewmodel['car_model_status']==1){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=manage-fleet&status=2&id=<?php echo $viewmodel['car_model_id']?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=manage-fleet&status=1&id=<?php echo $viewmodel['car_model_id']?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>
                                                <span data-target="#deleteModel<?php echo $viewmodel['car_model_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>

                                           
                                           </span>

                                              </div>
</td>

                            </tr>
                        <?php }?>
                        </tbody>

                    </table>
                </div>
                </div>
</div>
</div>
</form>
</div>
</div>
<div class="clear"></div>

</div>
</div>
</div>
</div></div>
<div class="col-md-4">
<div class="panel">
  <div class="panel-heading">
    <h3 class="panel-title">How it Works ?</h3>
  </div>
  <div class="panel-body">
    This module is basically a directory of your fleet in all Countries and City. You will have add a rate card in <b>Fare Management Module.</b> You can also commision in Fare Management Module.
  </div>
</div>
</div>
</div>
</div>




<div class="modal fade" id="popup" role="dialog">
<div class="modal-dialog">

<!-- Modal content starts-->

<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title fdetailsheading">Add New Vehicle </h4>
</div>
<form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
<br>
<div class="form-group ">
<label for="lastname" class="control-label col-lg-2">Name  *</label>
<div class="col-lg-9">
<input type="text" class="form-control" placeholder="Vehicle Type Name IN English" name="car_type_name" id="car_type_name" required/>
</div>
</div>
<div class="form-group ">
<label for="username" class="control-label col-lg-2">Upload Image*</label>
<div class="col-lg-9">

<div class="input-group image-preview">
<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
<span class="input-group-btn">
<!-- image-preview-clear button -->
<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
<span class="glyphicon glyphicon-remove"></span> Clear
</button>
<!-- image-preview-input -->
<div class="btn btn-default image-preview-input">
<span class="glyphicon glyphicon-folder-open"></span>
<span class="image-preview-input-title">Browse</span>
<input type="file" accept="image/png, image/jpeg, image/gif" name="car_type_image" id="car_type_image" required /> <!-- rename it -->
</div>
</span>
</div>

</div>
</div>
<div class="form-group">
<div class="col-lg-offset-2 col-lg-9" align="center" >
<input type="submit" class=" btn btn-info black-background white" id="save" name="save" value="Save" >
</div>
</div>
</form>
</div>
</div>
</div>




<!--CAR MODEL-->
<!--ADD MODEL-->
<div class="modal fade" id="carmodelpopup" role="dialog">
<div class="modal-dialog">

<!-- Modal content starts-->

<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title fdetailsheading">Add Vehicle Model</h4>
</div>
<form  method="post"  enctype="multipart/form-data">
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Vehicle Type</label>
<select class="form-control" name="car_type_id1" id="car_type_id" required>
<?php
        foreach ($list as $data){
?>
<option value="<?php echo $data['car_type_id'];?>"><?php echo $data['car_type_name']; ?></option>
<?php }?>

</select>
</div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <label for="field-3" class="control-label">vehicle Make</label>
        <select class="form-control" name="car_make1" id="car_make" required>
            <?php
            foreach ($brandlist as $carbrand){
                ?>
                <option value="<?php echo $carbrand['make_name'];?>"><?php echo $carbrand['make_name']; ?></option>
            <?php }?>

        </select>
    </div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Vehicle Model</label>
<input type="text" class="form-control"  placeholder="Model Name" name="car_model_name1"  id="car_model_name" required>
</div>
</div>

</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
<button type="submit" name="save3"  class="btn btn-info">Save</button>
</div>
</form>
</div>
</div>
</div>


<!--EDIT-->
<?php foreach($list3 as $viewmodel){?>
<div class="modal fade" id="<?php echo $viewmodel['car_model_id'];?>" role="dialog">
<div class="modal-dialog">

<!-- Modal content starts-->

<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title fdetailsheading">Edit Car Model Details</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Car Type</label>
<select class="form-control" name="car_type_id" id="car_type_id" required>
<?php
$car_type_id = $viewmodel['car_type_id'];
$car_type_name = $viewmodel['car_type_name'];
?>
<option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
<?php foreach($list123 as $car) :?>
    <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
<?php endforeach; ?>

</select>
</div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Model Name</label>
<input type="text" class="form-control"  placeholder="Model Name" name="car_model_name" value="<?php echo $viewmodel['car_model_name'];?>" id="car_model_name" required>
</div>
</div>


<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Model Name In French</label>
<input type="text" class="form-control"  placeholder="Model Name In French" name="car_model_french" value="<?php echo $viewmodel['car_model_name_french'];?>" id="car_model_french" required>
</div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Make</label>
<input type="text" class="form-control"  placeholder="Make" name="car_make" value="<?php echo $viewmodel['car_make'];?>" id="car_make" required>
</div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Model</label>
<input type="text" class="form-control"  placeholder="Model" name="car_model" value="<?php echo $viewmodel['car_model'];?>" id="car_model" required>
</div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Year</label>
<input type="text" class="form-control"  placeholder="Year" name="car_year" value="<?php echo $viewmodel['car_year'];?>" id="car_year" required>
</div>
</div>

<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Color</label>
<input type="text" class="form-control"  placeholder="Color" name="car_color" value="<?php echo $viewmodel['car_color'];?>" id="car_color" required>
</div>
</div>


<div class="col-md-12">
<div class="form-group">
<label for="field-3" class="control-label">Car Model Image</label>
<input type="file" name="car_model_image" id="car_model_image" />
</div>
</div>

</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
<button type="submit" name="savechanges3" value="<?php echo $viewmodel['car_model_id'];?>" class="btn btn-info">Save Changes</button>
</div>
</div>
</div>
</div>
<?php }?>

<!--make popup-->
<div class="modal fade" id="makepopup" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content starts-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title fdetailsheading">Add New Vehicle Make </h4>
            </div>
            <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
                <br>
                <div class="form-group ">
                    <label for="lastname" class="control-label col-lg-2">Name  *</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" placeholder="Make Name" name="make_name" id="make_name" required/>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="username" class="control-label col-lg-2">Upload Image*</label>
                    <div class="col-lg-9">

                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
<!-- image-preview-clear button -->
<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
<span class="glyphicon glyphicon-remove"></span> Clear
</button>
                                <!-- image-preview-input -->
<div class="btn btn-default image-preview-input">
<span class="glyphicon glyphicon-folder-open"></span>
<span class="image-preview-input-title">Browse</span>
<input type="file" accept="image/png, image/jpeg, image/gif" name="make_image" id="make_image" required /> <!-- rename it -->
</div>
</span>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-9" align="center" >
                        <input type="submit" class=" btn btn-info black-background white" id="save2" name="save2" value="Save" >
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--DELETE TYPE-->
<?php
foreach($list as $DelCarType){ ?>
    <div class="modal fade" id="delete<?php echo $DelCarType['car_type_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Vehicle Type</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>Do You Really Want To Delete This Vehicle Type?</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $DelCarType['car_type_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>

<!--DELETE MAKE-->
<?php
foreach($makelist as $DelCarMake){ ?>
    <div class="modal fade" id="deleteMake<?php echo $DelCarMake['make_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Vehicle Make</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>Do You Really Want To Delete This Vehicle Maker?</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete1" value="<?php echo $DelCarMake['make_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>

<!--DELETE MODEL-->
<?php
foreach($list3 as $DelCarModel){ ?>
    <div class="modal fade" id="deleteModel<?php echo $DelCarModel['car_model_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Vehicle Model</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>Do You Really Want To Delete This Vehicle Model?</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete2" value="<?php echo $DelCarModel['car_model_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>

        </div>
    </div>
    </div>
<?php } ?>

</body></html>