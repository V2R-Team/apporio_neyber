<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from web_contact WHERE id=1";
$result = $db->query($query);
$list = $result->row;

if(isset($_POST['save']))
{
    $title = $_POST['title'];
    $title1 = $_POST['title1'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $skype = $_POST['skype'];
    $query2="UPDATE web_contact SET address='$address',title='$title',title1='$title1',email='$email',phone='$phone',skype='$skype' WHERE id=1";
    $db->query($query2);
    $db->redirect("home.php?pages=web-contact-us");
}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
    function initialize() {

        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    function validatelogin() {
        var title = document.getElementById('title').value;
        var title1 = document.getElementById('title1').value;
        var email = document.getElementById('email').value;
        if(title == "")
        {
            alert("Enter Title");
            return false;
        }
        if(title1 == "")
        {
            alert("Enter Title Two");
            return false;
        }
        if(email == "")
        {
            alert("Enter Email");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Contect Us Page</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Title*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Title" name="title" value="<?= $list['title'] ?>" id="title" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Title Two*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Title Two" name="title1"  value="<?= $list['title1'] ?>" id="title1" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Email" name="email" id="email" value="<?= $list['email'] ?>"  >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Phone" name="phone" id="phone" value="<?= $list['phone'] ?>" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Skype*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Skype" name="skype" id="skype" value="<?= $list['skype'] ?>" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Map Address*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Map Address" name="address" id="address" value="<?= $list['address'] ?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
