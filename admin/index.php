<?php 
include_once '../apporioconfig/start_up.php'; 

if(isset($_POST['login']) && $_POST['login'] == "LOGIN") {

$query="select * from admin WHERE admin_username='".$_POST['admin_username']."' and admin_password='".$_POST['admin_password']."'";
  $result = $db->query($query);
  $ex_rows=$result->num_rows;
    $list = $result->row;
  if($ex_rows == 1){
        $_SESSION['ADMIN']['ID'] = $list['admin_id'];
        $_SESSION['ADMIN']['UN'] = $list['admin_fname'];
        $_SESSION['ADMIN']['Role'] = $list['admin_role'];
        header("Location: home.php?pages=dashboard");
     }else{
        $msg = "Username And Password Wrong";
      echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    }
 }
?>

<script type="text/javascript">
function validatelogin() {
    re = /^[A-Za-z ]+$/;
    te = /^[0-9]+$/;
    se = /^[0-9A-Za-z ]+[A-Za-z ]+$/;
  if( document.getElementById('admin_fname').value == "" ) {
   alert('Please Enter Your UserName');
   document.getElementById('admin_fname').focus();
   return false;
  }
  if(document.getElementById('password').value == "")
  {
  alert("Please Enter Your Password");
  document.getElementById('password').focus();
  return false;
  }
  return true;
}


</script>    
    

<!DOCTYPE html>
<html lang="en">
    

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Neyber Admin">
        <meta name="author" content="">

        <title>Neyber || Admin</title>
        
        <link href="images/favicon.png" rel="icon"/>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>


  <link href="css/home.css" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
<head>

<script>
$(document).ready(function(){

  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })

})
</script>

    </head>



    <body>
    
    <section class="container-fluid main_header">
      <div class="lft">
        <a class="" target="_blank" href="http://apporio.org/Neyber/">Website</a>
      </div>
      <div class="rgt">
        <ul>
           <li style="padding-left:10px;"><a target="_blank" href="http://apporio.org/Neyber/Welcome/rider_signin"><i class="glyphicon glyphicon-user"></i>Rider Login</a></li>
           <li><a target="_blank" href="http://apporio.org/Neyber/Welcome/driver_signin"><i class="glyphicon glyphicon-log-in"></i>Driver Login</a></li>
        </ul>
      </div>
    </section>
    

<div class="loginmain">

  <ul class="tabs">
    <li class="tab-link current col-md-4 col-sm-4- col-xs-4" data-tab="tab-1"  onclick="setdetails()">Super Admin</li>
    <li class="tab-link col-md-4 col-sm-4- col-xs-4" data-tab="tab-1" onclick="setdetail()">Dispatcher</li>
    <li class="tab-link col-md-4 col-sm-4- col-xs-4" data-tab="tab-1" onclick="setdetails1()">Billing</li>
    <div class="clear"></div>
  </ul>

  <div id="tab-1" class="tab-content current">
    
    
     <form class="form-horizontal m-t-20" method="post" onSubmit="return validatelogin()">
                                            
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username"  name="admin_username" id="admin_fname" value="" />
                        </div>
                    </div>
                    <div class="form-group ">
                        
                        <div class="col-xs-12">
                            
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="admin_password" id="password" value="" />
                        </div>
                    </div>

                   
                    
                    
                    <div class="form-group text-right">
                        <div class="col-xs-12">
                       

                       <button class="login" type="submit" name="login" value="LOGIN" id="login">Log In</button>
                    <div class="form-group text-left">
          <div class="col-sm-7">
                            <span style="cursor:pointer;" data-toggle="modal" data-target="#forgotpassword">Forgot your password?</span> 
                       </div>
                    </div>
          </div>
                  </div>
                    
                </form>
  </div>
  
  
</div>

    
    </body>

</html>
