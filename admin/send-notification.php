<?php
include('common.php');
include_once '../apporioconfig/start_up.php';
if ($_SESSION['ADMIN']['ID'] == "") {
    $db->redirect("home.php?pages=index");
}
include 'pn_android.php';
include 'pn_iphone.php';

//NOTIFICATIONS FOR ALL

if (isset($_POST['sendAll'])) {

    $query2 = "INSERT INTO push_messages (push_message,push_app) VALUES ('" . $_POST['messageAll'] . "','" . $_POST['appAll'] . "')";
    $db->query($query2);
    $ride_id = "0";
    $ride_status = "50";
    $message = $_POST['messageAll'];
    if ($_POST['appAll'] == 1) {
        $query = "select * from user where device_id != ''";
        $result = $db->query($query);
        $list = $result->rows;
        foreach ($list as $user) {
            $device_id = $user['device_id'];
            $flag = $user['flag'];
            if ($flag == 1) {
                IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
            } else {
                AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
            }
        }
    } else {
        $query = "select * from driver where device_id != ''";
        $result = $db->query($query);
        $list = $result->rows;
        foreach ($list as $driver) {
            $device_id = $driver['device_id'];
            $flag = $driver['flag'];
            if ($flag == 1) {
                IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            } else {
                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            }
        }
    }
    $db->redirect("home.php?pages=send-notification");
}

//NOTIFICATIONS FOR CITY

if (isset($_POST['sendCity'])) {

    $query2 = "INSERT INTO push_messages (push_message,push_app) VALUES ('" . $_POST['messageCity'] . "',2)";
    $db->query($query2);
    $city = $_POST['city'];
    $message = $_POST['messageCity'];
    $ride_id = "0";
    $ride_status = "50";
    $query = "select * from driver where city_id =" . $city;
    $result = $db->query($query);
    $list1 = $result->rows;
    foreach ($list1 as $driver1) {
        $device_id = $driver1['device_id'];
        $flag = $driver1['flag'];
        if ($flag == 1) {
            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        } else {
            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
    }
    $db->redirect("home.php?pages=send-notification");
}

//NOTIFICATIONS FOR SINGLE

if (isset($_POST['sendSingle'])) {
    $person = $_POST['person'];
    $ride_id = "0";
    $ride_status = "50";
    $message = $_POST['messageSingle'];
    if($_POST['appSingle'] == 2){
        $query = "select * from driver where driver_id =" . $person;
        $result = $db->query($query);
        $list2 = $result->rows;
        foreach ($list2 as $driver2) {
            $device_id = $driver2['device_id'];
            $flag = $driver2['flag'];
            if ($flag == 1) {
                IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            } else {
                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            }
        }
    }
    else {
        $message = $_POST['messageSingle'];
        $query = "select * from user where user_id =" . $person;
        $result = $db->query($query);
        $list2 = $result->rows;
        foreach ($list2 as $user2) {
            $device_id = $user2['device_id'];
            $flag = $user2['flag'];
            if ($flag == 1) {
                IphonePushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
            } else {
                AndroidPushNotificationCustomer($device_id,$message,$ride_id,$ride_status);
            }
        }
    }

}
$City_Qry = "SELECT city_id,city_name FROM city WHERE city_admin_status = 1";
$City_Rslt = $db->query($City_Qry);
$CityList = $City_Rslt->rows;
?>


<script>
    function validate1() {
        var appAll = document.getElementById('appAll').value;
        var messageAll = document.getElementById('messageAll').value;
        if(messageAll == "") { alert("Enter Message");return false; }
        if(appAll == "") { alert("Select Application");return false; }
    }
    function validate2()
    {
        var messageCity = document.getElementById('messageCity').value;
        var city = document.getElementById('city_id').value;
        if(messageCity == "") { alert("Enter Message");return false; }
        if(city == "") { alert("Select City");return false; }
    }
    function validate3()
    {
        var messageSingle = document.getElementById('messageSingle').value;
        var appSingle = document.getElementById('appSingle').value;
        var person = document.getElementById('person').value;
        if(messageSingle == "") { alert("Enter Message");return false; }
        if(appSingle == "") { alert("Select City");return false; }
        if(person == "") { alert("Select Anyone From List");return false; }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Send Push Notification</h3>
        <br>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav-tabs navi navi_tabs">
                <li class="active"><a data-toggle="tab" href="#all"><i class="fa fa-users" aria-hidden="true"></i>
                        <b>All</b></a></li>
                <li class=""><a data-toggle="tab" href="#city"><i class="fa fa-building" aria-hidden="true"></i>
                        <b>City</b></a></li>
                <li class=""><a data-toggle="tab" href="#single"><i class="fa fa-user" aria-hidden="true"></i>
                        <b>Single</b></a>
                </li>
            </ul>
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="tab-content m-0">
                        <div id="all" class="tab-pane active">
                            <div class="form">
                                <form class="cmxform form-horizontal tasi-form" method="post" onSubmit="return validate1()">
                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">Message</label>
                                        <div class="col-lg-6">
                                 <textarea class="form-control" placeholder="Message" name="messageAll" id="messageAll"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">To</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="appAll" id="appAll">
                                                <option value="">--Please Select--</option>
                                                <option value="1">All Customers</option>
                                                <option value="2">All Drivers</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12"
                                                   id="save"
                                                   name="sendAll" value="Send Notification">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- .form -->
                        <!--CITY-->
                        <div id="city" class="tab-pane">
                            <div class="form">
                                <form class="cmxform form-horizontal tasi-form" method="post" onSubmit="return validate2()">
                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">Message</label>
                                        <div class="col-lg-6">
                                   <textarea class="form-control" placeholder="Message" name="messageCity" id="messageCity"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">City</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="city" id="city_id">
                                                <option value="">--Please Select--</option>
                                                <?php
                                                foreach ($CityList as $City) { ?>
                                                    <option value="<?php echo $City['city_id']; ?>"><?php echo $City['city_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12"
                                                   id="save"
                                                   name="sendCity" value="Send Notification">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- .form -->
                        <!--SINGLE-->
                        <div id="single" class="tab-pane">
                            <div class="form">
                                <form class="cmxform form-horizontal tasi-form" method="post" onSubmit="return validate3()">
                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">Message</label>
                                        <div class="col-lg-6">
                                            <textarea class="form-control" placeholder="Message" name="messageSingle" id="messageSingle"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">Type</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="appSingle" id="appSingle" onchange="getId(this.value);">
                                                <option value="">--Please Select--</option>
                                                <option value="1">Customer</option>
                                                <option value="2">Driver</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class="control-label col-lg-2">To</label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="person" id="person">
                                                <option value="">--Please Select--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="sendSingle" value="Send Notification">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->
    <script>
        function getId(val) {
            $.ajax({
                type:"POST",
                url:"getPerson.php",
                data:"Single="+val,
                success:
                    function(data){
                        $('#person').html(data);
                    }
            });
        }
    </script>

</div>
</div>

</section>
</body>
</html>
