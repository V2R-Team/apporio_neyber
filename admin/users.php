<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

    $query="select * from user WHERE user_delete=0 ORDER BY user_id DESC";
	$result = $db->query($query);
	$list=$result->rows;
    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE user SET status='".$_GET['status']."' WHERE user_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=rider");
    }
	
	  if(isset($_POST['savechanges'])) 
     {
       $query2="UPDATE user SET user_name='".$_POST['user_name']."', user_email='".$_POST['user_email']."' , user_phone='".$_POST['user_phone']."'where user_id='".$_POST['savechanges']."'";
       $db->query($query2); 
       $db->redirect("home.php?pages=rider");
     }

     if(isset($_POST['delete']))
     {
       $query2="UPDATE user SET user_delete=1,status=2 where user_id='".$_POST['delete']."'";
       $db->query($query2);
       $db->redirect("home.php?pages=rider");
     }
?>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Rider Management</h3>
       <span class="tp_rht">
       <a href="home.php?pages=add-rider" data-toggle="tooltip" title="Add A Rider" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
      </span>
       </div>
    <div class="row">
         
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>
					  <th width="5%">Sr.No.</th>	
                      <th >Riders Name</th>
                      <th >Email</th>
                      <th width="12%">Mobile</th>
                      <th width="13%">Registration Date</th>
                      <th width="10%">Device</th>
                      <th width="10%">Rating</th>
                      <th>Status</th>       
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
				  
                    <?php $j = 1; foreach($list as $user){?>
                    <tr>
                        <td><a href="#userfulldetails<?php echo $user['user_id'];?>"  data-toggle="modal"><span class="badge bg-warning"><?php echo $j;?></span></a></td>
                      <td>
					  	<?php
                    		$user_name = $user['user_name'];
								echo $user_name;
                    	?>
                      </td>
                      <td>
					  	<?php
                    		$user_email = $user['user_email'];
							if($user_email=="")
							{
								echo "------";
							}
							else
							{
                                $e1=substr($user_email,0,2);
                                $e2= explode("@",$user_email);
                                $domain=$e2[1];
								echo "*****@".$domain;
							}
                    	?>
                      </td>
                       <td>
					  	<?php
                    		$user_phone=$user['user_phone'];
                            $e1=substr($user_phone,7);
                            echo "********".$e1;
                    	?>
                      </td>
                      
                      <td>
                      <?php 
                     $user_register=$user['register_date'];
                     echo  $user_register;
                      
                      ?>                      
                      
                      </td>
                      
                      
                      <td><?php
            	      $device = $user['flag'];
            	      switch ($device){
                          case "1":
                              echo "Iphone";
                              break;
                          case "2":
                              echo "Android";
                              break;
                          default:
                              echo "------";
                      }
            	      ?></td>
            	      
                      <td><?php
                   $userrating = $user['rating'];
                          if($userrating == 0){
                              echo "Not Rate Yet";
                          }else {
                              $wholeStars = floor( $userrating );
                              $halfStar = round( $userrating * 2 ) % 2;
                              $HTML = "";
                              for( $i=0; $i<$wholeStars; $i++ ){
                                  $HTML .= "<img src=star@13.png alt='Whole Star'>";
                              }
                              if( $halfStar ){
                                  $HTML .= "<img src=halfstar.png alt='Half Star'>";
                              }

                              print $HTML;
                          }
   
                      ?></td>
                      <?php
                                if($user['status']==1) {
                                ?>
                      <td class=""> 
                      <label class="label label-success" > Active</label>
                        </td>
                      <?php
                                } else {
                                ?>
                      <td class="text-center"> 
                        <label class="label label-default" > Deactive</label> 
                        </td>
                      <?php } ?>

                        <td>
              <div class="row action_row" style="width:95px;">
                <span data-target="#edit<?php echo $user['user_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                   <?php   if($user['status']==2) { ?>                      
    <a href="home.php?pages=rider&status=1&id=<?php echo $user['user_id']; ?>" data-original-title="Active" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye"> <i class="fa fa-eye"></i> </a>
                                <?php  }else{ ?>                    
     <a href="home.php?pages=rider&status=2&id=<?php echo $user['user_id']; ?>" data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis"> <i class="fa fa-eye-slash"></i> </a>
                             <?php   } ?>                       
      
   <span data-target="#delete<?php echo $user['user_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>                  
</div>        
		</td>       
                  </tr>
                  <?php  $j++; }?>
                    </tbody>
                  
                </table>
              </div>
         
     
    </div>
    <!-- End row --> 
    
  </div>
</form>


<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
foreach($list as $user){?>
    <div class="modal fade" id="userfulldetails<?php echo $user['user_id'];?>" role="dialog"> <?php echo $user['user_id'];?>
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content" style="padding:20px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">User Full Details</h4>
                </div>
                <div class="modal-body" style="max-height: 500px; overflow-x: auto;">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($user['user_image'] != '' && isset($user['user_image'])){echo '../'.$user['user_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                            <td><table style="margin-left:20px;" aling="center" border="0">
                                    <tbody>
                                    <tr>
                                        <th class=""> Name</th>
                                        <td class=""><?php
                                            $user_name = $user['user_name'];
                                            echo $user_name;
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <th class="">Email</th>
                                        <td class=""><?php
                                            $user_email = $user['user_email'];
                                            $e1=substr($user_email,0,2);
                                            $e2= explode("@",$user_email);
                                            $domain=$e2[1];
                                            echo "*****@".$domain;
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <th class="">Phone</th>
                                        <td class=""><?php
                                            $user_phone = $user['user_phone'];
                                            $e1=substr($user_phone,7);
                                            echo "********".$e1;
                                            ?></td>
                                    </tr>
                                    </tbody>
                                </table></td>
                        </tr>
                        <tr>
                            <th class="">Device</th>
                            <td class=""><?php
                                $phonedevice = $user['flag'];
                                switch ($phonedevice){
                                    case "1":
                                        echo "Iphone";
                                        break;
                                    case "2":
                                        echo "Android";
                                        break;
                                    default:
                                        echo "------";
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th class="">Rating</th>
                            <td class=""><?php
                                $rating = $user['rating'];
                                if ($rating == 0)
                                {
                                    echo "Not Rate Yet";
                                }else{
                                    $wholstar = floor($rating);
                                    $haflstar = round($rating * 2) % 2;
                                    $html = "";
                                    for ($i=0;$i<$wholstar;$i++)
                                    {
                                        $html .= "<img src=star@13.png alt='Whole Star'>";
                                    }if($haflstar){
                                        $html .= "<img src=halfstar.png alt='Half Star'>";
                                    }
                                    print $html;
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th class="">Register Date</th>
                            <td class=""><?php
                                $registerdate = $user['register_date'];
                                echo $registerdate;
                                ?></td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
<?php }?>
<!--User Details Closed--> 
<?php foreach($list as $user){?>
<div class="modal fade" id="edit<?php echo $user['user_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Users Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">User Name</label>
              <input type="text" class="form-control"  placeholder="User Name" name="user_name" value="<?php echo $user['user_name'];?>" id="user_name" required>
            </div>
          </div>
          
		  
		  <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">User Email</label>
                <?php
                $e1=substr($user_email,0,2);
                $e2= explode("@",$user_email);
                $domain=$e2[1];
                ?>
              <input type="text" class="form-control"  placeholder="User Email" name="user_email" value="<?php  echo "*****@".$domain;?>" id="user_email" required>
            </div>
          </div>
		  
		  <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">User Phone</label>
                <?php
                $user_phone=$user['user_phone'];
                $e1=substr($user_phone,7);
                ?>
              <input type="text" class="form-control"  placeholder="User Phone" name="user_phone" value="<?php echo "********".$e1;?>" id="user_phone" required>
            </div>
          </div>
		    
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $user['user_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
    </div>
  </div>
</div>
<?php }?>

<?php 
foreach($list as $user){ ?>
    <div class="modal fade" id="delete<?php echo $user['user_id'];?>" role="dialog">
        <div class="modal-dialog">

      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Delete Users </h4>
      </div>
      <div class="modal-body">
        <div class="row">
            	<h3>Do You Really Want To Delete The User?</h3></div>
            	<div class="modal-footer">
        		<button type="submit" name="delete" value="<?php echo $user['user_id'];?>" class="btn btn-danger">Delete</button>
        		<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        	</div>
      	</div>
      </div>
    </div>
  </div>
</div>
<?php } ?>

</section>

</body></html>