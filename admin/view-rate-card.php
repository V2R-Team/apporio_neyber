<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_GET["city"]))
{
	$city=$_GET["city"];
$query="select * from price_card INNER JOIN city ON price_card.city_id=city.city_id INNER JOIN car_type ON price_card.car_type_id=car_type.car_type_id where city.city_id='$city'";	
}
else
{
    $city=3;
$query="select * from price_card INNER JOIN city ON price_card.city_id=city.city_id INNER JOIN car_type ON price_card.car_type_id=car_type.car_type_id where city.city_id='$city'";
}
$result = $db->query($query);
$list=$result->rows;

$que="select * from city";
$res = $db->query($que);
$city_data = $res->rows;

$q="select * from car_type";
$r = $db->query($q);
$list12345 =$r->rows;

// edit booking fee
if(isset($_POST['bosech']))
{
    $query2="UPDATE price_card SET now_booking_fee='".$_POST['now_booking_fee']."',later_booking_fee='".$_POST['later_booking_fee']."' where price_id='".$_POST['bosech']."'";
    $db->query($query2);
	if(isset($_GET["city"]))
	{
		$city=$_GET["city"];
		$db->redirect("home.php?pages=view-rate-card&city=$city");
	}
	else
	{
		$db->redirect("home.php?pages=view-rate-card");
	}
}

// edit booking cancel fee
if(isset($_POST['cabosech']))
{
    $query2="UPDATE price_card SET cancel_ride_now_free_min='".$_POST['cancel_ride_now_free_min']."',cancel_ride_later_free_min='".$_POST['cancel_ride_later_free_min']."',cancel_fee='".$_POST['cancel_fee']."',scheduled_cancel_fee='".$_POST['scheduled_cancel_fee']."' where price_id='".$_POST['cabosech']."'";
    $db->query($query2);
    if(isset($_GET["city"]))
	{
		$city=$_GET["city"];
		$db->redirect("home.php?pages=view-rate-card&city=$city");
	}
	else
	{
		$db->redirect("home.php?pages=view-rate-card");
	}
}

// edit booking cancel fee
if(isset($_POST['ridisech']))
{
    $query2="UPDATE price_card SET base_distance='".$_POST['base_distance']."',base_distance_price='".$_POST['base_distance_price']."',base_price_per_unit='".$_POST['base_price_per_unit']."' where price_id='".$_POST['ridisech']."'";
    $db->query($query2);
    if(isset($_GET["city"]))
	{
		$city=$_GET["city"];
		$db->redirect("home.php?pages=view-rate-card&city=$city");
	}
	else
	{
		$db->redirect("home.php?pages=view-rate-card");
	}
}

// edit wating fee
if(isset($_POST['wasech']))
{
    $query2="UPDATE price_card SET free_waiting_time='".$_POST['free_waiting_time']."',wating_price_minute='".$_POST['wating_price_minute']."' where price_id='".$_POST['wasech']."'";
    $db->query($query2);
    if(isset($_GET["city"]))
	{
		$city=$_GET["city"];
		$db->redirect("home.php?pages=view-rate-card&city=$city");
	}
	else
	{
		$db->redirect("home.php?pages=view-rate-card");
	}
}

// edit ride time service fee
if(isset($_POST['ritisech']))
{
    $query2="UPDATE price_card SET free_ride_minutes='".$_POST['free_ride_minutes']."',price_per_ride_minute='".$_POST['price_per_ride_minute']."' where price_id='".$_POST['ritisech']."'";
    $db->query($query2);
    if(isset($_GET["city"]))
	{
		$city=$_GET["city"];
		$db->redirect("home.php?pages=view-rate-card&city=$city");
	}
	else
	{
		$db->redirect("home.php?pages=view-rate-card");
	}
}
$query="select city_name,city_id from city";
$result=$db->query($query);
$list12=$result->rows;
?>
<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Rate Card</h3>
            <span class="tp_rht">
            <a href="home.php?pages=add-rate-card" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Rate Card"><i class="fa fa-plus"></i></a>
           </span>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <select class="form-control" onchange="location=this.value;">
                            <option>Select Your City</option>
							<?php foreach ($list12 as $data){  ?>
                            <option value="home.php?pages=view-rate-card&city=<?= $data['city_id']; ?>" <?php if($data['city_id'] == $city){ ?> selected <?php } ?>><?= $data['city_name']; ?></option>
                            <?php } ?>
                            
                          </select>
                        </div>
                        <div class="clearfix"></div><br>

<!-- /booking services charges table -->
<!-- ride distance services charges table -->
<div class="col-md-6"  style="margin-bottom:10px;">
	<div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
	<h3 style="margin-top:-5px;">Ride Distance Charges</h3>
		<table  class="table table-striped table-bordered table-responsive alert alert-warning alert-dismissible fade in">
			<thead>
			<tr height="80px" >
				<th width="5%">S.No</th>
				<th>Car Type</th>
				<th width="22%">Base Ride Distance</th>
				<th width="22%">Base Ride Distance Price</th>
				<th width="22%">Price Per Unit After Base Distance</th>
				<th width="5%">Edit</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i =1;
			foreach($list as $ratecard){ ?>
			<tr>
				<td><?= $i; ?></td>
				<td><?= $ratecard['car_type_name']; ?></td>
				<td align="center" ><?= $ratecard['base_distance']; ?></td>
				<td align="center" ><?= $ratecard['base_distance_price']; ?></td>
				<td align="center" ><?= $ratecard['base_price_per_unit']; ?></td>
				<td>
					<span data-target="#ridisech<?php echo $ratecard['price_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
				</td>
				
				
			</tr>
			<?php $i++;
			}
			?>
			</tbody>
		</table>
	</div>    
</div>
<!-- /Ride distance services charges table -->
   
<!-- Ride distance services charges model -->							
<?php foreach($list as $ratecard){?>
    <div class="modal fade" id="ridisech<?php echo $ratecard['price_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Ride Distance Services Charges</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">City</label>
                                    <select class="form-control" name="city_id" id="city_id" required>
                                        <?php
                                        $city_id = $ratecard['city_id'];
                                        $city_name = $ratecard['city_name'];
                                        ?>
                                        <option value="<?php echo $city_id;?>"><?php echo $city_name; ?></option>
                                        <?php foreach($city_data as $data) :?>
                                            <option value="<?php echo $data['city_id'];?>"><?php echo $data['city_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Car Type</label>
                                    <select class="form-control" name="car_type_id" id="car_type_id"   required>

                                        <?php
                                        $car_type_id = $ratecard['car_type_id'];
                                        $car_type_name = $ratecard['car_type_name'];
                                        ?>

                                        <option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
                                        <?php foreach($list12345 as $car) :?>
                                            <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Base Ride Distance (Miles/KM)</label>
                                    <input type="text" class="form-control"  placeholder="Base Ride Distance (Miles/KM)" name="base_distance" value="<?php echo $ratecard['base_distance'];?>" id="base_distance" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Base Ride Distance Charges</label>
                                    <input type="text" class="form-control"  placeholder="Base Ride Distance Charges" name="base_distance_price" value="<?php echo $ratecard['base_distance_price'];?>" id="base_distance_price" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price Ride Per Unit</label>
                                    <input type="text" class="form-control"  placeholder="Base Price Miles" name="base_price_per_unit" value="<?php echo $ratecard['base_price_per_unit'];?>" id="base_price_per_unit" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="ridisech" value="<?php echo $ratecard['price_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<!-- / ride distance services charges model -->

<!-- wating services charges Table -->                         
<div class="col-md-6"  style="margin-bottom:12px;">
	<div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
	<h3 style="margin-top:-5px;">Wating Service Charges</h3>
		<table  class="table table-striped table-bordered table-responsive alert alert-warning alert-dismissible fade in">
			<thead>
			<tr height="80px">
				<th width="5%">S.No</th>
				<th>Car Type</th>
				<th width="28%">Free Waiting Time</th>
				<th width="28%">Price Per Minutes After Free Waiting Time</th>
				<th width="5%">Edit</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i =1;
			foreach($list as $ratecard){ ?>
			<tr>
				<td><?= $i; ?></td>
				<td><?= $ratecard['car_type_name']; ?></td>
				<td align="center" ><?= $ratecard['free_waiting_time']; ?></td>
				<td align="center" ><?= $ratecard['wating_price_minute']; ?></td>
				<td>
					<span data-target="#wasech<?php echo $ratecard['price_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
				</td>
			</tr>
			<?php $i++;
			}
			?>
			</tbody>
		</table>
	</div>    
</div>
<!-- /wating services charges Table -->  
<!-- Ride distance services charges model -->							
<?php foreach($list as $ratecard){?>
    <div class="modal fade" id="wasech<?php echo $ratecard['price_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Distance Service Charges</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">City</label>
                                    <select class="form-control" name="city_id" id="city_id" required>
                                        <?php
                                        $city_id = $ratecard['city_id'];
                                        $city_name = $ratecard['city_name'];
                                        ?>
                                        <option value="<?php echo $city_id;?>"><?php echo $city_name; ?></option>
                                        <?php foreach($city_data as $data) :?>
                                            <option value="<?php echo $data['city_id'];?>"><?php echo $data['city_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Car Type</label>
                                    <select class="form-control" name="car_type_id" id="car_type_id"   required>

                                        <?php
                                        $car_type_id = $ratecard['car_type_id'];
                                        $car_type_name = $ratecard['car_type_name'];
                                        ?>

                                        <option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
                                        <?php foreach($list12345 as $car) :?>
                                            <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Free waiting Time</label>
                                    <input type="text" class="form-control"  placeholder="Free waiting Time" name="free_waiting_time" value="<?php echo $ratecard['free_waiting_time'];?>" id="free_waiting_time" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price Per Minute After Free Wait Time</label>
                                    <input type="text" class="form-control"  placeholder="Price Per Minute After Free Wait Time" name="wating_price_minute" value="<?php echo $ratecard['wating_price_minute'];?>" id="wating_price_minute" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="wasech" value="<?php echo $ratecard['price_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<!-- / wating services charges model -->
 
<!-- Ride Time services charges Table --> 
<div class="col-md-6"> 
	<div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
	<h3 style="margin-top:-5px;">Ride Time Service Charges</h3>
		<table  class="table table-striped table-bordered table-responsive alert alert-warning alert-dismissible fade in">
			<thead>
			<tr height="80px">
				<th width="5%">S.No</th>
				<th>Car Type</th>
				<th width="28%">Free Riding Minutes</th>
				<th width="28%">Price Per Minutes After Free Ride Minutes</th>
				<th width="5%">Edit</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i =1;
			foreach($list as $ratecard){ ?>
			<tr>
				<td><?= $i; ?></td>
				<td><?= $ratecard['car_type_name']; ?></td>
				<td align="center" ><?= $ratecard['free_ride_minutes']; ?></td>
				<td align="center" ><?= $ratecard['price_per_ride_minute']; ?></td>
				<td>
					<span data-target="#ritisech<?php echo $ratecard['price_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
				</td>
			</tr>
			<?php $i++;
			}
			?>
			</tbody>
		</table>
	</div>    
</div>
<!-- /Ride Time services charges Table -->
<!-- Ride time services charges model -->							
<?php foreach($list as $ratecard){?>
    <div class="modal fade" id="ritisech<?php echo $ratecard['price_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Time Service Charges </h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">City</label>
                                    <select class="form-control" name="city_id" id="city_id" required>
                                        <?php
                                        $city_id = $ratecard['city_id'];
                                        $city_name = $ratecard['city_name'];
                                        ?>
                                        <option value="<?php echo $city_id;?>"><?php echo $city_name; ?></option>
                                        <?php foreach($city_data as $data) :?>
                                            <option value="<?php echo $data['city_id'];?>"><?php echo $data['city_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Car Type</label>
                                    <select class="form-control" name="car_type_id" id="car_type_id"   required>

                                        <?php
                                        $car_type_id = $ratecard['car_type_id'];
                                        $car_type_name = $ratecard['car_type_name'];
                                        ?>

                                        <option value="<?php echo $car_type_id;?>"><?php echo $car_type_name; ?></option>
                                        <?php foreach($list12345 as $car) :?>
                                            <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Free Ride Minute</label>
                                    <input type="text" class="form-control"  placeholder="Free Ride Minute" name="free_ride_minutes" value="<?php echo $ratecard['free_ride_minutes'];?>" id="free_ride_minutes" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Price Per Ride Minute</label>
                                    <input type="text" class="form-control"  placeholder="Price Per Ride Minute" name="price_per_ride_minute" value="<?php echo $ratecard['price_per_ride_minute'];?>" id="price_per_minute_waiting" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="ritisech" value="<?php echo $ratecard['price_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<!-- / ride time charges model -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>

</form>


<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>