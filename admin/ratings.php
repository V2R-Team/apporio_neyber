<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');


$query = "select * from rating_customer INNER JOIN rating_driver ON rating_customer.ride_id=rating_driver.ride_id";
$result = $db->query($query);
$list = $result->rows;
print_r($list);die();
?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Vehicle Type</h3>
            <span>
            <a href="home.php?pages=add-car-type" class="btn btn-default btn-lg" id="add-button" title="Add A Car Type" role="button">Add Vehicle Type</a>
      </span>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th width="50%">Vehicle Name</th>
                                        <th width="50%">Vehicle Name French </th>
                                        <th>Vehicle Image</th>
                                        <th width="12%">Status</th>
                                        <th width="4%">Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $cartype){?>
                                        <tr>

                                            <td><?php echo $cartype['car_type_id'];?></td>

                                            <td>
                                                <?php
                                                $car_type_name  = $cartype['car_type_name'];
                                                echo $car_type_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $car_type_name_french=$cartype['car_type_name_french'];
                                                if($car_type_name_french=="")
                                                {
                                                    echo "---------";
                                                }
                                                else
                                                {
                                                    echo $car_type_name_french;
                                                }
                                                ?>
                                            </td>


                                            <td><img src="../<?php echo $cartype['car_type_image'];?>"  width="80px" height="80px"></td>


                                            <?php
                                            if($cartype['car_admin_status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view-car-type&status=2&id=<?php echo $cartype['car_type_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view-car-type&status=1&id=<?php echo $cartype['car_type_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $cartype['car_type_id']?>"  ></button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
</section>
</body></html>