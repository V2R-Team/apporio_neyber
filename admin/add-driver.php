<?php
session_start();
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query = "select * from city WHERE city_admin_status=1";
$result = $db->query($query);
$city_list = $result->rows;

$querycompany = "select * from company";
$resultcompany = $db->query($querycompany);
$company_list = $resultcompany->rows;



$query = "select * from car_type WHERE car_admin_status=1";
$result = $db->query($query);
$car_list = $result->rows;
if(isset($_POST['save']))
{
    $query = "select * from driver WHERE driver_email='".$_POST['driver_email']."'";
    $result = $db->query($query);
    $list = $result->row;
    if(count($list) == 0)
    {
        $query = "select * from driver WHERE driver_phone='".$_POST['driver_phone']."'";
        $result = $db->query($query);
        $list = $result->row;
        if(count($list) == 0)
        {
            $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
            $data=$dt->format('M j');
            $day=date("l");
            $date=$day.", ".$data ;
            $query = "INSERT INTO driver (company_id,commission,driver_name,driver_email,driver_phone,driver_password,car_type_id,car_model_id,car_number,city_id,register_date,license_expire,rc_expire,insurance_expire,login_logout,detail_status,driver_admin_status)
 VALUES ('".$_POST['company_id']."','".$_POST['commission']."','".$_POST['driver_name']."','".$_POST['driver_email']."','".$_POST['driver_phone']."','".$_POST['driver_password']."','".$_POST['car_type_id']."','".$_POST['car_model_id']."','".$_POST['car_number']."','".$_POST['city_id']."','$date','".$_POST['licenseExpire']."','".$_POST['RCExpire']."','".$_POST['insuranceExpire']."','2','2','1')";
            $db->query($query);
            $driver_id = $db->getLastId();
            if(!empty($_FILES['rc'])) {
                $img_name = $_FILES['rc']['name'];
                $filedir = "../uploads/driver/";
                if (!is_dir($filedir)) mkdir($filedir, 0755, true);
                {
                    $fileext = strtolower(substr($_FILES['rc']['name'], -4));
                    if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                        if ($fileext == "jpeg") {
                            $fileext = ".jpg";
                        }
                        $pfilename = time() . "rc_".$driver_id.$fileext;
                        $filepath1 = "uploads/driver/".$pfilename;
                        $filepath = $filedir.$pfilename;
                        copy($_FILES['rc']['tmp_name'], $filepath);
                        $query1 = "UPDATE driver SET rc='$filepath1' WHERE driver_id='$driver_id'";
                        $db->query($query1);
                    }
                }
            }
            if(!empty($_FILES['license'])) {
                $img_name = $_FILES['license']['name'];
                $filedir = "../uploads/driver/";
                if (!is_dir($filedir)) mkdir($filedir, 0755, true);
                {
                    $fileext = strtolower(substr($_FILES['license']['name'], -4));
                    if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                        if ($fileext == "jpeg") {
                            $fileext = ".jpg";
                        }
                        $pfilename = time() . "license_".$driver_id.$fileext;
                        $filepath1 = "uploads/driver/".$pfilename;
                        $filepath = $filedir.$pfilename;
                        copy($_FILES['license']['tmp_name'], $filepath);
                        $query1 = "UPDATE driver SET license='$filepath1' WHERE driver_id='$driver_id'";
                        $db->query($query1);
                    }
                }
            }

            if(!empty($_FILES['insurance'])) {
                $img_name = $_FILES['insurance']['name'];
                $filedir = "../uploads/driver/";
                if (!is_dir($filedir)) mkdir($filedir, 0755, true);
                {
                    $fileext = strtolower(substr($_FILES['insurance']['name'], -4));
                    if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                        if ($fileext == "jpeg") {
                            $fileext = ".jpg";
                        }
                        $pfilename = time() . "insurance_".$driver_id.$fileext;
                        $filepath1 = "uploads/driver/".$pfilename;
                        $filepath = $filedir.$pfilename;
                        copy($_FILES['insurance']['tmp_name'], $filepath);
                        $query1 = "UPDATE driver SET insurance='$filepath1' WHERE driver_id='$driver_id'";
                        $db->query($query1);

                    }
                }
            }

                  $msg = "Driver Details Added";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
            $db->redirect("home.php?pages=drivers");
        }else{
                   $msg = "Phone Number Already Registerd";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
            $db->redirect("home.php?pages=add-driver");
        }
    }else{
                 $msg = "Email Already Registerd";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
            $db->redirect("home.php?pages=add-driver");
    }

}

?>

<script>
    function validatelogin() {
        var driver_name = document.getElementById('driver_name').value;
        var driver_email = document.getElementById('driver_email').value;
        var driver_phone = document.getElementById('driver_phone').value;
        var driver_password = document.getElementById('driver_password').value;
        var driver_confirm_password = document.getElementById('driver_confirm_password').value;
        var city_id = document.getElementById('city_id').value;
        var car_type_id = document.getElementById('car_type_id').value;
        var car_number = document.getElementById('car_number').value;
        var license = document.getElementById('license').value;
        var rc = document.getElementById('rc').value;
        var insurance = document.getElementById('insurance').value;
        if(driver_name == "")
        {
            alert("Enter Driver Name");
            return false;
        }
        if(driver_email == "")
        {
            alert("Enter Driver Email");
            return false;
        }
        if(driver_phone == "")
        {
            alert("Enter Driver Phone");
            return false;
        }
        if(driver_password == "")
        {
            alert("Enter Driver Default Password");
            return false;
        }
        if(driver_confirm_password == "")
        {
            alert("Enter Confirm Password");
            return false;
        }
        if(driver_password != driver_confirm_password)
        {
            alert("Password And Confirm Password D'not Match");
            return false;
        }
        if(city_id == "")
        {
            alert("Select Driver City");
            return false;
        }
        if(car_type_id == "")
        {
            alert("Select Driver Car Type");
            return false;
        }
        if(car_number == "")
        {
            alert("Enter Car Number");
            return false;
        }
        if(license == "")
        {
            alert("Upload Driver License");
            return false;
        }
        if(rc == "")
        {
            alert("Upload Car Registration Certificate");
            return false;
        }
        if(insurance == "")
        {
            alert("Upload Car Insurance");
            return false;
        }

    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Driver</h3>
        <span class="tp_rht">
           <a href="home.php?pages=drivers" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
       </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Driver Name" name="driver_name" id="driver_name"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Driver Email" name="driver_email" id="driver_email"/>
                                </div>
                            </div>


                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Driver Phone" name="driver_phone" id="driver_phone"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" placeholder="Driver Default Password" name="driver_password" id="driver_password"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" placeholder="Confirm Password" name="driver_confirm_password" id="driver_confirm_password"/>
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Company</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="company_id" id="company_id">
                                        <option value="">--Select Company Of Driver--</option>
                                        <?php foreach($company_list as $company){ ?>
                                            <option value="<?php echo $company['company_id'];?>"><?php echo $company['company_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">City</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="city_id" id="city_id">
                                        <option value="">--Select City Of Driver--</option>
                                        <?php foreach($city_list as $city){ ?>
                                            <option value="<?php echo $city['city_id'];?>"><?php echo $city['city_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Car Type</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="car_type_id" id="car_type_id" onchange="getId(this.value);">
                                        <option value="">--Select Car Type Of Driver--</option>
                                        <?php foreach($car_list as $car){ ?>
                                            <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Car Model</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="car_model_id" id="car_model_id">
                                        <option value="">--Select Car Type First--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Commission</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Driver Commission" name="commission" id="commission"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Car Number</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Car Number" name="car_number" id="car_number"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Driver License</label>
                                <div class="col-lg-4">
                                    <input type="file" class="form-control" placeholder="Driver License" name="license" id="license"/>
                                </div>
                                <label class="control-label col-lg-1">Expiry Date </label>
                                <div class="col-lg-5">
                                    <input type="date" class="form-control" placeholder="" name="licenseExpire" id="licenseExp"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Registration Certificate</label>
                                <div class="col-lg-4">
                                    <input type="file" class="form-control" placeholder="Car Registration Certificate" name="rc" id="rc"/>
                                </div>
                                <label class="control-label col-lg-1">Expiry Date </label>
                                <div class="col-lg-5">
                                    <input type="date" class="form-control" placeholder="" name="RCExpire" id="RCExpire"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">insurance</label>
                                <div class="col-lg-4">
                                    <input type="file" class="form-control" placeholder="Car Insurance" name="insurance" id="insurance"/>
                                </div>
                                <label class="control-label col-lg-1">Expiry Date </label>
                                <div class="col-lg-5">
                                    <input type="date" class="form-control" placeholder="" name="insuranceExpire" id="insuranceExpire"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    function getId(val) {
        $.ajax({
            type:"POST",
            url:"car_model.php",
            data:"car_type_id="+val,
            success:
                function(data){
                    $('#car_model_id').html(data);
                }
        });
    }
</script>