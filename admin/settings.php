<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from admin where admin_id=1";
$result = $db->query($query);
$list=$result->row;
$query = "select * from country";
$result = $db->query($query);
$list = $result->rows;
$query="select * from configuration where configuration_id=1";
$result = $db->query($query);
$configuration = $result->row;
if(isset($_POST['General']))
{
    $query2="UPDATE configuration SET country_id='".$_POST['country_id']."',project_name='".$_POST['project_name']."',rider_phone_verification='".$_POST['rider_phone_verification']."',rider_email_verification='".$_POST['rider_email_verification']."',driver_phone_verification='".$_POST['driver_phone_verification']."',driver_email_verification='".$_POST['driver_email_verification']."' WHERE configuration_id=1";
    $db->query($query2);
    $db->redirect("home.php?pages=settings");
}

if(isset($_POST['email']))
{
    $query2="UPDATE configuration SET email_name='".$_POST['email_name']."',email_header_name='".$_POST['email_header_name']."',email_footer_name='".$_POST['email_footer_name']."',reply_email='".$_POST['reply_email']."',admin_email='".$_POST['admin_email']."' WHERE configuration_id=1";
    $db->query($query2);
    $db->redirect("home.php?pages=settings");
}

if(isset($_POST['social']))
{
    $query2="UPDATE configuration SET admin_footer='".$_POST['admin_footer']."', support_number='".$_POST['support_number']."', company_name='".$_POST['company_name']."', company_address='".$_POST['company_address']."' WHERE configuration_id=1";
    $db->query($query2);
    $db->redirect("home.php?pages=settings");
}





?>

<style>.clear{clear:both !important;}</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Configuration</h3>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-12">
            <div class="panel panel-default p-0">
                <div class="panel-body p-0">
                    <ul class="nav nav-tabs profile-tabs">
                        <li class="active"><a data-toggle="tab" href="#personal">General</a></li>
                        <li class=""><a data-toggle="tab" href="#address">Email</a></li>
                        <li class=""><a data-toggle="tab" href="#social">Apperance</a></li>
                        <li class=""><a data-toggle="tab" href="#sms">SMS</a></li>
                        <li class=""><a data-toggle="tab" href="#payment">Payment</a></li>
                        <li class=""><a data-toggle="tab" href="#media">Social Media</a></li>
                        <li class=""><a data-toggle="tab" href="#app">App Settings</a></li>
                    </ul>
                    <div class="tab-content m-0">

                        <!-- settings -->
                        <div id="personal" class="tab-pane active">
                            <div class="user-profile-content">
                                <form role="form" method="post" onSubmit="return validatelogin()">
                                    <div class="form-group col-md-6">
                                        <label>Default Country Code </label>
                                        <select class="form-control" name="country_id" id="country_id">
                                            <option value="">---Select Country--</option>
                                            <?php foreach($list as $item){ ?>
                                                <option value="<?php echo $item['id'];?>" <?php if($item['id'] == $configuration['country_id']){ ?> selected <?php } ?>><?php echo $item['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Project Name</label>
                                        <input type="text" class="form-control" placeholder="Project Name" value="<?php echo $configuration['project_name'];?>" name="project_name" id="project_name">

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Enable Rider Phone Verification</label>
                                        <select class="form-control" name="rider_phone_verification" id="rider_phone_verification">
                                            <option value="1" <?php if($configuration['rider_phone_verification'] == 1){ ?> selected <?php } ?>>Yes</option>
                                            <option value="2" <?php if($configuration['rider_phone_verification'] == 2){ ?> selected <?php } ?>>No</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Enable Rider Email Verification</label>
                                        <select class="form-control" name="rider_email_verification" id="rider_email_verification">
                                            <option value="1" <?php if($configuration['rider_email_verification'] == 1){ ?> selected <?php } ?>>Yes</option>
                                            <option value="2" <?php if($configuration['rider_email_verification'] == 2){ ?> selected <?php } ?>>No</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Enable Driver Phone Verification</label>
                                        <select class="form-control" name="driver_phone_verification" id="driver_phone_verification">
                                            <option value="1" <?php if($configuration['driver_phone_verification'] == 1){ ?> selected <?php } ?>>Yes</option>
                                            <option value="2" <?php if($configuration['driver_phone_verification'] == 2){ ?> selected <?php } ?>>No</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Enable Driver Email Verification</label>
                                        <select class="form-control" name="driver_email_verification" id="driver_email_verification">
                                            <option value="1" <?php if($configuration['driver_email_verification'] == 1){ ?> selected <?php } ?>>Yes</option>
                                            <option value="2" <?php if($configuration['driver_email_verification'] == 2){ ?> selected <?php } ?>>No</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="General" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                   </form>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div id="address" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form" method="post" onSubmit="return validatelogin()">


                                    <div class="form-group col-md-6">
                                        <label>Email From Name</label>
                                        <input type="text" class="form-control" placeholder="Email From Name " value="<?php echo $configuration['email_name'];?>" name="email_name" id="email_name">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Mail Header</label>
                                        <input type="text" class="form-control" placeholder="Mail Header" value="<?php echo $configuration['email_header_name'];?>" name="email_header_name" id="email_header_name">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Mail Footer</label>
                                        <input type="text" class="form-control" placeholder="Mail Footer" value="<?php echo $configuration['email_footer_name'];?>" name="email_footer_name" id="email_footer_name">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>No Reply Email </label>
                                        <input type="text" class="form-control" placeholder="No Reply Email " value="<?php echo $configuration['reply_email'];?>" name="reply_email" id="reply_email">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Admin Email </label>
                                        <input type="email" class="form-control" placeholder="Admin Email" value="<?php echo $list['admin_email'];?>" name="admin_email" id="admin_email">
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="email" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div id="social" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form"  method="post" onSubmit="return validatelogin()">


                                    <div class="form-group col-md-6">
                                        <label>Copyright text to show in footer</label>
                                        <input type="text" class="form-control" placeholder="Copyright text to show in footer" value="<?php echo $configuration['admin_footer'];?>" name="admin_footer" id="admin_footer">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Support Phone</label>
                                        <input type="text" class="form-control" placeholder="Support Phone" value="<?php echo $configuration['support_number'];?>" name="support_number" id="support_number">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Company Name </label>
                                        <input type="text" class="form-control" placeholder="Company Name" value="<?php echo $configuration['company_name'];?>" name="company_name" id="company_name">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Company Address</label>
                                        <input type="text" class="form-control" placeholder="Company Address" value="<?php echo $configuration['company_address'];?>" name="company_address" id="company_address">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="social" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="sms" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form"  method="post" onSubmit="return validatelogin()">


                                    <div class="form-group col-md-6">
                                        <label>Twilio account SID </label>
                                        <input type="text" class="form-control" placeholder="AC32b01bb7ff56e7e08be1a019361f7722" value="" name="account_sid" id="">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Twilio Mobile Number (with plus sign as prefix)</label>
                                        <input type="number" class="form-control" placeholder="+19876543210" value="" name="mobile_number" id="">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Twilio account token  </label>
                                        <input type="text" class="form-control" placeholder="c328da6286cbdb96938c37311c5c4b94" value="" name="account_token" id=" ">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="sms-btn" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="payment" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form"  method="post" onSubmit="return validatelogin()">


                                    <div class="form-group col-md-6">
                                        <label>Enable Credit Card payment ("Yes" OR "No") </label>
                                        <input type="text" class="form-control" placeholder="Yes" value="" name="card_payment" id="">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Stripe Secret Key</label>
                                        <input type="number" class="form-control" placeholder="+sk_test_xGFa5eB2dkX29R8DIaHwCdLb" value="" name="ss_key" id="">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Stripe Publisher Key   </label>
                                        <input type="text" class="form-control" placeholder="pk_test_TJeNhmUQgsrhU0mGDztG9q6r" value="" name="sp_key" id=" ">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="payment-btn" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div id="media" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form"  method="post" onSubmit="return validatelogin()">

                                    <div class="form-group col-md-6">
                                         <label>Facebook AppID</label>
                                        <input type="text" class="form-control" placeholder="1251400154950504" value="" name="fb_appid" id="">

                                        <label> Facebook app secret key  </label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="fb_secretkey" id=" ">

                                        <label>Footer Instagram Link</label>
                                        <input type="number" class="form-control" placeholder="#" value="" name="footer_insta" id="">

                                         <label> Footer FaceBook Link  </label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="footer_fb" id=" ">

                                           </div>
                                    <div class="form-group col-md-6">
                                         <label>Footer Twitter Link</label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="footer_twitter" id=" ">

                                         <label>Footer LinkedIn Link</label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="footer_linkedin" id=" ">

                                        <label>Facebook Iframe</label>
                                        <textarea type="text" class="form-control" placeholder="" value="" name="fb_iframe" id=" "></textarea>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="payment-btn" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                        <div id="app" class="tab-pane">
                            <div class="user-profile-content">
                                <form role="form"  method="post" onSubmit="return validatelogin()">


                                    <div class="form-group col-md-6">
                                        <label>Kilometer radius around passenger to show online drivers</label>
                                        <input type="text" class="form-control" placeholder="5" value="" name="radius" id="">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Driver's cab request method</label>
                                        <select type="number" class="form-control"  value="" name="req_method" id="">
                                            <option selected>Distance</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Google Project Id</label>
                                        <input type="text" class="form-control" placeholder="359668703610" value="" name="google_projectID" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Police control room number of Site's country.</label>
                                        <input type="text" class="form-control" placeholder="100" value="" name="PCR" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Google Server API Key for push notification</label>
                                        <input type="text" class="form-control" placeholder="AIzaSyDzf6y7pgQDdGHuGeXDdCY_dsiN6riNaeU" value="" name="google_notificationAPIkey id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> PASS PHRASE for both PEM file: should be same for both (Passenger and driver pem) file</label>
                                        <input type="text" class="form-control" placeholder="123456" value="" name="pass_phrase" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> PubNub Publish Key </label>
                                        <input type="text" class="form-control" placeholder="pub-c-60b4348c-10b0-4568-a34d-66a93628a654" value="" name="pubnub_publish_key" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Online driver List limit by distance (Integer Value)  </label>
                                        <input type="text" class="form-control" placeholder="40" value="" name="pubnub_subscribe_key" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label> Android App Link</label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="android_applink" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>PubNub secret Key</label>
                                        <input type="text" class="form-control" placeholder="sec-c-ZWE3NzA2MzAtNGVjNi00Y2M4LWI4YzUtMTQxNTExMjM0NGZk" value="" name="pubnub_secret_key" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Iphone App Link</label>
                                        <input type="text" class="form-control" placeholder="#" value="" name="iphone_applink" id=" ">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Google Server API Key for Web panel</label>
                                        <input type="text" class="form-control" placeholder="AIzaSyDwRzsrkm8sZUUSp5pCYzZw-_xbtrn2KEY" value="" name="google_webpanelAPIkey" id=" ">
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group" style="text-align: center;">
                                                <button class="btn btn-default" name="payment-btn" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="clear"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>