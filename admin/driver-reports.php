<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from car_type";
$result = $db->query($query);
$list=$result->rows;
foreach ($list as $key=>$login)
{
    $car_type_id = $login['car_type_id'];
    $query = "select * from driver WHERE car_type_id='$car_type_id'";
    $result = $db->query($query);
    $driver_list=$result->rows;
    $total_driver = count($driver_list);
    $list[$key]=$login;
    $list[$key]["total_driver"]=$total_driver;
}
$query="select * from driver ORDER BY rating DESC LIMIT 10";
$result = $db->query($query);
$driver=$result->rows;
$query="select * from driver ORDER BY total_payment_eraned DESC LIMIT 10";
$result = $db->query($query);
$driver_amount = $result->rows;
?>
<div class="wraper container-fluid" >

    <div class="row col-md-12">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Driver Signup Comparison Month Wise</h3>
                </div>
                <div class="panel-body">
                    <div id="columnchart_values"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Driver Car Type Comparison</h3>
                </div>
                <div class="panel-body">
                    <div id="piechart" style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Top Driver By Ratings</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div"  style="width: 500px; height: 400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Top Driver By Revenue</h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div1"  style="width: 500px; height: 400px;"></div>
                </div>
            </div>
        </div>
    </div>


</div>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Car Type', 'Driver Per Car Type'],
            <?php foreach ($list as $data){?>
            ['<?php echo $data['car_type_name']?>', <?php echo $data['total_driver']?>],
            <?php } ?>
        ]);

        var options = {
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data,options);
    }

</script>
<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ["Month", "User Per Month", { role: "style" } ],
            ["January", 10, "#3F33FF"],
            ["February", 100, "#3F33FF"],
            ["March", 10, "#3F33FF"],
            ["May", 200, "color: #3F33FF"],
            ["June", 50, "color: #3F33FF"],
            ["July", 125, "color: #3F33FF"],
            ["August", 70, "color: #3F33FF"],
            ["September", 75, "color: #3F33FF"],
            ["October", 80, "color: #3F33FF"],
            ["November", 90, "color: #3F33FF"],
            ["December", 60, "color: #3F33FF"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            width: 600,
            height: 400,
            bar: {groupWidth: "50%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view, options);
    }
</script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Drivers', 'Driver Rating Points',{ role: 'style' }],
            <?php foreach ($driver as $value){?>
            ['<?php echo $value['driver_name']?>', <?php echo $value['rating']?>,'silver'],
            <?php } ?>
        ]);

        var options = {
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Total Rating',
                minValue: 0
            },
            vAxis: {
                title: 'Drivers'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data,options);
    }
</script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Drivers', 'Driver Revenue Money',{ role: 'style' }],
            <?php foreach ($driver_amount as $value){?>
            ['<?php echo $value['driver_name']?>', <?php echo $value['total_payment_eraned']?>,'#520D0D'],
            <?php } ?>
        ]);

        var options = {
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Total Rating',
                minValue: 0
            },
            vAxis: {
                title: 'Drivers'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div1'));
        chart.draw(data,options);
    }
</script>
</section>
</body></html>
