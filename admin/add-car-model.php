<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

    $query="select * from car_type";
	$result = $db->query($query);
	$list=$result->rows;
        
if(isset($_POST['save'])) {
    $query1="INSERT INTO car_model (car_year,car_color,car_model_name,car_model_name_french,car_type_id,car_make,car_model) VALUES('".$_POST['car_year']."','".$_POST['car_color']."','".$_POST['car_model_name']."','".$_POST['car_model_french']."','".$_POST['car_type_id']."','".$_POST['car_make']."',,'".$_POST['car_model']."')";
    $db->query($query1);
    $car_model_id = $db->getLastId();
    if(!empty($_FILES['car_image']))
    {
        $img_name = $_FILES['car_image']['name'];
        $filedir  = "../uploads/car/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['car_image']['name'],-4));
        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "car_".$car_model_id.$fileext;
            $filepath1 = "uploads/car/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['car_image']['tmp_name'], $filepath);

            $upd_qry = "UPDATE car_model SET car_model_image ='$filepath1' where car_model_image ='$car_model_id'";
            $db->query($upd_qry);
        }
    }
    $msg = "Vehicle Model Details Save Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
     $db->redirect("home.php?pages=add-car-model");
}
?>

<script>
    function validatelogin() {
        var car_type_id = document.getElementById('car_type_id').value;
        var car_model_name = document.getElementById('car_model_name').value;
        var car_make = document.getElementById('car_make').value;
        var car_model = document.getElementById('car_model').value;
        var car_year = document.getElementById('car_year').value;
        var car_color = document.getElementById('car_color').value;
        var car_image = document.getElementById('car_image').value;

        if(car_type_id == "")
        {
            alert("Select Vehicle Type");
            return false;
        }
        if(car_model_name == "")
        {
            alert("Enter Vehicle Model Name");
            return false;
        }
        if(car_make == "")
        {
            alert("Enter Make");
            return false;
        }
        if(car_model == "")
        {
            alert("Enter Model");
            return false;
        }
        if(car_year == "")
        {
            alert("Enter Year");
            return false;
        }
        if (car_color == "")
        {
            alert("Enter Color");
            return false;
        }
        if(car_image == "")
        {
            alert("Upload Car Image");
            return false;
        }

    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Vehicle Model</h3>
        <span>
            <a href="home.php?pages=view-car-model" class="btn btn-default btn-lg" id="add-button" title="Back to Listing" role="button">Back to Listing</a>
      </span>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  enctype="multipart/form-data" onSubmit="return validatelogin()">

                <div class="form-group ">
                  <label class="control-label col-lg-2">Select Vehicle Type*</label>
                  <div class="col-lg-6">
                     <select class="form-control" name="car_type_id" id="car_type_id" >
                        <option value="">--Please Select Vehicle Type--</option>
                          <?php foreach($list as $cartype){ ?>
                           <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>
                <?php } ?>
                    </select>
                  </div>
                </div>            

                <div class="form-group ">
                  <label class="control-label col-lg-2">Name*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Car Model Name" name="car_model_name" id="car_model_name" >
                  </div>
                </div>
                
                
               <div class="form-group ">
                  <label class="control-label col-lg-2">Name In French*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Car Model Name In French" name="car_model_french" id="" >
                  </div>
                </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Make*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Make" name="car_make" id="car_make" >
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Model*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Model" name="car_model" id="car_model" >
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Year*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Year" name="car_year" id="car_year" >
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Color*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Color" name="car_color" id="car_color" >
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Image*</label>
                      <div class="col-lg-6">
                          <input type="file" class="form-control" placeholder="Image" name="car_image" id="car_image" >
                      </div>
                  </div>



                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
