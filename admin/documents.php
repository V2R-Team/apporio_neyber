<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');

$query1="select * from table_documents WHERE document_type='1';";
$result1 = $db->query($query1);
$list1=$result1->rows;

$query2="select * from table_documents WHERE document_type='2';";
$result2 = $db->query($query2);
$list2=$result2->rows;



?>

<style>.clear{clear:both !important;}</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Documents List</h3>
        <span class="tp_rht">
                <a href="home.php?pages=add-document" data-toggle="tooltip" title="Add Documents" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
                <!--<a href="home.php?pages=add-document" class="btn btn-default btn-lg" id="add-button"  role="button">Add Document</a>-->


      </span>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">FOR DRIVER</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($list1 as $driver){?>
                        <li class="list-group-item"><?php echo '<div class="driver_document">'. "  ".$driver['document_name']."</div>";?>
                        </li>
                        <?php }
                        ?>
                    </ul>


                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">FOR VEHICLE</h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($list2 as $vehicle){?>
                            <li class="list-group-item"><?php echo '<div class="driver_document">'. "  ".$vehicle['document_name']."</div>";?>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
    </div>
</div>
</div>
</section>
<!-- Main Content Ends -->
<script>
$('option').mousedown(function(e) {
e.preventDefault();
$(this).prop('selected', !$(this).prop('selected'));
return false;
});
</script>
</body></html>