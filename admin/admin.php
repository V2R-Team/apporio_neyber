<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
    $query="select * from admin";
	$result = $db->query($query);
	$list=$result->rows;       


?>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Admin</h3>
        <span class="tp_rht">
       <a href="home.php?pages=add-admin" data-toggle="tooltip" title="Add A Admin" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
        </span>
       </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>
                      <th>Admin Name</th>
                        <th>Admin Username</th>
                      <th>Admin Email</th>
                      <th>Admin Phone</th>
					  <th>Admin Group</th>
                      <th width="4">Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    
                    foreach($list as $admin){?>
                    <tr>
                      <td>
		                <?php
                    	      $user_name = $admin['admin_fname'];
			                   echo $user_name;
                    	?>
                      </td>
                        <td>
                            <?php
                            $admin_username = $admin['admin_username'];
                            echo $admin_username;
                            ?>
                        </td>
                      <td>
					  	<?php
                    		$admin_email = $admin['admin_email'];
							if($admin_email=="")
							{
								echo "------";
							}
							else
							{
                                $e1=substr($admin_email,0,2);
                                $e2= explode("@",$admin_email);
                                $domain=$e2[1];
								echo "*****@".$domain;
							}
                    	?>
                      </td>
                       <td>
					  	<?php
                    		$admin_phone = $admin['admin_phone'];
                            $e1=substr($admin_phone,7);
                            echo "********".$e1;
                    	?>
                      </td>
					   <td>
					  	<?php
                    		$admin_role = $admin['admin_role'];
                          switch ($admin_role) {
									case '1':
										echo "Super Administrator";
										break;
									case '2':
										echo "Dispatcher Admin";
										break;
									case 3:
										echo "Billing Admin";
										break;
								}
                    	?>
                      </td>
                        <?php
                        if($admin['admin_status']==1) {
                            ?>
                            <td class="">
                                <label class="label label-success" > Active</label>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td class="text-center">
                                <label class="label label-default" > Deactive</label>
                            </td>
                        <?php } ?>
                            <td>
                            <span data-target="#edit<?php echo $admin['admin_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                            </td>
                  </tr>
                  <?php }?>
                    </tbody>
                  
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End row --> 
    
  </div>
</form>

<?php foreach($list as $admin){?>
<div class="modal fade" id="edit<?php echo $admin['admin_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Admin Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">First Name</label>
              <input type="text" class="form-control"  placeholder="First Name" name="admin_fname" value="<?php echo $admin['admin_fname'];?>" id="admin_fname" required>
            </div>
          </div>
          
		  
		  <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Last Name</label>
              <input type="text" class="form-control"  placeholder="Last Name" name="admin_lname" value="<?php echo $admin['admin_lname'];?>" id="admin_lname" required>
            </div>
          </div>
		  
		  <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Email</label>
                <?php
                $admin_email = $admin['admin_email'];
                $e1=substr($admin_email,0,2);
                $e2= explode("@",$admin_email);
                $domain=$e2[1];

                ?>
              <input type="text" class="form-control"  placeholder="Email" name="admin_email" value="<?php echo "*****@".$domain;?>" id="admin_email" required>
            </div>
          </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Phone</label>
                    <?php
                    $admin_phone = $admin['admin_phone'];
                    $e1=substr($admin_phone,7);

                    ?>
                    <input type="text" class="form-control"  placeholder="Phone" name="admin_phone" value="<?php echo "********".$e1;?>" id="admin_phone" required>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-3" class="control-label">Username</label>
                    <input type="text" class="form-control"  placeholder="Username" name="admin_username" value="<?php echo $admin['admin_username'];?>" id="admin_username" required>
                </div>
            </div>
		    
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $admin['admin_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
    </div>
  </div>
</div>
<?php }?>

</section>

</body></html>