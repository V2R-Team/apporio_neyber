<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

if(isset($_POST['save'])) 
{
        $rental_category = $_POST['rental_category'];
        $query="select * from rental_category WHERE rental_category='$rental_category'";
	$result = $db->query($query);
        $ex_rows = $result->num_rows;
        if($ex_rows == 0)
        {
                $query2="INSERT INTO rental_category(rental_category,rental_category_hours,rental_category_kilometer) VALUES ('".$_POST['rental_category']."','".$_POST['rental_category_hours']."','".$_POST['rental_category_kilometer']."')";
		$db->query($query2);
		$car_type_id = $db->getLastId();
                $msg = "Category Added Successfully";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
               $db->redirect("home.php?pages=add-rental-category");
          
        }else{
                $msg = "Category Already Added";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
               $db->redirect("home.php?pages=add-rental-category");
        }
}

?>


<script>
    function validatelogin() {
        var rental_category = document.getElementById('rental_category').value;
        var rental_category_hours = document.getElementById('rental_category_hours').value;
        var rental_category_kilometer = document.getElementById('rental_category_kilometer').value;
        if(rental_category == "")
        {
            alert("Enter Rental Package Name");
            return false;
        }
        if(rental_category_hours == "")
        {
            alert("Enter Travel Hours");
            return false;
        }
        if(rental_category_kilometer == "")
        {
            alert("Enter Travel Kilometre");
            return false;
        }
       
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Rental Package</h3>
        
            <span class="tp_rht">
            <a href="home.php?pages=rental-category" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
       
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">


                  <div class="form-group ">
                      <label for="lastname" class="control-label col-lg-2">Rental Package Name*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Rental Package Name" name="rental_category" id="rental_category"/>
                      </div>
                  </div>

                <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Travel Hours*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Travel Hours" name="rental_category_hours" onkeypress="return isNumber(event)" id="rental_category_hours"/>
                  </div>
                </div>

                  <div class="form-group ">
                      <label for="lastname" class="control-label col-lg-2">Travel Kilometre*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Travel Kilometre" name="rental_category_kilometer" onkeypress="return isNumber(event)" id="rental_category_kilometer"/>
                      </div>
                  </div>
                
          
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
