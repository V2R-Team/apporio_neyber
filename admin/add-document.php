<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

if(isset($_POST['save'])) {

    $query = "INSERT INTO table_documents (document_name,document_type) VALUES ('" . $_POST['doc_name'] . "','" . $_POST['doc_type'] . "')";
    $db->query($query);
    $db->redirect("home.php?pages=documents");
}
?>
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Document</h3>
        <span class="tp_rht">
            
      </span>
      
      <span class="tp_rht">
                <!--<a href="home.php?pages=documents" class="btn btn-default btn-lg" id="add-button"  title="Back to Listing" role="button">Back to Listing</a>-->
                <a href="home.php?pages=documents" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                <div class="form-group ">
                  <label  class="control-label col-lg-2">Name  *</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Enter Name of Document" name="doc_name"/>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2">Type  *</label>
                  <div class="col-lg-6">
                      <select name="doc_type" class="form-control">
                          <option value="1">Driver</option>
                          <option value="2">Vehicle</option>
                      </select>
                  </div>
                </div>
                
                  <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
