<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from country";
$result = $db->query($query);
$list = $result->rows;
if(isset($_POST['save'])) {
    $country = $_POST['country'];
    $user_email = $_POST['user_email'];
    $user_name = $_POST['user_name'];
    $user_phone = $_POST['user_phone'];
    $user_password = $_POST['user_password'];
    $phone = $country.$user_phone;
    $query="select * from user WHERE user_phone='$phone'";
    $result = $db->query($query);
    $ex_rows1=$result->num_rows;
    if($ex_rows1 == 0)
    {
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data ;

        $query1="INSERT INTO user (user_name,user_email,user_password,user_phone,register_date) VALUES('$user_name','$user_email','$user_password','$phone',' $date')";
        $db->query($query1);
        $msg = "Rider Added";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=rider");
    }else{
        $msg = "This Phone Number Already Registerd";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-rider");
    }


}
?>

<script>
    function validatelogin() {
        var user_name = document.getElementById('user_name').value;
        var user_email = document.getElementById('user_email').value;
        var country = document.getElementById('country').value;
        var user_phone = document.getElementById('user_phone').value;
        var user_password = document.getElementById('user_password').value;
        var user_image = document.getElementById('user_image').value;
        if(user_name == "") { alert("Enter Full Name");return false; }
        if(country == "") { alert("Select Country");return false; }
        if(user_phone == "") { alert("Enter Phone Number");return false; }
        if(isNaN(user_phone)) { alert("Enter the valid Mobile Number");
            return false;
        }
        if(user_email == "") { alert("Enter Email");return false; }
        if(user_password == "") { alert("Enter Password");return false; }
        if(user_image == "") { alert("Upload Profile Image");return false; }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Riders</h3>
        <span class="tp_rht">
           <a href="home.php?pages=rider" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Full Name*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Full Name" name="user_name" id="user_name" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Country Code*</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="country" id="country" >
                                        <option value="">--Select Country--</option>
                                        <?php foreach($list as $country){ ?>
                                            <option id="<?php echo $country['phonecode'];?>"  value="<?php echo $country['phonecode'];?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Phone*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" placeholder="Phone Number" name="user_phone" id="user_phone" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Email*</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" placeholder="Rider Email" name="user_email" id="user_email" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Password*</label>
                                <div class="col-lg-6">
                                    <input type="password" class="form-control" placeholder="Password" name="user_password" minlength="6" id="user_password" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Profile Image</label>
                                <div class="col-lg-6">
                                    <input type="file" class="form-control" placeholder="Profile Image" name="user_image" id="user_image" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Add Rider" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
               </div>
        </div>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body>
</html>
