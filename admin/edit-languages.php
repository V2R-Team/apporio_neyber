<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query='SELECT * FROM table_notifications';
$result=$db->query($query);
$list=$result->rows;

$query1='SELECT * FROM table_languages';
$result1=$db->query($query1);
$data=$result1->rows;

$query2='SELECT * FROM table_messages WHERE language_id="'.$_GET['lang'].'"';
$result2=$db->query($query2);
$message=$result2->rows;

if(isset($_POST['save'])) {
    $query3='SELECT * FROM table_notifications';
    $result3=$db->query($query3);
    $list2=$result3->rows;
   foreach ($list2 as $i){
        $query3='UPDATE table_messages SET message ="'.$_POST[$i['message_id']].'" WHERE message_id="'.$i['message_id'].'" AND language_id="'.$_GET['lang'].'"';
       $db->query($query3);
   }

}
?>



    <div class="wraper container-fluid">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><div class="row"><h2 class="col-md-4"> Edit Language</h2>
                    <div class="col-md-8" align="right">Select a language :
                        <form method="post">
                        <select class="bootstrap-select" name="select_language" onchange="location=this.value;">
                            <option disabled selected>Language Name</option>
                            <?php foreach ($data as $language){ ?>
                                <option value="home.php?pages=edit-languages&lang=<?php echo $language['language_id']?>"><?php echo $language['language_name']?></option>
                            <?php } ?>
                        </select>
                    </form>
                    </div>
                </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post">
                                <?php foreach ($list as $data1){ ?>
                                    <div class="col-md-5 txtbx">
                                        <lable class="txthead"><?php
                                            $msg=$data1['message'];
                                            echo strtoupper(''.$msg);?>
                                        </lable>
                                        <input class="form-control " name="<?php echo $data1['message_id'];?>" value="<?php foreach ($message as $msg){
                                            if($data1['message_id']==$msg['message_id']){
                                                echo $msg['message'];
                                            }
                                        }?>">
                                    </div>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group" style="text-align: center;">
                                            <button class="btn btn-danger" name="save" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
        <!-- End row -->

    </div>
</form>
</section>
<!-- Main Content Ends -->

</body></html>