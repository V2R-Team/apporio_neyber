<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
	if(isset($_POST['seabt12']))
	{
		$sel=$_POST["sear12"];
		$in=$_POST["seain12"];
		$query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id where driver.$sel='$in' AND driver.verification_status=1";
	}
	else{
    $query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id WHERE driver.verification_status=1 ORDER BY driver.driver_id DESC";
	}
	$result = $db->query($query);
	$list=$result->rows;

    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET driver_admin_status='".$_GET['status']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=drivers");
    }
    
    if(isset($_GET['online_offline']) && isset($_GET['id'])) 
    {
     $query1="UPDATE driver SET online_offline='".$_GET['online_offline']."' WHERE driver_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=drivers");
    }

if(isset($_POST['savechanges'])) {
    $query2 = "UPDATE driver SET driver_name='" . $_POST['driver_name'] . "', driver_email='" . $_POST['driver_email'] . "',driver_phone='" . $_POST['driver_phone'] . "',car_type_id='" . $_POST['car_type_id'] . "',car_model_id='" . $_POST['car_model_id'] . "',car_number='" . $_POST['car_number'] . "',city_id='" . $_POST['city_id'] . "',commission='" . $_POST['commission'] . "' where driver_id='" . $_POST['savechanges'] . "'";
    $db->query($query2);

    if (!empty($_FILES['rcEdit'])) {
        $img_name = $_FILES['rc']['name'];
        $filedir = "../uploads/driver/";
        if (!is_dir($filedir)) mkdir($filedir, 0755, true);
        {
            $fileext = strtolower(substr($_FILES['rc']['name'], -4));
            if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                if ($fileext == "jpeg") {
                    $fileext = ".jpg";
                }
                $pfilename = time() . "rc_" . $driver_id . $fileext;
                $filepath1 = "uploads/driver/" . $pfilename;
                $filepath = $filedir . $pfilename;
                copy($_FILES['rc']['tmp_name'], $filepath);
                $query1 = "UPDATE driver SET rc='$filepath1',rc_expire='" . $_POST['RCExpireEdit'] . "'  where driver_id='" . $_POST['savechanges'] . "'";
                $db->query($query1);
            }
        }
    }
    if (!empty($_FILES['licenseEdit'])) {
        $img_name = $_FILES['license']['name'];
        $filedir = "../uploads/driver/";
        if (!is_dir($filedir)) mkdir($filedir, 0755, true);
        {
            $fileext = strtolower(substr($_FILES['license']['name'], -4));
            if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                if ($fileext == "jpeg") {
                    $fileext = ".jpg";
                }
                $pfilename = time() . "license_" . $driver_id . $fileext;
                $filepath1 = "uploads/driver/" . $pfilename;
                $filepath = $filedir . $pfilename;
                copy($_FILES['license']['tmp_name'], $filepath);
                $query1 = "UPDATE driver SET license='$filepath1',license_expire='" . $_POST['licenseExprEdit'] . "' where driver_id='" . $_POST['savechanges'] . "'";
                $db->query($query1);
            }
        }
    }

    if (!empty($_FILES['insuranceEdit'])) {
        $img_name = $_FILES['insurance']['name'];
        $filedir = "../uploads/driver/";
        if (!is_dir($filedir)) mkdir($filedir, 0755, true);
        {
            $fileext = strtolower(substr($_FILES['insurance']['name'], -4));
            if ($fileext == ".jpg" || $fileext == ".gif" || $fileext == ".png" || $fileext == "jpeg") {
                if ($fileext == "jpeg") {
                    $fileext = ".jpg";
                }
                $pfilename = time() . "insurance_" . $driver_id . $fileext;
                $filepath1 = "uploads/driver/" . $pfilename;
                $filepath = $filedir . $pfilename;
                copy($_FILES['insurance']['tmp_name'], $filepath);
                $query1 = "UPDATE driver SET insurance='$filepath1',insurance_expire='" . $_POST['insuranceExpireEdit'] . "' where driver_id='" . $_POST['savechanges'] . "'";
                $db->query($query1);

            }
        }
    }
    $db->redirect("home.php?pages=drivers");
}
//delete driver

if(isset($_POST['delete']))
     {
       $delqry1="DELETE from driver where driver_id='".$_POST['delete']."'";
       $db->query($delqry1);
       $db->redirect("home.php?pages=drivers");
     }
?>


     
<!-- script for alert box -->
<script>
	function msg()
	{
		alert("This functionality is not for demo version");
	}
</script>
  <div class="wraper container-fluid">
    <div class="page-title">
 
      <h3 class="title">Drivers</h3>
<span class="tp_rht">
      <a href="home.php?pages=pending-driver-approvals" data-toggle="tooltip" title="Pending Approvals" class="btn btn-pink"><i class="fa fa-bell" aria-hidden="true"></i> </i>Pending Approvals</a>    
      <a href="home.php?pages=add-driver" data-toggle="tooltip" title="Add Driver" class="btn btn-primary add_btn"><i class="fa fa-plus"></i></a>
<span>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
			
            <div class="col-md-12">
			<form method="post">
            		<div class="col-md-2 col-sm-2 col-xs-12">
                          <select class="form-control" name="sear12">
                            <option value="driver_name">---Search by---</option>
                            <option value="driver_name">Name</option>
                            <option value="driver_email">Email</option>
                            <option value="driver_phone">Mobile</option>
                          </select>
                        </div>
                
                <div class="col-md-2 col-xs-12 form-group ">
                  <div class="input-group">
                    <input type="text" id="first-name" name="seain12" placeholder="Search" class="form-control col-md-12 col-xs-12" required>
                  </div>
                </div>

                
                <div class="col-sm-2  col-xs-12 form-group ">
                      <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
               </div> 
			</form>
            <div class="col-sm-4"></div>
               <div class="dt-buttons btn-group col-sm-2">
               		<a class="btn btn-default buttons-copy buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons" href="#" onclick="msg()"><span>Import</span></a>
               		<a class="btn btn-default buttons-csv buttons-html5 btn-sm" tabindex="0" aria-controls="datatable-buttons" href="#" onclick="msg()"><span>Export</span></a>
               </div>
               
              <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                <table id="datatable" class="table table-striped table-bordered table-responsive">
                  <thead>
                    <tr>

                      <th width="5%">Id</th>
                      <th>Driver Name</th>
                      <th>Company Name</th>
                      <th>Email Id</th>
                      <th>Mobile No.</th> 
                      <th>Verified Date</th>
                      <th>Trips</th>
                      <th>Earning</th>
                      <th>Rating</th>
                      <th>Status</th>
                      <th >Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $j = 1;
                    foreach($list as $driver){?>
                    <tr>
                      <td><a href="#driverdetails<?php echo $driver['driver_id'];?>"  data-toggle="modal"><span class="badge bg-warning"><?php echo $j;?></span></a></td>
                      <td>
					  	<?php
							$driver_name = $driver['driver_name'];
								echo $driver_name;
						?>
                      </td>
                      <td>
                       <?php
                       $company_id = $driver['company_id'];
                       $query="select company_name from company WHERE company_id=".$company_id;
                       $result = $db->query($query);
                       $company=$result->row;
                       echo $company['company_name'];

                       ?>
                      </td>
                      <td>
					  	<?php
							$driver_email = $driver['driver_email'];
                            $e1=substr($driver_email,0,2);
                            $e2= explode("@",$driver_email);
                            $domain=$e2[1];
                            echo "*****@".$domain;
						?>
                      </td>
                      <td >
					  	<?php
							$driver_phone = $driver['driver_phone'];
                            $e1=substr($driver_phone,7);
                            echo "********".$e1;
						?>
                      </td>
                      
               
                      <td>
                      		<?= $driver['verification_date'];?>
                      </td>
                     
                      <td>
                      		<?= $completed_rides = $driver['completed_rides'];?>
                      </td>
                      
                      <td>
                      	<?= $total_payment_eraned = $driver['total_payment_eraned'];?>
                      </td>
                      <td><?php
                       $driverrating = $driver['rating'];
                       if ($driverrating == 0)
                       {
                           echo "Not Rate Yet";
                       }else{
                           $wholstar = floor($driverrating);
                           $haflstar = round($driverrating * 2) % 2;
                           $html = "";
                           for ($i=0;$i<$wholstar;$i++)
                           {
                               $html .= "<img src=star@13.png alt='Whole Star'>";
                           }if($haflstar){
                               $html .= "<img src=halfstar.png alt='Half Star'>";
                           }
                           print $html;
                       }
                       ?>
                      </td>
                      
                      <td>
                     <div class="row action_row" style="width:100px;">
              		   <?php if($driver['online_offline']==1) { ?>
                	   	<a href="#" class="" title="Online"><label class="label label-warning" style='width:48px;display: inline-block;'>Online</label></a>
	               	   <?php } else { ?>
                	   	<a href="#" class="" title="Offline"><label class="label label-danger" style='width:48px;display: inline-block;'>Offline</label></a>
                	   	
                	   	
                	   <?php } ?>
                	   
                	   <?php if($driver['driver_admin_status']==1) { ?>
                	   	<a href="#" class="" title="Active"><label class="label label-success" style='width:48px;display: inline-block;'> Active</label></a>
                	   	<?php } else { ?>
                	   	<a href="#" class="" title="Deactive"> <label class="label label-default" style='width:48px;display: inline-block;'> Deactive</label></a>
				 <?php } ?>
					
			</div>
			<div class="row action_row" style="width:100px;">		 
			   <?php if($driver['login_logout']==1) { ?>
            	      		<label class="label label-warning" style="width:48px;display: inline-block;">Login</label>
				<?php } else { ?>
            	      		<label class="label label-danger" style="width:48px;display: inline-block;">Logout</label>
            	      		 <?php } ?>
                          <?php
            	      	     $statusbusy = $driver['busy'];
            	      		 if($statusbusy==0)
            	      		 {
            	      		 echo "<label class=\"label label-info\" style='width:48px;display: inline-block;'>Free</label>"."<br>";
            	      		 }
            	      		 else if($statusbusy==1)
            	      		 {
            	      		 echo "<label class=\"label label-primary\" style='width:48px;display: inline-block;' >Busy</label>"."<br>";
            	      		 }
            	      		  else
            	      		 {
            	      		 echo "----";
            	      		 }
            	   		  ?>
 			</div>
                      </td>
			
		      <td >
                
                
               <div class="row action_row" style="width:92px;"> 
                <span data-target="#edit<?php echo $driver['driver_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                            
                                            <?php if($driver['driver_admin_status']==1){ ?>
                                            
                                            <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=drivers&status=2&id=<?php echo $driver['driver_id'];?>"> <i class="fa fa-eye"></i> </a>
                                            <?php } else { ?>
                                            <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=drivers&status=1&id=<?php echo $driver['driver_id'];?>"> <i class="fa fa-eye-slash"></i> </a>
                                            <?php } ?>

                                             <span data-target="#delete<?php echo $driver['driver_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>

     
                </div>
                </td>
                    </tr>
                    <?php
                      $j++;
                     }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php foreach($list as $driver){?>
<div class="modal fade" id="edit<?php echo $driver['driver_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Driver Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
		
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Name</label>
              <input type="text" class="form-control"  placeholder="Driver Name" name="driver_name" value="<?php echo $driver['driver_name'];?>" id="driver_name" required>
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Email</label>
                <?php
                $driver_email = $driver['driver_email'];
                $e1=substr($driver_email,0,2);
                $e2= explode("@",$driver_email);
                $domain=$e2[1];
                $driver_phone = $driver['driver_phone'];
                $e1=substr($driver_phone,7);

                ?>
              <input type="text" class="form-control"  placeholder="Driver Email" name="driver_email" value="<?php  echo "*****@".$domain;?>" id="driver_email" required>
            </div>
          </div>
          
	<div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Driver Phone</label>
              <input type="text" class="form-control"  placeholder="Driver Phone" name="driver_phone" value="<?php echo "********".$e1;?>" id="driver_phone" required>
            </div>
          </div>
		  
         <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Type</label>
              <select class="form-control" name="car_type_id" id="car_type_id"  onchange="getId(this.value);" required>
                 <option value="<?php echo $driver['car_type_id'];?>"><?php echo $driver['car_type_name']; ?></option>
            <?php foreach($list12345 as $car) :?>
                           <option value="<?php echo $car['car_type_id'];?>"><?php echo $car['car_type_name']; ?></option>
                <?php endforeach; ?>
    					
					</select>
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Model</label>
              <select class="form-control" name="car_model_id" id="car_model_id" required>
                 <option value="<?php echo $driver['car_model_id'];?>"><?php echo $driver['car_model_name']; ?></option>
					</select>
            </div>
          </div>
		  
	 <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Car Number</label>
              <input type="text" class="form-control"  placeholder="Car Number" name="car_number" value="<?php echo $driver['car_number'];?>" id="car_number" required>
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">Commission</label>
              <input type="number" class="form-control"  placeholder="Commission" name="commission" value="<?php echo $driver['commission'];?>" id="commission" required>
            </div>
          </div>
          
          
           <div class="col-md-12">
            <div class="form-group">
              <label for="field-3" class="control-label">City</label>
              <select class="form-control" name="city_id" id="city_id" required>
                 <option value="<?php echo $driver['city_id'];?>"><?php echo $driver['city_name']; ?></option>
                  <?php foreach($city as $city) :?>
                           <option value="<?php echo $city['city_id'];?>"><?php echo $city['city_name']; ?></option>
                <?php endforeach; ?>
					</select>
            </div>
          </div>


            <div class="form-group col-md-12 ">
                <label for="lastname" class="control-label col-lg-2">Driver License</label>
                <div class="col-lg-5">
                    <input type="file"  class="form-control" placeholder="Driver License" name="licenseEdit" id="license"/>
                </div>
                <label class="control-label col-lg-1">Expiry Date </label>
                <div class="col-lg-4">
                    <input type="date" class="form-control" placeholder="" name="licenseExprEdit" id="licenseExp"/>
                </div>
            </div>

            <div class="form-group col-md-12">
                <label for="lastname" class="control-label col-lg-2">Registration Certificate</label>
                <div class="col-lg-5">
                    <input type="file" class="form-control" placeholder="Car Registration Certificate" name="rcEdit" id="rc"/>
                </div>
                <label class="control-label col-lg-1">Expiry Date </label>
                <div class="col-lg-4">
                    <input type="date" class="form-control" placeholder="" name="RCExpireEdit" id="RCExpire"/>
                </div>
            </div>

            <div class="form-group col-md-12 ">
                <label for="lastname" class="control-label col-lg-2">insurance</label>
                <div class="col-lg-5">
                    <input type="file" class="form-control" placeholder="Car Insurance" name="insuranceEdit" id="insurance"/>
                </div>
                <label class="control-label col-lg-1">Expiry Date </label>
                <div class="col-lg-4">
                    <input type="date" class="form-control" placeholder="" name="insuranceExpireEdit" id="insuranceExpire"/>
                </div>
            </div>



        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" name="savechanges" value="<?php echo $driver['driver_id'];?>" class="btn btn-info">Save Changes</button>
      </div>
    </div>
  </div>
</div>
<!--DELETE MODEL-->
    <div class="modal fade" id="delete<?php echo $driver['driver_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Vehicle Model</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>This is Demo Version.</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $driver['driver_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
<?php }?>



<!--Driver Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
 foreach($list as $driver){?>
<div class="modal fade" id="driverdetails<?php echo $driver['driver_id'];?>" role="dialog"> <?php echo $driver['driver_id'];?>
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content" style="padding:20px !important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Driver Full Details</h4>
      </div>
        <div class="modal-body" style="max-height: 500px; overflow-x: auto;">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <thead>
              <tr>
                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($driver['driver_image'] != '' && isset($driver['driver_image'])){echo '../'.$driver['driver_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                <td><table style="margin-left:20px;" aling="center" border="0">
                    <tbody>
                      <tr>
                        <th class=""> Name</th>
                        <td class=""><?php
                             $drivername = $driver['driver_name'];
                             echo $drivername;
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Email</th>
                        <td class=""><?php
                            $driver_email = $driver['driver_email'];
                            $e1=substr($driver_email,0,2);
                            $e2= explode("@",$driver_email);
                            $domain=$e2[1];
                            echo "*****@".$domain;
                           ?></td>
                      </tr>
                      <tr>
                        <th class="">Phone</th>
                        <td class=""><?php
                            $driver_phone = $driver['driver_phone'];
                            $e1=substr($driver_phone,7);
                            echo "********".$e1;
                            ?></td>
                      </tr>
                      <tr>
                        <th class="">Status</th>
                        <td class=""><?php
            	           	 $loginlogout = $driver['login_logout'];
            	      		 if($loginlogout==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Login</button>";
							 }
            	      		 else
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Logout</button>";
            	      		 }
            	   		  ?>
                          <?php
            	      	     $statusbusy = $driver['busy'];
            	      		 if($statusbusy==0)
            	      		 {
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Free</button>";
            	      		 }
            	      		 else if($statusbusy==1)
            	      		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\" >Busy</button>";
            	      		 }
            	      		  else
            	      		 {
            	      		 echo "----";
            	      		 }
            	   		  ?>
                          <?php
            	      		 $onlineoffline=$driver['online_offline'];
            	      		 if($onlineoffline==1)
            	             { 
            	      		 echo "<button class=\"btn btn-success btn-xs activebtn\">Online</button>";
            	      		 }
            	      		 else
            	     		 {
            	      		 echo "<button class=\"btn btn-danger btn-xs\">Offline</button>";
            	      		 }
            	   		 ?>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <th class="">Device</th>
                <td class=""><?php
                   	  $phonedevice = $driver['flag'];
                    switch ($phonedevice){
                        case "1":
                            echo "Iphone";
                            break;
                        case "2":
                            echo "Android";
                            break;
                        default:
                            echo "------";
                    }
                   ?></td>
              </tr>
              <tr>
                <th class="">Rating</th>
                <td class=""><?php
                    $driverrating = $driver['rating'];
                    if ($driverrating == 0)
                    {
                        echo "Not Rate Yet";
                    }else{
                        $wholstar = floor($driverrating);
                        $haflstar = round($driverrating * 2) % 2;
                        $html = "";
                        for ($i=0;$i<$wholstar;$i++)
                        {
                            $html .= "<img src=star@13.png alt='Whole Star'>";
                        }if($haflstar){
                            $html .= "<img src=halfstar.png alt='Half Star'>";
                        }
                        print $html;
                    }
                    ?></td>
              </tr>
              <tr>
                <th class="">Car Type</th>
                <td class=""><?php
                         $car_type_name = $driver['car_type_name'];
                         echo $car_type_name;
                  ?>
                </td>
              </tr>
               <tr>
                <th class="">Commission</th>
                <td class=""><?php
                       $commission = $driver['commission'];
                       echo $commission."%";
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">Car Model</th>
                <td class=""><?php
                       $car_model_name = $driver['car_model_name'];
                       echo $car_model_name;
                   ?>
                </td>
              </tr>
              <tr>
                <th class="">Car Number</th>
                <td class=""><?php
                      $carnumber = $driver['car_number'];
                      echo $carnumber;
                   ?></td>
              </tr>
              <tr>
                <th class="">City Name</th>
                <td class=""><?php 
                    $city_name = $driver['city_name'];
                    echo $city_name;
                    ?></td>
              </tr>
              <tr>
                <th class="">Register Date</th>
                <td class=""><?php
                      $registerdate = $driver['register_date'];
                      echo $registerdate;
                   ?></td>
              </tr>
              <tr>
                <th class="">Current Location</th>
                <td class=""><?php
                      $currentlocation = $driver['current_location'];
                                                        if($currentlocation=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $currentlocation;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Completed Rides</th>
                <td class=""><?php
                                                        $completedrides=$driver['completed_rides'];
                                                        if($completedrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $completedrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Rejected Rides</th>
                <td class=""><?php
                                                        $rejectrides=$driver['reject_rides'];
                                                        if($rejectrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $rejectrides;
                                                           }
                                                       ?></td>
              </tr>
              <tr>
                <th class="">Cancelled Rides</th>
                <td class=""><?php
                                                        $cancelledrides=$driver['cancelled_rides'];
                                                        if($cancelledrides=="")
                                                           {
                                                           echo "---------";
                                                           }
                                                           else
                                                           {
                                                           echo $cancelledrides;
                                                           }
                                                       ?></td>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
           <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
            <tfoot>
            	<tr>
                	<th>Licence</th>
                    <th>Registration Certificate (RC)</th>
                    <th>Insurance</th>
                </tr>
            	<tr>
					<?php $filenotexit="http://apporio.co.uk/apporiotaxi/uploads/driver/filenotexit.png"; ?>
                    <?php $fileexit="http://apporio.co.uk/apporiotaxi/uploads/driver/fileexit.png"; ?>
                	
                     <td>
                     	<a target="_blank" href="<?php if($driver['license'] != '' && isset($driver['license'])){echo '../'.$driver['license'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['license'] != '' && isset($driver['license'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                      </td>
                      
                     <td>
                     	<a target="_blank" href="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo '../'.$driver['rc'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['rc'] != '' && isset($driver['rc'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                         </a>
                     </td>
                     <td>
                     	<a target="_blank" href="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo '../'.$driver['insurance'];}else{ echo $filenotexit; }?>">
                        	<img src="<?php if($driver['insurance'] != '' && isset($driver['insurance'])){echo $fileexit;}else{ echo $filenotexit; }?>"  width="150px" height="150px">
                        </a>
                     </td>
                    
                </tr>
            </tfoot>
           </table>
        </div>
    </div>
  

    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>

<script>
function getId(val) {

  $.ajax({
        type: "POST",
        url: "viewcar_model.php",
        data: "car_type_id="+val,
        success: 
        function(data){
       $('#car_model_id').html(data);
        }
    });  
}
 
</script> 
</section>
</body></html>