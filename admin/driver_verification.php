<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
if (isset($_GET['driver_id'])){
    $query="select * from driver INNER JOIN city ON driver.city_id=city.city_id INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id INNER JOIN car_model ON driver.car_model_id=car_model.car_model_id WHERE driver.driver_id='".$_GET['driver_id']."'";
    $result = $db->query($query);
    $list=$result->row;
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Pending Approval</h3>
      <span class="tp_rht">
         <a href="home.php?pages=pending-driver-approvals" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>	
    </div>
    <div class="row">

        <div class="panel panel-default">
          <div class="panel-body">
          	<div class="col-md-12">
	          <div class="col-md-4">
			
	                  <div class="bs-example" data-example-id="simple-jumbotron">
	                  <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:2px;">
		                    <strong>PERSONAL DETAILS</strong> 
		                  </div>
		            <div class="alert alert-info alert-dismissible" role="alert" style=" height:145px;">
		                    <table width="100%" style="font-size:16px;">
		                  	<tbody>
		                  		<tr>
		                  			<td width="30%"><b>Name</b></td>
		                  			<td width="50%"><?= $list['driver_name'];?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>Email Id</b></td>
		                  			<td width="50%">
                                        <?php
                                        $driver_email = $list['driver_email'];
                                        $e1=substr($driver_email,0,2);
                                        $e2= explode("@",$driver_email);
                                        $domain=$e2[1];
                                        echo "*****@".$domain;
                                    ?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>Phone No.</b></td>
		                  			<td width="50%">
                                        <?php
                                        $driver_phone = $list['driver_phone'];
                                        $e1=substr($driver_phone,7);
                                        echo "********".$e1;
                                        ?>
                                        </td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>City</b></td>
		                  			<td width="50%"><?= $list['city_name'];?></td>
		                  		</tr>
		                  	</tbody>
		                  </table>	
		           </div>      
	                  </div>
	                 
		  </div>	
		  <div class="col-md-4" >
			
	                  <div class="bs-example" data-example-id="simple-jumbotron">
	                  <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:2px;">
		                    <strong>VEHICLE DETAILS</strong> 
		                  </div>
	                    <div class="alert alert-info alert-dismissible" role="alert" style=" height:145px;">
				 <table width="100%" style="font-size:16px;">
		                  	<tbody>
		                  		<tr>
		                  			<td width="40%"><b>Car Type</b></td>
		                  			<td width="50%"><?= $list['car_type_name']?></td>
		                  		</tr>
		                  		<tr>
		                  			<td width="40%"><b>Car Model</b></td>
		                  			<td width="50%"><?= $list['car_model_name']?></td>
		                  		</tr>

		                  	</tbody>
		                  </table>	
	                    </div>
	                  </div>
	                 
		  </div>	
		  <div class="col-md-4">
			
	                  <div class="bs-example" data-example-id="simple-jumbotron">
	                  <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom:2px;">
		                    <strong>BANK DETAILS</strong> 
		                  </div>
	                    <div class="alert alert-info alert-dismissible" role="alert" style="height:145px;">
				 <table width="100%" style="font-size:16px;">
		                  	<tbody>
		                  		<tr>
		                  			<td width="50%"><b>A/c No.</b></td>
		                  			<td width="50%">Not Updated</td>
		                  		</tr>
		                  		<tr>
		                  			<td width="50%"><b>Swift Code</b></td>
		                  			<td width="50%">Not Updated</td>
		                  		</tr>
		                  		<tr>
		                  			<td width="50%"><b>IFSC Code</b></td>
		                  			<td width="50%">Not Updated</td>
		                  		</tr>
		                  		<tr>
		                  			<td width="50%"><b>A/c Name</b></td>
		                  			<td width="50%">Not Updated</td>
		                  		</tr>
		                  		<tr>
		                  			<td width="30%"><b>City</b></td>
		                  			<td width="50%">Not Updated</td>
		                  		</tr>
		                  	</tbody>
		                  </table>	
	                    </div>
	                  </div>
	                 
		  </div>	
		   <div class="row">
		     <div class="col-md-12 col-sm-12 col-xs-12">
		     <div class="col-md-4">
		        <div class="alert alert-success alert-dismissible col" role="alert" style="margin-bottom:2px;">
		              <strong>UPLOADED DOCUMENTS</strong> 
		        </div>
		      </div>  
		     </div>	
	  	     <div class="col-md-12 col-sm-12 col-xs-12">
	          	<div class="col-sm-4 col-md-4"> 

		          <table class="table table-striped table-bordered table-responsive dataTable no-footer" role="grid" aria-describedby="datatable_info">
		          	<thead>
		          	    <tr role="row">
		          		<th>Sr. No.</th>
		          		<th>Document Name</th>
		          		<th>Link</th>
		          		<th>Action</th>
		                    </tr?		
		          	</thead>
		          	<tbody>
		          	    <tr role="row">
		          		<td>1</td>
		          		<td> Driving License</td>
		          		<td><a href="#"><i class="fa fa-file" aria-hidden="true" target="_blank"></i> <em>file</em></a></td>
		          		<td><label class="btn btn-success br2 btn-xs fs12 activebtn"> verify </label></td>
		          	    </tr>
		          	     <tr role="row">
		          		<td>2</td>
		          		<td> R/C</td>
		          		<td><a href="#"><i class="fa fa-file" aria-hidden="true" target="_blank"></i> <em>file</em></a></td>
		          		<td><label class="btn btn-success br2 btn-xs fs12 activebtn"> verify </label></td>
		          	    </tr>
                        <tr role="row">
                            <td>3</td>
                            <td> Insurence</td>
                            <td><a href="#"><i class="fa fa-file" aria-hidden="true" target="_blank"></i> <em>file</em></a></td>
                            <td><label class="btn btn-success br2 btn-xs fs12 activebtn"> verify </label></td>
                        </tr>
		          	<tbody>
		          </table>
	          	</div>
	       	     </div>
      	         </div>
		  <div class="form-group">
                        <div class="clearfix"></div><br>
                        <a class="col-md-5 col-sm-5 col-xs-12">
                          <div class="clearfix"></div><br>
                            <a href="home.php?pages=pending-driver-approvals&driver_id=<?php echo $list['driver_id']; ?>"<button type="button" class="btn btn-success" style="margin-right:3px;">Verify</button></a>
                          <button type="button" class="btn btn-warning"  data-toggle="modal" data-target="#pendingapproval">Reject</button>
                        </div>
                  </div>	
          </div>

          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
    <!-- End row --> 
    
  </div>
  
</div>
<!-- popup -->

    <div class="modal fade" id="pendingapproval" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Notify Driver</h4>
                    <div class="col-md-12">
 			<p>Welcome to apporio taxi panel</p>
 			 <button type="button" class="btn btn-success" style="margin-right:3px;" >Skip</button>
                         <button type="button" class="btn btn-warning" >Send Mail</button>
 		</div>
                </div>
 		
            </div>

        </div>
    </div>

<!-- Main Content Ends -->

</body>
</html>
