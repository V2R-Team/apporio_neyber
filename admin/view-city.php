<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;
  $query = "select * from currency";
  $result = $db->query($query);
  $currency = $result->rows; 
 
   $query = "select * from languages";
  $result = $db->query($query);
  $list1 = $result->rows; 
if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE city SET city_admin_status='".$_GET['status']."' WHERE city_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view-city");
}


if(isset($_POST['savechanges']))
{
    $query2="UPDATE city SET city_name='".$_POST['city_name']."',currency='".$_POST['currency']."',distance='".$_POST['distance']."' where city_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-city");
}

//delete type
if(isset($_POST['delete']))
     {
       $delqry1="DELETE from city where city_id='".$_POST['delete']."'";
       $db->query($delqry1);
       $db->redirect("home.php?pages=view-city");
     }
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datatable_filter label:first").after("<a href='home.php?pages=add-city' data-toggle='tooltip' data-original-title='Add City'><button type='button' style='margin-top: 18px; margin-left: 15px;'  class='btn addBtn btn-success'> <i class='fa fa-plus-circle'></i></button></a>");
        $("#datatable_filter").css({"margin-top":"-15px"});
    });
</script>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View City</h3>
        </div>
        <div class="row">
            <div class='col-md-8 col-sm-8 col-xs-8'>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl"> 
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>City Name</th>
                                        <th>Currency</th>
                                        <th>Distance</th>
                                        <th width="10%">Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $city){ ?>
                                        <tr>
                                           <td>
                                                <?php
                                                $city_name=$city['city_name'];
                                                echo $city_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $currency_name=$city['currency'];
                                                echo $currency_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $distance=$city['distance'];
                                                echo $distance;
                                                ?>
                                            </td>
                                            <?php
                                            if($city['city_admin_status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    
                                                        <label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    
                                                        <label class="label label-default" > Deactive</label>
                                                </td>
                                            <?php } ?>
                                            <td>
                                            <div class="row action_row" style="width:95px;">    
                                                <span data-target="#<?php echo $city['city_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                                
												<?php if($city['city_admin_status']==1){ ?>
												<a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=view-city&status=2&id=<?php echo $city['city_id']?>"> <i class="fa fa-eye"></i> </a>
												<?php } else { ?>
												<a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=view-city&status=1&id=<?php echo $city['city_id']?>"> <i class="fa fa-eye-slash"></i> </a>
												<?php } ?>
                                                <span data-target="#delete<?php echo $city['city_id'];?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>   
                                            </div>
											</td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
           
        </div>
        
        <!-- End row -->

    </div>
     
<?php foreach($list as $city){?>
<div class="modal fade" id="<?php echo $city['city_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit City Details</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
        <div class="modal-body">
          
		  <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">City Name</label>
                <input type="text" class="form-control"  placeholder="City Name" name="city_name" value="<?php echo $city['city_name'];?>" id="city_name" required>
              </div>
            </div>
          </div>
		  
		   <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Currency</label>
                 <select class="form-control" name="currency" id="currency" required>
                   <?php foreach($currency as $key=>$cur){ ?>
                        <option id="<?php echo $cur['iso'];?>"  value="<?php echo $cur['iso'];?>" <?php if($cur['iso'] == $city['currency']){ ?> selected <?php } ?>><?php echo $cur['name']; ?></option>
				   <?php } ?>
                   </select>
              </div>
            </div>
          </div>
		  
		  
		   <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Distance Unit</label>
                 <select class="form-control" name="distance" id="distance" required>
				   <option value="">Select Distance Unit</option>
				   <option value="Miles" <?php if($city['distance'] == "Miles"){ ?> selected <?php } ?>>Miles</option>
                     <option value="Kilometers" <?php if($city['distance'] == "Kilometers"){ ?> selected <?php } ?>>Kilometers</option>
                   </select>
              </div>
            </div>
          </div>

		  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $city['city_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
  </div>
</div>
<?php }?>

</section>

<!--DELETE City-->
<?php
foreach($list as $city){ ?>
    <div class="modal fade" id="delete<?php echo $city['city_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete City</h4>
                </div>
                <form  method="post">
                    <div class="modal-body">
                        <div class="row">
                            <h3>Do You Really Want To Delete this City?</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $city['city_id'];?>" class="btn btn-danger">Delete</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
<?php } ?>
<!-- Main Content Ends -->

</body></html>